<?php header("Access-Control-Allow-Origin: *");
date_default_timezone_set('America/Lima');
/**
*
*/
if ( ! function_exists( 'get_path_comprobante' ) )
{
	/**
 	*  @return retorna la ubicacion del comprobante
 	*  @param string $fileNameIn = nombre del archivo
 	*  @param string $method = extension del archivo
 	*  @param string $ruc_emi = ruc del emisor
 	*  @param string $typeDocument = tipo de documento(  01 , 03)
 	**/
 	 function get_path_comprobante( string $fileNameIn, string $extension , string $fecha, $ruc_emi, $typeDocument)
 	{

 		$typeShippingFolder = $extension == 'cdr' ? '/RESPUESTAS' : '/ENVIOS';
 	    $folders = $typeShippingFolder ;
 		$fileName = $typeShippingFolder == '/RESPUESTAS' ? 'R-'. $fileNameIn : $fileNameIn;
 		switch( $typeDocument )
	    {
	        case '01': 
	                $folders .= '/FACTURAS';
	            break;
	        case '03':
	                $folders .= '/BOLETASVENTA';
	            break;
	        case '07':
	                $folders .= '/NOTASDECREDITO';
	            break;
	         case '08':
	                $folders .= '/NOTASDEDEBITO';
	            break;
	         case '09':
	                $folders .= '/GUIAREMISION';
	            break;
	        case '20':
	                $folders .= '/RETENCIONES';
	        	break;
	        case '40':
	                $folders .= '/PERCEPCIONES';
	            break;
	        case 'RA':
               		$folders .= '/COMUNICACIONBAJA';
           		 break;
           	 case 'RC':
		           	if ( $typeShippingFolder== "/ENVIOS"  ) 
		           	{
		           	 	$folders .= '/RESUEMENDIARIO';
		           	}else
		           	{
		           		$folders .= '/RESUMENDIARIO';
		           	}
           		 break;
           	case 'RR':
               		$folders .= '/REVERSIONES';
           		 break;

	    }
	    $folders .= '/'. $fecha;
	    $pathCpe = env( 'APP_SO' ) == 'WINDOWS' ?  'C:\cpe\\' : '/cpe/';
	    if ( !empty( $fileNameIn )  ) 
	    {
	    	$extension = $extension == 'cdr'? 'zip' : $extension;
	    	$path = $pathCpe . $ruc_emi . $folders  . '/' .  $fileName . '.' .  $extension ;//linux 
	    }else
	    {
	    	$path =$pathCpe . $ruc_emi . $folders . "/"  ;
	    }
	    return $path;
 	}
}
/**
* Busca y retorna la ruta de un archivo dado el filtro ticket
*/
if (  !function_exists( 'get_anexo_by_ticket' )  ) 
{
	function get_anexo_by_ticket( $ruc, $ticket )
	{

		
		$path =  env('PATH_SAVE_FILE','/cpe/') . $ruc . '/ANEXOS/';
		$pathEnviado =  $path .'ENVIADOS/';
		if ( file_exists( $path ) )
		{
			$results=glob($path . $ticket . "*", GLOB_BRACE );
			foreach ( $results as $nombre_archivo )
			{
			    $info = new SplFileInfo( $nombre_archivo );
			    if ( !$info->isReadable() ) 
			    {
					return [ 'status' => false, 'error' => 'ANEXO: No se tiene acceso al archivo del ajunto: '. $nombre_archivo ];
			    }
			    $file = base64_encode(file_get_contents( $nombre_archivo));
			    $fileName =  $info->getFilename() ;
				rename($nombre_archivo, $pathEnviado . $fileName );
				return ['status' => true, 'file' => $file  , 'extension' => $info->getExtension(), 'name_file' => $fileName];
			}
			return ['status' => false, 'message' => 'No existe archivo adjunto en: '. $path, 'error' => '' ];
		}else{
			create_new_directory( $path );
			create_new_directory( $pathEnviado );
		}
		return ['status' => false, 'error' => 'ANEXO: No se tiene acceso a la ruta del ajunto: '. $path ];
		
	}
}
/**
* Escribe un mensaje de error 
* @param $cadena es el mensaje 
* @param $tipo es el tipo de error  0 = error , 1 = afirmacion
*/
if (  ! function_exists( 'write_log' )  ) {
	function write_log($cadena, $tipo = 0)
        {
        
            $hoy = $tipo ? date("Y_m_d").".txt" : "error_" . date("Y_m_d").".txt";
            $fileAll = "ws-all.log";
        	// $nombre_directorio =  "/var/www/html/ws_txt_json/Logs/";
            $nombre_directorio =  $_SERVER[ 'DOCUMENT_ROOT' ]. "/ws/Logs/";
         
                    if( file_exists("$nombre_directorio") == true ){
                            $fp = fopen("$nombre_directorio/".$hoy, "a+");
                            $fpAll = fopen("$nombre_directorio/".$fileAll, "a+");
                    }
                    else{
                            $directotio = mkdir("$nombre_directorio", 0777);
                            $fp = fopen("$nombre_directorio/".$hoy, "w");
                            $fpAll = fopen("$nombre_directorio/".$fileAll, "w");
                    }
            //all
        	// fwrite( $fpAll, $cadena."==============================================\n");
        	fwrite( $fpAll, " (" . date("d/m/Y H:i:s:") .round(microtime(true) * 1000). ") : " . $cadena . " \n");
        	fclose( $fpAll);

            fwrite( $fp, " (" . date("d/m/Y H:i:s:") .round(microtime(true) * 1000). ") : " . $cadena . " \n");
            fclose( $fp);
        }

}

/**
* Escribe un mensaje de error 
* @param $cadena es el mensaje 
* @param $tipo es el tipo de error  0 = error , 1 = afirmacion
*/
if (  ! function_exists( 'get_type_doc_by_type' )  ) 
{
	function get_type_doc_by_type( $tipo = 0 )
    {
    
       switch( $tipo )
	    {
	        case '01': 
	                $typeDoc = 'FACTURA';
	            break;
	        case '03':
	                $typeDoc = 'BOLETAS DE VENTA';
	            break;
	        case '07':
	                $typeDoc = 'NOTASDECREDITO';
	            break;
	         case '08':
	                $typeDoc = 'NOTASDEDEBITO';
	            break;
	        case '09':
	                $typeDoc = 'GUIAREMISION';
	            break;
	        case '20':
	                $typeDoc = 'RETENCIONES';
	            break;
	        case '40':
	                $typeDoc = 'PERCEPCIONES';
	            break;
	        case 'RR':
	                $typeDoc = 'RESUMEN REVERSION';
	            break;
	        case 'RC':
	                $typeDoc = 'RESUMEN DIARIO';
	            break;
	        case 'RA':
	                $typeDoc = 'COMUNICACION BAJA';
	            break;
	        default :
	         $typeDoc = 'COMUNICACION BAJA';
	         break;
	        
	    }
	    return $typeDoc;
    }

}

if (!function_exists( 'create_new_directory' ) ) 
{
	/**
	* Crea un nuevo directorio
	*/
	function  create_new_directory( $directorio_final, $usuario = 'www-data' )
	{
		try {
			if (!is_dir($directorio_final)) 
		    {
		       
		        if ( mkdir($directorio_final, 0777, true) ) 
		        {
		        	chown( $directorio_final, $usuario );    
		        	chgrp ( $directorio_final, $usuario );
		        	// write_log( 'Se creo correctamente el archivo: '.$directorio_final , 1  );
		        }else
		    	{
		    		throw new Exception('No se pudo crear el archivo de respuesta: ' . $directorio_final, 1);
		    	}
		    }	
		} catch (Exception $e) {
			throw new Exception("No se pudo crear la carpeta :"  . $directorio_final, 1);
					
		}
		
	}
}

if (!function_exists( 'asign_permission_file' ) ) 
{
	/**
	* asigna permisos al archivo 
	*/
	function  asign_permission_file( $dirFile, $usuario = 'www-data' )
	{
		if (file_exists($dirFile)) 
	    {
	       
        	chown( $dirFile, $usuario );    
        	chgrp ( $dirFile, $usuario );
	    }
	}
}
if ( !function_exists( 'get_name_operador' ) ) 
{
	function  get_name_operador( $tipo_operador )
	{
		switch ( $tipo_operador ) 
		{
			case  0:
					$name = 'SUNAT';
				break;
			
			case 1:
					$name = 'OSECONSE';
				break;

			default:
					$name = 'OSE DIGIFLOW';
				break;
		}
		return $name;
	}
}




// $record  = [ 
// 	'ruc' => '20202380621', 
// 'serie' => 'F001',
// 'numero' => '00003353',
// 'tipo_cpe' => '03',
// 'tipo_correo' => '2',
// 'razon_social'=> 'MANAGEMENT SAS',
// 'fecha' => '2020-04-15',
// 'enlace_front_registro_adquiriente' => true,
// 'enlace_front_sesion_adquiriente' => true,
// 'token_registro' => 'adfa',
// 'subject' => [ 'numero'=> 'adf', 'serie' => 'RA-023123' ]
//  ];
 
// var_dump(get_email_template($record)) ;
// exit;

/**
*@return retorna el nombre del estado del cpe
*/
 if ( !function_exists( 'get_name_status_by_respe' ) )
 {
 	function get_name_status_by_respe( $type )
 	{
 		$resType = '' ;
 		switch ($type) 
 		{
 			case 'A':
 				    $resType = "ACEPTADO" ;
 				break;

 			case 'O':
 					$resType = "ACEPTADO CON OBSERVACION" ;
 				break;

 			case 'R':
 					$resType = "RECHAZADO" ;
 				break;

 			case 'B':
 					$resType = "ANULADO" ;
 				break;
 			default:
 				$resType = '';
 			break;
 		}
 		return $resType;
 	}
 }


if ( !function_exists('sendJsonWsRest') ) {
	/**
* Envio del json al servicio rest JSP
* @param $sendJson = json a enviar 
* @param $url = url del servicio EJM: "http://178.238.228.156:8080/rest/emitirPost?usuario=mapfresalud&contrasena=mapfresalud"
* @param $archivoPhp = nombre del archivo php o Handler que retorna el json EJM:  "Handler_RDiario"
* @param $usuario = Usuario del sistema REST
* @param $pass = Contraseña del sistema REST
*/
	function sendJsonWsRest( $sendJson,  $usuario, $pass )
	{
	    include  $_SERVER[ 'DOCUMENT_ROOT' ] . '/ws/Config/config.php';
	    $sendJson = json_encode($sendJson);
	    // $userEmisor = getUserEmisorByRuc( $remitente );
	    // $usuario = $userEmisor['username'];
	    // $pass=$userEmisor['password'];
	    $respuesta = "";
	    $url = $config[ 'api_services_java_pse' ] . '/rest/emitirPost?usuario='.$usuario."&contrasena=".$pass;
	    $ch   =   curl_init ( $url ) ; 

	    curl_setopt ( $ch ,   CURLOPT_POST ,   1 ) ; 
	    curl_setopt ( $ch ,   CURLOPT_POSTFIELDS ,   $sendJson) ; 
	    curl_setopt ( $ch ,   CURLOPT_HTTPHEADER ,   array ('Content-Type: application/json') ) ;  
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    $respuesta = curl_exec($ch);

	    if ( !$respuesta ) 
	    {
	        $error = 'Curl error: ' . curl_error($ch). ' ,URL: '. $url  . ' jsonSend:' . $sendJson;
	        write_log(  $error );
	        throw new Exception($error, 1);
	    }
	    else
	    {
	        $obj = json_decode( $respuesta );
	        $status = isset( $obj->status ) ? $obj->status : '200';
	        if ( $status == "500" ) 
	        {

	            $error =  "Error : " .  $respuesta  . ", URL: '" . $url . ' jsonSend:' . $sendJson . ' statusService: '. $status;
	            write_log(  $error );
	            throw new Exception($error, 1);
	        }

	    }

	    return [ 'message' => $respuesta ];
	}
}

if ( !function_exists('setParameterRequestQuery') ) {
	/**
* Setea los parametros de entrada del front, para la consulta de la base de datos
* @param $arrayDatos 
* @param $tipoConsulta SI TIPO DE CONSULTA ES CG ENTONCES ES SOLO COMPROBANTES GENERADOS
*/
	function setParameterRequestQuery( $arrayDatos , $tipoConsulta = 'CG')
	{
		switch ( $tipoConsulta ) 
        {
        	case 'CG'://Comprobantes Generados
        			$doc_tipo =  ["'01'", "'03'", "'07'", "'08'", "'20'", "'40'","'09'","'13'"];
        		break;
        	default:
        			$doc_tipo =  ["'01'", "'03'", "'07'", "'08'", "'20'", "'40'","'09'","'13'"];
        		break;
        }
		$estados = [];
	    $nro_documento =( isset( $arrayDatos["nro_documento"] ) && $arrayDatos["nro_documento"] != "" ) ? $arrayDatos["nro_documento"] : false ;
        
        $emi_ruc = ( isset( $arrayDatos["emi_ruc"] ) && $arrayDatos["emi_ruc"] != "" ) ? $arrayDatos["emi_ruc"] : false ;
        $razon_social_adquiriente = ( isset( $arrayDatos["razon_social_adquiriente"] ) && $arrayDatos["razon_social_adquiriente"] != "" ) ? $arrayDatos["razon_social_adquiriente"] : false ;
        
        $doc_serie = ( isset( $arrayDatos["doc_serie"] ) && $arrayDatos["doc_serie"] != ""  && strlen($arrayDatos["doc_serie"])>2 ) ? str_replace("'", "", $arrayDatos["doc_serie"])  : false ;
        $doc_tipo =  ( isset( $arrayDatos["doc_tipo"] ) && $arrayDatos["doc_tipo"] != "" ) ? Array( "'" . $arrayDatos["doc_tipo"]. "'" )  : $doc_tipo ;
        $status_email =  ( isset( $arrayDatos["status_email"] ) && $arrayDatos["status_email"] != "" ) ? strval( $arrayDatos["status_email"])  : '' ;
        $doc_nume =  ( isset( $arrayDatos["doc_nume"] ) && $arrayDatos["doc_nume"] != "" ) ? str_replace("'", "", $arrayDatos["doc_nume"])  : false ;
        $doc_fechini =  ( isset( $arrayDatos["doc_fechini"] ) && $arrayDatos["doc_fechini"] != "" ) ? str_replace("'", "", $arrayDatos["doc_fechini"])  : false ; 
        $doc_fechfin =( isset( $arrayDatos["doc_fechfin"] ) && $arrayDatos["doc_fechfin"] != "" ) ? str_replace("'", "", $arrayDatos["doc_fechfin"])  : false ; 

        $ruc_adquiriente =( isset( $arrayDatos["ruc_adquiriente"] ) && $arrayDatos["ruc_adquiriente"] != "" ) ? str_replace("'", "", $arrayDatos["ruc_adquiriente"])  : false ;
        
        
        $pb_a =  ( isset( $arrayDatos["pb_a"] ) && $arrayDatos["pb_a"] ) ? array_push($estados, "'A'")  : '' ;
        $pb_o =  ( isset( $arrayDatos["pb_o"] ) && $arrayDatos["pb_o"] ) ? array_push($estados, "'O'")  : '' ;
        $pb_r =  ( isset( $arrayDatos["pb_r"] ) && $arrayDatos["pb_r"] ) ? array_push($estados, "'R'")  : '' ;
        $pb_p =  ( isset( $arrayDatos["pb_p"] ) && $arrayDatos["pb_p"] ) ? array_push($estados, "NULL")  : '' ;
        $pb_b =  ( isset( $arrayDatos["pb_b"] ) && $arrayDatos["pb_b"] ) ? array_push($estados, "'B'")  : '' ;


        $parameter = [ 'estados' => $estados, 'nro_documento' => $nro_documento, 'emi_ruc' => $emi_ruc, 'doc_serie' => $doc_serie, 'doc_tipo' => $doc_tipo, 'doc_nume' => $doc_nume , 'doc_fechini' => $doc_fechini, 'doc_fechfin' => $doc_fechfin,'status_email' =>$status_email , 'razon_social_adquiriente' => $razon_social_adquiriente, 'ruc_adquiriente'=> $ruc_adquiriente];
       
         return $parameter;
	}
}

if ( !function_exists( 'verify_cod_service_reprocess' ) ) 
{
	/**virifica los codigo de servicios que son para reprocesar
	* @param $codigo codigo de repuesta del servicio
	**/
	function verify_cod_service_reprocess( $codigo ) 
	{
		$arrayCodigoErrorService = ["0098", "0100", "0109", "0125", "0127", "0130", "0131", "0132", "0133", "0134", "0135", "0136", "0137", "0138", "0200", "0201", "0202", "0203", "0250", "0251", "0252", "0253" ];
		return in_array($codigo, $arrayCodigoErrorService );
	}
}


if ( !function_exists( 'compress_file_zip' ) ) 
{
	/**
	* comprime un archivo en zip
	*/
	function compress_file_zip( $pathFile, $zipName , $nameFile)
	{
		$z = new ZipArchive();
		
		if( $z->open( $zipName, ZIPARCHIVE::CREATE ) === true )
		{
			$z->addFile( $pathFile, $nameFile);
			$z->close();
			$message = 'se creo el archivo : '.$zipName . " del archivo ". $pathFile. "<br>";
			return  $zipName;
		}else{
			$error = 'Error de compresion: en el archivo :' . $pathFile . " nombre: ". $zipName ;
			echo $error;
			write_log( $error );
		}
	}
}


if ( !function_exists( 'get_subjet_by_ruc' ) ) 
{
	/**
	* retorna los valores del set_from_email para el ws de correo
	*/
	function get_subjet_by_ruc( $ruc ='' )
	{
		// $emailByDefault = 'miscomprobantes@fexcpe.com';
		$name = 'miscomprobantes';
		$email = 'miscomprobantes@fexcpe.com';
		switch ($ruc) 
		{
			case '20100119065':
					$name = 'Facturacion Xper';
					$email = 'facturacionxper@xerox.com';
				break;
			case '20418896915':
					$name = 'comprobantes@mapfre.com.pe';
					$email = 'comprobantes@mapfre.com.pe';
				break;
			case '20517182673':
					$name = 'comprobantes@mapfre.com.pe';
					$email = 'comprobantes@mapfre.com.pe';
				break;
			case '20202380621':
					$name = 'comprobantes@mapfre.com.pe';
					$email = 'comprobantes@mapfre.com.pe';
				break;
		}
		$result = ['name' => $name, 'email' => $email ];
		return $result;
	}
}

if ( !function_exists( 'get_data_by_cpe' ) ) 
{
	/**
	* retorna cierta informacion por tipo de cpe [ 'tipoMoneda' ]
	*/
	function get_data_by_cpe( $tipoCpe, $cola, $dataXml )
	{
		switch ($tipoCpe) 
		{
			case '20':
						$tb_emisor_ws =  $cola->{"url_servicio_retenciones"} ? $cola->{"url_servicio_retenciones"}  : $cola->{"tb_emisor_ws"};
						$tipoMoneda = $dataXml['datos_globales']['cpe_cod_mone'];
				break;
			case '40':
						$tb_emisor_ws =  $cola->{"url_servicio_retenciones"} ? $cola->{"url_servicio_retenciones"}  : $cola->{"tb_emisor_ws"};
						$tipoMoneda = $dataXml['datos_globales']['cpe_cod_mone'];
				break;
			case '09':
						$tb_emisor_ws =  $cola->{"url_servicio_guias"} ? $cola->{"url_servicio_guias"}  : $cola->{"tb_emisor_ws"};
						$tipoMoneda = NULL;
						if ( $dataXml !== false ) 
						{
							$tipoMoneda = $dataXml['detalle_cpe'][''];
						}
				break;
			default:
					$tb_emisor_ws =  $cola->{"tb_emisor_ws"};
					$tipoMoneda = $dataXml['detalle_cpe']['codigoTipoMoneda'];
				break;
		}
		return ['tipoMoneda' => trim($tipoMoneda), 'tb_emisor_ws' => trim($tb_emisor_ws) ];
	}
}
if ( !function_exists( 'get_ws' ) ) 
{
	/**
	* retorna cierta informacion por tipo de cpe [ 'tipoMoneda' ]
	*/
	function getWsByType( $tipoCpe, $cola )
	{
		switch ($tipoCpe) 
		{
			case '20':
						$tb_emisor_ws =  $cola["url_servicio_retenciones"] ? $cola["url_servicio_retenciones"]  : $cola["tb_emisor_ws"];
				break;
			case '40':
						$tb_emisor_ws =  $cola["url_servicio_retenciones"] ? $cola["url_servicio_retenciones"]  : $cola["tb_emisor_ws"];
				break;
			case '09':
						$tb_emisor_ws =  $cola["url_servicio_guias"] ? $cola["url_servicio_guias"]  : $cola["tb_emisor_ws"];
				break;
			default:
					$tb_emisor_ws =  $cola["tb_emisor_ws"];
				break;
		}
		return $tb_emisor_ws;
	}
}
if ( !function_exists( 'get_config' ) ) 
{
	/**
	* retorna cierta informacion por tipo de cpe [ 'tipoMoneda' ]
	*/
	function get_config()
	{
		include $_SERVER[ 'DOCUMENT_ROOT' ]. "/ws/Config/config.php";
		return $config;
	}
}

if ( !function_exists( 'read_log_cola' ) ) 
{
	/**
	* retorna cierta informacion por tipo de cpe [ 'tipoMoneda' ]
	*/
	function read_log_cola( $tb_comprobante_id ,$type = 'r')
	{
		
			$directorio_cola =  $_SERVER[ 'DOCUMENT_ROOT' ]. "/ws/colaenviocpe/";
			$file_cola =$directorio_cola. "cola.json";
			if( file_exists("$directorio_cola") == true )
			{
				if ( !file_exists($file_cola) ) 
				{
					$fp = fopen($file_cola, "w");
					fwrite($fp,$tb_comprobante_id . PHP_EOL);
					fclose($fp);
				}
			    else{
			    	
			    	$fp = fopen($file_cola, "r+");
			    	$colas =trim(  fread($fp, 4096) );

			    	if ( $type == 'r' ) 
			    	{
				    	if ( strpos($colas, strval($tb_comprobante_id))  !== false) 
					    {
					    	$mensaje_cola =  "esta ocupado : ".  $tb_comprobante_id;
					    	write_log( $mensaje_cola,1 );
					    	exit;
					    }else{
					    	$strCola = ','.$tb_comprobante_id;		    	
					    	fwrite($fp, $strCola);
					    	flock($fp, LOCK_UN);    //Unlock File
					    	var_dump($strCola);
				    	}
				    	fclose($fp);
			    	}else
			    	{
			    		$escribir = fopen($file_cola, 'w');
						$strColas = str_replace(','.$tb_comprobante_id, '', $colas);
						fwrite($escribir, $strColas);
						flock($escribir, LOCK_UN);    //Unlock File
				    	fclose($escribir);
			    	}
			    }
			}
			
			
			
	}
}


/**
* Escribe un mensaje de error 
* @param $cadena es el mensaje 
* @param $tipo es el tipo de error  0 = error , 1 = afirmacion
*/
if (  ! function_exists( 'write_log_job' )  ) {
	function write_log_job($cadena, $tipo = 0)
        {
        
            $hoy = $tipo ? date("Y_m_d").".txt" : "error_" . date("Y_m_d").".txt";
            $fileAll = "jobs-all.log";
        	// $nombre_directorio =  "/var/www/html/ws_txt_json/Logs/";
            $nombre_directorio =  $_SERVER[ 'DOCUMENT_ROOT' ]. "/ws/jobs/logs";
            if( file_exists("$nombre_directorio") == true ){
                    $fp = fopen("$nombre_directorio/".$hoy, "a+");
                    $fpAll = fopen("$nombre_directorio/".$fileAll, "a+");
            }
            else{
                    $directotio = mkdir("$nombre_directorio", 0777);
                    $fp = fopen("$nombre_directorio/".$hoy, "w");
                    $fpAll = fopen("$nombre_directorio/".$fileAll, "w");
            }
            //all
        	// fwrite( $fpAll, $cadena."==============================================\n");
        	fwrite( $fpAll, " (" . date("d/m/Y H:i:s:") .round(microtime(true) * 1000). ") : " . $cadena . " \n");
        	fclose( $fpAll);

            fwrite( $fp, " (" . date("d/m/Y H:i:s:") .round(microtime(true) * 1000). ") : " . $cadena . " \n");
            fclose( $fp);
        }

}



/**
* Escribe un mensaje de error 
* @param $cadena es el mensaje 
* @param $tipo es el tipo de error  0 = error , 1 = afirmacion
*/
if (  ! function_exists( 'verifyAndDown' )  ) {
	function verifyAndDown( $url, $name_file, $formato )
    {
        // $url = 'https://storage.googleapis.com/xerox_migracion/'.$ruc.'/'.$name_file.'.'.$formato;	
		// $url = "http://178.238.228.156:8080/rest/getdocs/RECIBIDO/pdf/10321309874-RHP-E001-17";
    	$config = get_config();
		// exit;
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30 );
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");  
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		$respuesta = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		if ( $httpcode =='200' )   
		{ 
			$name_file = $name_file.'.'.$formato;
		    $path_file =$config['path_base'] .'temp/'.$name_file;
			$fp = fopen( $path_file , "w");
			fwrite( $fp, $respuesta);
	    	fclose( $fp);
			if ( filesize($path_file) > 0 ) 
			{
				return $path_file;	
			}
		}
		return false;
    }
}

if ( !function_exists( 'extract_to_zip' ) ) 
{
	/**
	* comprime un archivo en zip
	*/
	function extract_to_zip( $pathOrigen, $pathDestino )
	{
		$zip = new ZipArchive;
		if ($zip->open($pathOrigen) === TRUE) 
		{
			$zip->extractTo($pathDestino);
			$zip->close();
		}
	}
}

if ( !function_exists( 'porciones_cpe' ) ) 
{
	/**
	* comprime un archivo en zip
	*/
	function porciones_cpe( $nameFile )
	{
		$porciones = explode('-',  explode('.', $nameFile)[0] );
		return [
			$porciones[ 0 ],// ruc
			$porciones[ 1 ],// tipo_documento
			$porciones[ 2 ],// serie
			$porciones[ 3 ]// correlativo
		];
	}
}
/**
* setea la respuesta de sunat y ose
* @return Array [  responseCode, description  ]
*/
if ( !function_exists('getResponseRechazado') ) {
	function getResponseRechazado( $doc )
	{
		$result[ 'responseCode' ] = '';
		$descriptionDetail = [];
		if ( isset( $doc->getElementsByTagName('detail')->item(0)->nodeValue )   ) 
		{
			$result[ 'responseCode' ] = $doc->getElementsByTagName('faultstring')->item(0)->nodeValue;
			foreach ($doc->getElementsByTagName('detail') as $nodo_detail) 
			{	

				$descriptionDetail[] =  $nodo_detail->getElementsByTagName('message')->item(0)->nodeValue;
			}
			$result['description'] = str_replace("'"," ",implode(";",$descriptionDetail));
		}else 
		{
			// var_dump($doc);
			// exit;
			$result[ 'responseCode' ] = preg_replace('/[^0-9]+/', '', $doc->getElementsByTagName('faultcode')->item(0)->nodeValue) ;
			$result[ 'description' ] = $doc->getElementsByTagName('faultstring')->item(0)->nodeValue;
		}

		return $result;
	}
}