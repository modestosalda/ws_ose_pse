<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Providers\ProviderServiceSunatOse;
use App\Models\EmisorPSE;
use App\Models\EmisorOse;
use Sentry;
use App\Providers\Client\SendBill;
use App\Providers\Client\SendSummary;
use App\Providers\Client\GetStatusAnulacion;
use App\Jobs\UpdateAndSendCpe;
use App\Models\JobInfo;
use App\Models\ComprobantesPSE;
use App\Models\ComprobanteAllPSE;
use Illuminate\Support\Facades\Cache;
use App\Jobs\SendEmailCpeEmision;
use App\Handlers\HandlerManagementSaveCPE;
use Exception;
use App\Providers\FactoringInnovaServiceProvider;
class ComprobanteAPI extends Controller
{
    private $_comprobantePse;
    private $_providersSunatOse ;
    function __construct(){
        $this->_comprobantePse = new ComprobantesPSE();
        $this->_comprobantePseAll = new ComprobanteAllPSE();
    }

    public function sendCpeEmitidos( Request $request )
    {
    	try {
            
            // tiene que llegar el ID del comprobante
            // comprobantes de emision
            $cpeName = $request->cpe_name;
            $date = $request->date;
            $xmlSend = $request->xml;
            list( $ruc,$typeCpe ) =  porciones_cpe( $cpeName );
            if ( in_array($typeCpe , [ '01', '03', '07','08' ,'09' ,'20' ,'40' ,'13' ]) ) 
            {
                // echo "hola";
                $result = $this->cpeEmision( $request );
            }else{
                //comprobantes de anulacion 
                $result = $this->cpeAnulacion( $request );
            }
            return response()->json( $result  );
         
    	} catch (Exception $e) {
            Sentry::captureException( $e );
    		$result = [ 'status' => false , 'message' => $e->getMessage() ];
    		return response()->json( $result ,401 );
    	}
    }
    
    
    /**
    * realiza una peticion http a los servicios de sunat o ose dado el request y el tipo de documento
    */
    private function requestServiceByType( $request , $type , $nroTicket  = '')
    {
        /** Cachear */
        list( $ruc,$typeCpe ) =  porciones_cpe( $request->cpe_name  );
        $emisor =  EmisorPSE::where('tb_emisor_ruc', $ruc)->first()  ;
        /**validar da error en count*/
        if ( $emisor ) 
        {
            $cpeName = $request->cpe_name;
            $url = getWsByType($typeCpe, $emisor ) ;
            $this->_providersSunatOse = new ProviderServiceSunatOse($type, $url );
            $this->_providersSunatOse->setUser( $emisor[ 'tb_emisor_user_sol' ] );
            $this->_providersSunatOse->setPassword( $emisor[ 'tb_emisor_clave_sol' ] );
            $this->_providersSunatOse->setBase64( $request->xml );
            $this->_providersSunatOse->setFileName( $request->cpe_name . ".zip" );
            $this->_providersSunatOse->setNroTicket( $nroTicket );
            $this->_providersSunatOse->requestService();
        }else{
            throw new \Exception("El emisor con el RUC :" . $ruc . ' no existe', 1);    
        }
        
    }
    /**
    * Gestiona el envio de los comprobantes de Anulacion
    */
    private function cpeAnulacion( Request $request )
    {
        $reprocess = true;
        while ( $reprocess ) 
        {
            // crea el servicio y manda la peticion
            $this->requestServiceByType( $request, 'sendSummary' );
            // Envio la respuesta de sunat y esta clase se encarga de desenvolverlo y retornarme la respuesta en array
            $sendSummary = new SendSummary( $request->cpe_name . ".zip" );
            $result = $sendSummary->getDataXmlResponse( $this->_providersSunatOse->getResponse());

            // // $this->managementCPEAnulacion( $result );
            $reprocess = $sendSummary->getReprocess() ?? $this->_providersSunatOse->getReprocese();
        }
        $result = $this->contextCPEAnulacion( $request, $result );
        return  $result;   
        // $reprocess =  $sendSummay->getReprocess() ?? $this->_providersSunatOse->getReprocese();
    }
    /*
    *   $request informacion de documento
    *   $data respuesta del servicio Sensummary
    */
    public function contextCPEAnulacion(Request $request ,Array $data )
    {
       
        if ( isset( $data[ 'data' ][ 'nro_ticket' ] ) ) 
        {
            $reprocess = true;
            list( $ruc,$typeCpe ) =  porciones_cpe( $request->cpe_name  );
            if ( $reprocess ) 
            {
                $this->requestServiceByType( $request, 'getStatus', $data[ 'data' ][ 'nro_ticket' ] );
                // Envio la respuesta de sunat y esta clase se encarga de desenvolverlo y retornarme la respuesta en array
                $getStatusAnulacion = new GetStatusAnulacion( $request->cpe_name . ".zip" );
                $response = $getStatusAnulacion->getDataXmlResponse( $this->_providersSunatOse->getResponse(),  $request->xml, $request->date);
                $reprocess =  $getStatusAnulacion->getReprocess() ?? $this->_providersSunatOse->getReprocese();   
            }
            $dataResponse = $response['data'];
            $dataSave = $response['data'];
            $dataSave[ 'num_identificacion_cli' ] = $request->adquiriente_numero_documento_identidad;
            $dataSave[ 'fecha_emision' ] = $request->date;
            $dataSave[ 'moneda' ] = $request->codigo_tipo_moneda;
            $dataSave[ 'monto_total' ] = $request->monto_total;
            $dataSave[ 'cpe_name' ] = $request->cpe_name;
            $this->_comprobantePse->updateCompobanteByJob( $dataSave );
            $this->_comprobantePseAll->updateCompobanteByJob( $dataSave );

            if ( in_array(['A','O'], $dataResponse[ 'responseService' ]) ) 
            {
                $handlerManagementSaveCPE = new HandlerManagementSaveCPE();
                //pendiente de desarrollar
                $handlerManagementSaveCPE->saveAnulacionCPE( $request->json, $typeCpe, $comprobante_id , $nombreCpe  );
            }
            $result = [ 'status' => true , 'message' => 'El comprobante se envio correctamente' , 
                'response_service' => [
                    'description' => $dataResponse[ 'description' ],
                    'response_service' => get_name_status_by_respe( $dataResponse[ 'responseService' ]) ,
                    'status_code ' => $dataResponse[ 'responseCode' ],
                    'observation_service' => $dataResponse[ 'obsService' ] ?? '',
                    'cdr_base64' => $getStatusAnulacion->getCDRBase64()
                ]
            ];
        }else{
            $dataResponse  = $data['data'];
            $result = [ 'status' => true , 'message' => 'El comprobante se envio correctamente' , 
                'response_service' => [
                    'description' => $dataResponse[ 'description' ],
                    'response_service' => get_name_status_by_respe( $dataResponse[ 'responseService' ]) ,
                    'status_code ' => $dataResponse[ 'responseCode' ],
                    'observation_service' => $dataResponse[ 'obsService' ] ?? ''
                    
                ]
            ];
        }
        return $result;
    }
    /*
    * Gestiona el envio de los comprobantes de Emision
    * 
    */
    private function cpeEmision( Request $request )
    {
        $reprocess = true;
        $cpeName = $request->cpe_name;
        list($ruc, $typeCpe) = porciones_cpe( $cpeName );
        $date = $request->date;
        $xmlSend = $request->xml;
        $comprobanteId =$request->comprabante_id ?? 0 ;
        while ( $reprocess == true)
        {
            // crea el servicio y manda la peticion
            $this->requestServiceByType( $request, 'sendBill' );
            // Envio la respuesta de sunat y esta clase se encarga de desenvolverlo y retornarme la respuesta en array
            $sendBill = new SendBill( $request->cpe_name . ".zip" );
            $response = $sendBill->getDataXmlResponse( $this->_providersSunatOse->getResponse(),  $request->xml, $request->date );
            $reprocess =  $sendBill->getReprocess() ?? $this->_providersSunatOse->getReprocese();
        }
        $dataResponse = $response['data'];
        $dataSave = $response['data'];
        $dataSave[ 'num_identificacion_cli' ] = $request->adquiriente_numero_documento_identidad;
        $dataSave[ 'fecha_emision' ] = $request->date;
        $dataSave[ 'moneda' ] = $request->codigo_tipo_moneda;
        $dataSave[ 'monto_total' ] = $request->monto_total;
        $dataSave[ 'cpe_name' ] = $request->cpe_name;
        $this->_comprobantePse->updateCompobanteByJob( $dataSave );
        $this->_comprobantePseAll->updateCompobanteByJob( $dataSave );
        $pathXML = get_path_comprobante(  str_replace('.zip', '',  $cpeName )  ,  'xml' ,  $date, $ruc, $typeCpe );
        $pathPDF = get_path_comprobante(  str_replace('.zip', '',  $cpeName )  ,  'pdf' ,  $date, $ruc, $typeCpe );
        $pathCDR = get_path_comprobante(  str_replace('.zip', '',  $cpeName )  ,  'cdr' ,  $date, $ruc, $typeCpe );
        //falta
        $emailJobs = [
                        'tb_comprobante_id' => $comprobanteId,
                        'name_cpe' => $request->cpe_name . ".zip",
                        // 'correo' => $jsonEntrada->correo,
                        'correo' => 'modesto.saldana@4puntocero.net',
                        'fecha_emision' => $request->date, 
                        'nro_documento_identidad' => $request->adquiriente_numero_documento_identidad, 
                        'path_file_pdf' => $pathPDF,
                        'path_file_xml' => $pathXML, 
                        'path_file_cdr' =>  $pathCDR, 
                        'response_service' => $dataResponse[ 'responseService' ],
                        'razon_social' =>  $request->adquiriente_razon_social , 
                        'nro_ticket' => false
        ];
                        /* ENVIA EL EMAIL **/

        if ( in_array( $dataResponse[ 'responseService' ] , ['A','O'] ) ) 
        {
            $handlerManagementSaveCPE = new HandlerManagementSaveCPE();
            $handlerManagementSaveCPE->saveCpeEmititos( $pathXML, $typeCpe , $comprobanteId  );
        }
        SendEmailCpeEmision::dispatch( $emailJobs );
        $result = [ 'status' => true , 'message' => 'El comprobante se envio correctamente' , 
            'response_service' => [
                'description' => $dataResponse[ 'description' ],
                'response_service' => get_name_status_by_respe( $dataResponse[ 'responseService' ]) ,
                'status_code ' => $dataResponse[ 'responseCode' ],
                'observation_service' => $dataResponse[ 'obsService' ] ?? '',
                'cdr_base64' => $sendBill->getCDRBase64()
            ]
        ];
        return $result;

    }


    /**
     * Envia el Comprobante a otro WS
     * */
    public function sendCpeToWs( Request $request )
    {
        $fileSent = [];
        $fileDontSent = [];
        $files = json_decode( str_replace("'", '"',$request->data));
        $emisorOse = new EmisorOse();
        $factoringInnovaServiceProvider = new FactoringInnovaServiceProvider();
        foreach ($files as $value) {
            $fileName  =  str_replace(".zip" , "", $value->file_name);
            $tbComprobanteId  =  str_replace(".zip" , "", $value->comprobante_id);
            try {
                
                list($ruc, $typeCpe, $serie, $correlativo) = porciones_cpe( $fileName );
                $pathFile = get_path_comprobante( $fileName ,  'xml' ,  $value->emision_date, $ruc, $typeCpe );
                
                $usuarioEmisor = $emisorOse->getUsuarioEmisorByRuc( $ruc );
                $userEmail = $usuarioEmisor ?  $usuarioEmisor->correo : null;
                $requestBody = [
                    // 'user' => 'silac63915@revutap.com',
                    'user' => $userEmail,
                    'issuingRuc' => $ruc,
                    'legalPersonRuc' => $ruc,
                    // 'clientEmail' => 'silac63915@revutap.com',
                    'clientEmail' => $userEmail,
                    'invoiceNumber' => $correlativo,
                    'coinSymbol' => 'PEN',
                    'serialNumber' => $serie,
                ];
                if ( file_exists( $pathFile ) ) 
                {
                    $fileToBase64 = base64_encode( file_get_contents( $pathFile  ) );
                    $requestBody[ "file" ] = [ "content" => $fileToBase64, 'filename' => $fileName. ".xml" ];
                }
                $factoringInnovaServiceProvider->setBody( $requestBody );
                $response = $factoringInnovaServiceProvider->sendInvoce();
                if( $response[ 'status' ] )
                    $fileSent[] = [ 'file' => $fileName, 'tb_comprobante_id' => $tbComprobanteId , 'invoice_id' => $response[ 'data' ][ 'invoiceId' ] ];
                else{
                    $fileDontSent[] = [ 'file' => $fileName, 'tb_comprobante_id' => $tbComprobanteId ];
                    \Sentry::captureMessage( json_encode($response) );
                }
            } catch (\Exception $e) {
                    $fileDontSent[] = [ 'file' => $fileName, 'tb_comprobante_id' => $tbComprobanteId  ];
                    \Sentry::captureMessage( $e->getMessage() );
            }
        }
        $this->_comprobantePse->updateStatusCpeSent( $fileSent );
        return response()->json(['status' => true, 'message' => '', 'data' => ['file_sent' => $fileSent, 'file_dont_sent' => $fileDontSent]]);
    }
}
