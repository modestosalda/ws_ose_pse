<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\EmisorOse;
use App\Models\EmisorPSE;

use App\Models\Categoria;
use App\Models\UserEmisorOSE;
use App\Models\Model_perfil;
use App\Strategies\ServiceEmailContext;
use App\Http\Requests\CreateUserAPIRequest;
use Sentry;
use Exception;

class EmisorAPI extends Controller
{	
	private $_emisorOse;
	private $_modelCategoria;
	private $_modelUsuario;
	private $_modelPerfil;
	function __construct(){
		$this->_emisorOse = new EmisorOse();
		$this->_modelCategoria = new Categoria();
		$this->_modelUsuario = new UserEmisorOSE();
		$this->_modelPerfil = new Model_perfil();
    }

    public function crearEmisorUsuario(CreateUserAPIRequest $request){
        try {
            \DB::beginTransaction();
            $status = 400;
            $data = $request->all();
            $usuario =  $this->_emisorOse->getUsuarioByUsername($data['username']);
            
            if (!$usuario) {
                $admin = $this->_modelUsuario->getAdminByRuc($data['ruc']);

                if (!$admin){
                    $registro = $this->_emisorOse->getEmisorByRuc($data['ruc']);
                    
                    if (!isset($registro['id'])) {
                        $verifyEmisor = $this->_emisorOse->getCategoriaEmisorByRuc($data['ruc']);

                        if (!$verifyEmisor) {
                            $Categoria = $this->_modelCategoria->getCategoriaByName('EF');
                            $dataEmisor = 
                            [  
                                "ruc" => $data['ruc'],
                                "descripcion" => $request->razon_social,
                                "tipo" => 1,
                                "plantilla" => 0,
                                "categoria_id" => $Categoria['Id'],
                                "categoria" => $Categoria,
                                "estatus" => 'A',
                                "padre" =>1,
                                "fecha" => date('Y-m-d H:i:s'),
                            ];
                            $insertEmisor = $this->_emisorOse->saveEmisorWithCategory( $dataEmisor );
                        }
                        $emisor = $this->_emisorOse->getCategoriaEmisorByRuc($data['ruc']);

                        $Perfil = $this->_modelPerfil->getIdPerfilbyDescripcion('EF SU');
                        if( $emisor ){
                            $password=bin2hex(random_bytes(10));
                            $dataUserEmisor['password_2'] = $password;
                            $dataUserEmisor['password']=password_hash( $password, PASSWORD_DEFAULT);
                            $dataUserEmisor['emisor_id']=$emisor['id'];
                            $dataUserEmisor['username']=$data['username'];
                            $dataUserEmisor['apellidos']=' ';
                            $dataUserEmisor['dni'] = $request->ruc;
                            $dataUserEmisor['nombres']=$request->razon_social;
                            $dataUserEmisor['correo']=$request->correo;
                            $dataUserEmisor['estado']=1;
                            $dataUserEmisor['tipo']='A';
                            $dataUserEmisor['direccion']=$request->direccion;
                            $dataUserEmisor['telefono']='';
                            $dataUserEmisor['fecha']  =date('Y-m-d H:i:s');
                            $dataUserEmisor['verificacion'] =1;
                            $dataUserEmisor['perfil']=$Perfil['id'];
                            $exclude= ['id','ruc','estatus','razon_social','descripcion','tipointegracion' ];
                            $dataUser = $this->_modelUsuario->saveUserEmisor($dataUserEmisor,$exclude);
                            /** Registro de usuario envio  */
                            $Perfil = $this->_modelPerfil->getIdPerfilbyDescripcion('EF Envio');

                            $dataUserEmisor['perfil']=$Perfil['id'];
                            $dataUserEmisor['username']=$dataUserEmisor['username'].'envio';
                            $this->_modelUsuario->saveUserEmisor($dataUserEmisor,$exclude);
                            $this->_createEmisorPseByRequest( $request );
                            $this->sendNotificationEmail( $request->correo, $password, $request->razon_social );
                            $response = ['success' => true , 'message' => 'Emisor Creado correctamente', 'data' => $dataUser];
                            $status  = 200;
                        }else{
                            $response= [ 'estado' =>500, 'msg'=>'Error al escribir en la BD' ];
                            \DB::rollBack();
                        }
                    } else {
                        $response= [ 'estado' =>409, 'msg'=>'El RUC ingresado ya está registrado' ];
                        \DB::rollBack();
                    }
                } else {
                    $response  = [ 'estado' =>409, 'msg' => 'Ya existe un usuario tipo administrador registrado' ];
                    \DB::rollBack();
                }
            } else {
                $response  = [ 'estado' =>409, 'msg'=>'Ya existe el usuario ingresado' ];
                \DB::rollBack();
            }
            \DB::commit();
            return response()->json( $response, $status );
        } catch (\Exception $e) {
            \DB::rollBack();
            return response()->json($e->getMessage(), 400 );
        }   
    }
    private function _createEmisorPseByRequest( $request ){
       $emisorPse = new EmisorPSE();
       $emisorPseByRuc = $emisorPse->getEmisorByRuc( $request->ruc );

       if ( !$emisorPseByRuc ) 
       {
            $data = [ 
                'tb_emisor_ruc' => $request->ruc, 
                'tb_emisor_ws' => 'https://e-factura.sunat.gob.pe/ol-ti-itcpfegem/billService', 
                'tb_emisor_user' => 'MODDATOS',
                'tb_emisor_key'  => ' ',
                'tb_emisor_plantilla' =>'1',
                'tb_emisor_pass' => 'MODDATOS', 
                'tb_emisor_user_sol' => 'MODDATOS', 
                'tb_emisor_clave_sol' => 'MODDATOS', 
                'tb_emisor_tipo_operador' => 0,  
                'url_servicio_retenciones'=> 'https://e-factura.sunat.gob.pe/ol-ti-itemision-otroscpe-gem/billService',  
                'url_servicio_guias' => 'https://e-guiaremision.sunat.gob.pe/ol-ti-itemision-guia-gem/billService',
                'tb_emisor_nombres' => $request->razon_social,
                'tb_emisor_nombrecomercial' => $request->razon_social, 
                'tb_emisor_codigopais' => 'PE', 
                'tb_emisor_ubigeo' => $request->ubigeo, 
                'tb_emisor_departamento' => $request->dpto_name, 
                'tb_emisor_provincia' => $request->provincia_name, 
                'tb_emisor_distrito'=> $request->distrito_name, 
                'tb_emisor_urbanizacion' => $request->ubanizacion_name
            ];
            $emisorPse->createEmisorPSE( $data );
       }
       

    }
    private function sendNotificationEmail( $email, $password, $razonSocial )
    {
        $sender = ['name' => '4puntocero', 'email' => 'miscomprobantes@yastaintegrado.pe' ];
        $subject='Notificación de registro de usuario emisor';
        $serviceEmailContext = new ServiceEmailContext(  env('SERVICE_EMAIL_COLA_ENVIO', 'sendingblue'));
        $body = 'Bienvenido a la plataforma de Facturacion electronica.<br><b>EMPRESA $razonSocial</b><br>';
        $body .= "Su password de inicio de session es: ". $password;
        $providerServiceEmail = $serviceEmailContext->instaceService( $email, $sender, $subject, [] , $body );
        $responseEmail = $providerServiceEmail->sendEmail();
    }
}