<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Handlers\HandlerColaEnvio;

class ColaEnvioCpe extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cola_envio';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cola de envio de cpe';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $handlerColaEnvio = new HandlerColaEnvio();
        $res = $handlerColaEnvio->sendCpeEmitidos();
    }
}
