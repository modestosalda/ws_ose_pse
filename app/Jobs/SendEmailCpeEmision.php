<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\CorreoPlantillaPSE;
use App\Models\ComprobantesPSE;

class SendEmailCpeEmision implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $_data;
    private $_modelCorrePlantilla;
    private $_modelComprobantePSE;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(  $data )
    {
        $this->_data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $this->_modelCorrePlantilla = new CorreoPlantillaPSE();
            $this->_modelComprobantePSE = new ComprobantesPSE();
            $name_cpe = $this->_data[ 'name_cpe' ] ;// corroborar que venga con .zip
            list( $ruc,$type_cpe, $serie , $correlativo  ) = porciones_cpe( $name_cpe );
            $tb_comprobante_id = $this->_data[ 'tb_comprobante_id' ] ?? '';
            $correo = $this->_data[ 'correo' ];
            $fecha_emision = $this->_data[ 'fecha_emision' ];
            $nro_documento_identidad = $this->_data[ 'nro_documento_identidad' ];
            if ( !isset( $this->_data[ 'base64pdf' ]  ) ) 
            {   
                $this->_data[ 'base64pdf' ] = '';
                if ( file_exists( $this->_data['path_file_pdf'] ) ) 
                {
                    $this->_data[ 'base64pdf' ]  =  base64_encode(file_get_contents( $this->_data['path_file_pdf'] ));
                }
            }
            $base64pdf  = $this->_data[ 'base64pdf' ];
            $path_file_pdf = $this->_data[ 'path_file_pdf' ];
            $path_file_xml = $this->_data[ 'path_file_xml' ];
            $response_service = $this->_data[ 'response_service' ];
            $razon_social = $this->_data[ 'razon_social' ];
            $nro_ticket = $this->_data[ 'nro_ticket' ] ?? '';
            $response = $this->_modelCorrePlantilla->sendEmailCPE($name_cpe,  $correo, $fecha_emision, $nro_documento_identidad, $base64pdf, $path_file_pdf, $path_file_xml, $response_service, $razon_social, $nro_ticket ) ;
            // var_dump($response);
            $statusCodeEmail = $response[ 'status_code'  ] ?? '202';
            $prefijo = "tb_comprobante_";
            $where = [  
                $prefijo."ruc" =>  $ruc,
                $prefijo."nomextarccomele" =>  $name_cpe,
                "correlativo" =>  $correlativo,
                "serie" =>  $serie,
                "tipo_cpe" =>  $type_cpe 
            ];
            if ( $tb_comprobante_id ) 
            {
                $where[ $prefijo."id"  ] = $tb_comprobante_id;
            }
            $this->_modelComprobantePSE
            ->where( $where )
            ->update([ 'tb_comprobante_emailstaus' => $statusCodeEmail ]);

        } catch (\Exception $e) {
            \Sentry::captureException($e);
        }
       
    }
}
