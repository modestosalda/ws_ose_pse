<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\ComprobantesPSE;

class UpdateAndSendCpe implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $_dataCpe;
    protected $_comprobantePSE;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $dataCpe )
    {
        $this->_dataCpe = $dataCpe;
        $this->_comprobantePSE = new ComprobantesPSE();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
  //        ["description"]=>
  // string(49) "La Factura numero F053-00016486, ha sido aceptada"
  // ["responseCode"]=>
  // string(1) "0"
  // ["responseService"]=>
  // string(1) "A"
  // ["obsService"]=>
  // string(112) "3244 - Debe consignar la informacion del tipo de transaccion del comprobante - INFO : 3244 (nodo: "/" valor: "")"
  // ["desexcService"]=>
  // string(0) ""
  // ["sinProcesar"]=>
  // bool(false)
  // ["fecha_emision"]=>
  // string(10) "2020-01-01"
  // ["moneda"]=>
  // string(2) "PE"
        // "cpe_name"
  // ["monto_total"]=>
        
        $this->_comprobantePSE->updateCompobanteByJob( $this->_dataCpe );
        // echo ($this->_comprobante->num_identificacion_cli);
        // var_dump( $this->_comprobante );
        // echo "estoy aqui";
    }
}
