<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\CorreoPlantillaPSE;
use App\Models\ComprobantesPSE;

class SendEmailCpeEmision implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $_data;
    private $_modelCorrePlantilla;
    private $_modelComprobantePSE;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(  $data )
    {
        $this->_data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       
       
    }
}
