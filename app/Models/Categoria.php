<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
/**
 * Modelo para Emisores PSE
 * Created By Modesto Saldaña Michalec<modesto_salda_m93@gmail.com>
 */

class Categoria extends Model
{
	protected $connection = 'mysql';
	public $table = 'ose_categoria';

	/**
	* @return retorna la categoria segun el nombre
	*/
	public function getCategoriaByName($name){
		$result = $this->where('descripcion',$name)->get()->toArray();

		return $result? $result[0]: [];
	}


}

