<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
/**
 * Model Comprobante
 */
class ComprobantesPSE extends Model
{
    // const TABLE_NAME = 'sh_cloud_pse.vw_codigobarra_comprobante';
    public $table = 'sh_cloud_pse.tb_comprobante';
    // const TABLE_NAME = 'sh_cloud_pse.tb_comprobante';
    const TB_RELATION_LEFT = [  "tb_nc_cab" => " LEFT JOIN sh_cloud_cpe.tb_nc_cab cpe ON split_part( pse.tb_comprobante_nomextarccomele, '.' :: TEXT, 1 ) = ( cpe.cpe_num_ruc :: TEXT || '-' || cpe.modif_tipo_doc :: TEXT || '-' || cpe.modif_serie_corre :: TEXT ) " ];
     public $dataMask = [
      'rucemi' => 'tb_comprobante_ruc',
      'tipo_doc' => 'tipo_cpe',
      'num_doc' => 'tb_comprobante_nomextarccomele',
      'monto' => 'tb_comprobante_total',
      'estadostr' => 'tb_comprobante_desres',
      // 'fechadoc' => 'tb_comprobante_fecha_emision',
      'email_cpe' => 'tb_comprobante_emailto',
      'numeroidentificacioncliente'=> "tb_comprobante_num_identificacion_cli",
      // 'emailto_status' => 'tb_comprobante_emailstaus',
    ];



    /**
     * tb_comprobante_emailstatus container
     *
     * @var string
     */
    private $tb_comprobante_emailstatus;

    /**
     * @return string
     */
    public function getTbComprobanteEmailstatus()
    {
        return $this->tb_comprobante_emailstatus;
    }

    /**
     * @param string $tb_comprobante_emailstatus
     *
     * @return self
     */
    public function setTbComprobanteEmailstatus( $tb_comprobante_emailstatus )
    {
        $this->tb_comprobante_emailstatus = $tb_comprobante_emailstatus;

        return $this;
    }

    /**
    * Actualiza el comprobante dado la comunicacion de baja, resumend diario y resumen de reversion
    * @param string $nameFile = nombre del comprobante "20517182673-01-F102-00003064.zip"
    * @param string $tipo_documento = Tipo de documento ( 01,03,06 ) 
    * @param string $status = estado a cambiar
    */
    public function ActualizarComprobanteByStatusAndNameFile( $nameFile, $tipo_documento, $status  )
    {
        $arrayFile = explode('-', $nameFile);
        $ruc = $arrayFile[0];
        $serie = $arrayFile[2];
        $correlativo = $arrayFile[3];
        $pg_script = "update " . self::TABLE_NAME . " 
                                    set tb_comprobante_respre= '" .  $status . "', tb_comprobante_es_aceptado='3' 
                                    where 
                                      tb_comprobante_ruc='" .$ruc. "'
                                      and serie='". $serie ."'
                                      and correlativo like '%".$correlativo."%' ";
        $result = $this->InsertOrUpdate( $pg_script );
        
        return $result;
    }

    public function getComprobanteByNameFileZip( $nameFile )
    {
        $file = str_replace('.zip', '', $nameFile) ;
        $script = "select *
                      from sh_cloud_pse.vw_codigobarra_comprobante 
                      where  ruc || '-' || tipocomprobante || '-' || seriecomprobante || '-' || numerocomprobante= '" . $file . "' ";
        $result = $this->queryPersonalizate( $script );
        return $result;
    }
    /**
    * @param string $nameFile = nombre del comprobante "20517182673-01-F102-00003064.zip"
    */
    public function getComprobanteByNameZip( $nameFile , 
                                             Array $orderBy = [ 'order' => 'desc', 'orderBy' => 'tb_comprobante_fechorpub' ] , 
                                             $limit = 0 )
    {
        $script = "select *
                      from sh_cloud_pse.vw_codigobarra_comprobante 
                      where  tb_comprobante_nomextarccomele= '" . $nameFile . "' " ;

        // $script .= " order by " . $orderBy[ 'orderBy' ] . " " . $orderBy[ 'order' ];
        $result = $this->queryPersonalizate( $script );
        return $result;
    }
    /**
    * 
    */
    public function getComprobanteByRucAndSerieAndCorrelAndTipoCpe( $ruc, $serie, $correlativo, $tipoCpe )
    {
      $script = "SELECT * FROM " .self::TABLE_NAME ;
      $script .= " WHERE tipo_cpe = '" .$tipoCpe."'  and tb_comprobante_ruc = '" .$ruc. "' and serie='"  . $serie . "' and correlativo like '%" . $correlativo . "%'";
      $script .= " order by 1 desc limit 1" ;
      $result = $this->queryPersonalizate( $script );
      return $result;
    }
    /**
    * 
    */
    public function getComprobanteById( $id )
    {
        $script = "select *
                      from sh_cloud_pse.tb_comprobante 
                      where  tb_comprobante_id= '" . $id . "' order by tb_comprobante_fechorpub desc ";
        $result = $this->queryPersonalizate( $script );
        return $result;
    }

   
    /**
    * @return retorna la lista de comprobantes dado el tipo de consulta RD, RR , RA
    */
    public function getComprobantesByTipoConsulta( $emi_ruc , $doc_tipo, $doc_serie, $correlativo , $doc_fechini, $doc_fechfin , $tipo_consulta )
    {

      $pgCad = "SELECT DISTINCT ON
                ( tb_comprobante_nomextarccomele ) tb_comprobante_nomextarccomele :: TEXT,
                ruc AS rucemi,
                temi.tb_emisor_nombres AS rsocial,
                fecha :: TEXT AS fechadoc,
                tipocomprobante tipo_doc,
                ( seriecomprobante || '-' || numerocomprobante ) AS num_doc,
                total AS monto,
                tb_comprobante_desres AS estado_cpe,
                tb_comprobante_emailto email_cpe,
                tb_comprobante_emailstaus AS emailto_status,
                tb_comprobante_nomextarccomele AS filezip,
                tb_comprobante_respre AS respre,
                cperr.tb_reverdeta_identif :: TEXT AS identifrr,
                cpecb.tb_comudeta_identif :: TEXT AS identifcb,
                cpenc.cpe_num_ruc || '-' || cpenc.tipo_nc || '-' || cpenc.cpe_num_serie || '-' || cpenc.cpe_num_correl AS identifnc,
                cperd.tb_resumdeta_identif :: TEXT AS identifrd 
            FROM
                sh_cloud_pse.tb_emisor temi
                INNER JOIN sh_cloud_pse.vw_codigobarra_comprobante pse ON temi.tb_emisor_ruc = pse.ruc
                LEFT JOIN sh_cloud_cpe.tb_comunicacionbaja_detalle cpecb ON split_part( pse.tb_comprobante_nomextarccomele, '.' :: TEXT, 1 ) = cpecb.tb_comudeta_nombrecpe
                LEFT JOIN sh_cloud_cpe.tb_resumenreversiones_det cperr ON split_part( pse.tb_comprobante_nomextarccomele, '.' :: TEXT, 1 ) = cperr.tb_reverdeta_nombrecpe
                LEFT JOIN sh_cloud_cpe.tb_resumendiario_detalle cperd ON split_part( pse.tb_comprobante_nomextarccomele, '.' :: TEXT, 1 ) = cperd.tb_resumdeta_nombrecpe
                LEFT JOIN sh_cloud_cpe.tb_nc_cab cpenc ON split_part( pse.tb_comprobante_nomextarccomele, '.' :: TEXT, 1 ) = (
                cpenc.cpe_num_ruc :: TEXT || '-' || cpenc.modif_tipo_doc :: TEXT || '-' || cpenc.modif_serie_corre :: TEXT)
                where 1=1  and  tb_comprobante_respre <> 'B'  and tb_comprobante_respre <> 'R' ";
        if ( $emi_ruc ) 
        {
          $pgCad .= " AND ruc='" . $emi_ruc  . "'";
        }
        if ( $doc_serie ) 
        {
          $pgCad .= " AND seriecomprobante='" . $doc_serie . "' ";
        }
        if ( $correlativo ) 
        {
          $pgCad .= " AND numerocomprobante LIKE'%" . $correlativo . "%' ";
        }
        if ( $doc_fechini && $doc_fechfin ) 
        {
            $pgCad .= " and (fecha::date>='" . $doc_fechini . "'::date and fecha::date<='" . $doc_fechfin . "'::date) ";
        }
        switch ( $tipo_consulta ) 
        {
            case 'CB':
                    $pgCad .= $doc_tipo  ? "  AND pse.tipocomprobante = " . $doc_tipo : "  AND pse.tipocomprobante IN ('01','08','07')";

                break;
            case 'RR':
                    $pgCad .=  $doc_tipo  ? "  AND pse.tipocomprobante = " . $doc_tipo : "  AND pse.tipocomprobante IN ('20','40')" ;
                    
                break;
            case 'RD':
                    $pgCad .=  $doc_tipo  ? "  AND pse.tipocomprobante = " . $doc_tipo : "  AND pse.tipocomprobante IN ('03','07', '08')" ;
                    
                break;
        }
        $result = $this->queryPersonalizate( $pgCad );
        return $result;
    }
    /**
    * Actualiza la tabla tb_comprobnte desde las colas de sendsumary ( RR, RA, RC  )
    */
    public function updateEstadoCpeSendsummary( $nombre_cpe,$codigo_retorno,$msj_retorno,$nro_ticket, $nroTicketFex = null , $comprobante_id = null)
    {

        if ( $codigo_retorno != 'null' ) 
        {
            $msj_retorno = str_replace( "'" , "", $msj_retorno );
            $codigo_retorno =  intval( str_replace( "'" , "", $codigo_retorno ) );
            $script= "UPDATE sh_cloud_pse.tb_comprobante 
            SET tb_comprobante_codres= '$codigo_retorno' ,
              tb_comprobante_respre = 'R',
              tb_comprobante_desres = '$msj_retorno', 
              tb_comprobante_desexc =  '$msj_retorno',
              tb_comprobante_ticket = null 
              WHERE tb_comprobante_nomextarccomele =  '$nombre_cpe'  ";
              // if ( $comprobante_id != null ) 
              // {
                $script .= " and tb_comprobante_id= '" . $comprobante_id . "' ";
              // }
              $result = $this->InsertOrUpdate( $script );

              //actualiza tb_comprobante_all
                $script= "UPDATE sh_cloud_pse.tb_comprobante_all
            SET tb_comprobante_codres= '$codigo_retorno' ,
              tb_comprobante_respre = 'R',
              tb_comprobante_desres = '$msj_retorno', 
              tb_comprobante_desexc =  '$msj_retorno'

              WHERE tb_comprobante_nomextarccomele =  '$nombre_cpe' ";

                $result = $this->InsertOrUpdate( $script );
        }
         else 
         {
             $script= "UPDATE sh_cloud_pse.tb_comprobante 
                   SET tb_comprobante_codres= null , 
                   tb_comprobante_desres = null,
                   tb_comprobante_respre = null,
                   tb_comprobante_desexc = null,
                   tb_comprobante_ticket = '$nro_ticket' 
                   WHERE tb_comprobante_nomextarccomele =  '$nombre_cpe' ";
            // if ( $comprobante_id != null ) 
            // {
              $script .= " and tb_comprobante_id= '" . $comprobante_id . "'";
            // }
             $result = $this->InsertOrUpdate( $script );
        }
        // echo $script;
    }
    /**
    * @return Retorna los comprobantes listos para enviar a sendsumary
    * @param $type tipo de comprobante EJ: "RC", "RR", "RA"
    * @param $is_null_ticket 
    * @param $limit si es = 0; entonces no hay limites
    */
    function obtenerBase64ZipByTypeDocument( Array $type, Bool $is_null_ticket = false, $limit = 0 )
    {
        $script  = "SELECT 
                  tb_comprobante_base64zip,
                    tb_comprobante_id,
                    tb_comprobante_nomextarccomele,
                    tb_comprobante_codbar,
                    tb_comprobante_ticket,
                    tb_emisor_ws,
                    tb_comprobante_ruc,
                    tipo_cpe,
                    tb_emisor_user_sol,
                    tb_emisor_clave_sol,
                    tb_emisor_tipo_operador ,
                    nroticket
                    from sh_cloud_pse.tb_comprobante,sh_cloud_pse.tb_emisor
                    where             
                    tb_comprobante_base64cdr is null
                    --and tb_comprobante_codbar = ''
                    and tb_comprobante_respre is null
                    and tb_comprobante_codres is null
                    and tb_comprobante_desres is null
                     
                    and tb_comprobante_fechorpub >= '2019-11-12 00:00:00.727823'
                    and tb_comprobante_ruc = tb_emisor_ruc
                    
                  " ;//and  tb_comprobante_nomextarccomele  like '%RC%'
        if ( count($type) ) 
        {
          foreach ($type as  $typeItem) 
          {
              $script .= " and  tipo_cpe = '". $typeItem ."' ";
          }
        }
        $script.= ' and es_migrado is null ';
        $script.= $is_null_ticket ? " and tb_comprobante_ticket is null " : " and tb_comprobante_ticket is not null ";
        $script.= $limit>0 ? " limit " . $limit : "";

        $result = $this->queryPersonalizate( $script );
        return $result;
    }
    /**
    * $filters Filtros obtenido con anterioridad de la funcion getWithFilters()
    */
    public function getCPEExcel( $filters )
    {
      $replaceObservacion =" REPLACE(tb_comprobante_obsres, '" . '"' . "', '''')";
      $replaceEstado =" REPLACE(tb_comprobante_desres, '" . '"' . "', '''')";
      $script = "SELECT
                DISTINCT ON
                ( tb_comprobante_nomextarccomele ) tb_comprobante_nomextarccomele :: TEXT,
               -- string_agg( DISTINCT  ( tb_doc_referenciados.tipo_documento::text || '-' || tb_doc_referenciados.serie::text || '-' || tb_doc_referenciados.correlativo::text), ', ' )::TEXT AS num_doc_referenciado,
               '-' as num_doc_referenciado,
               -- string_agg( DISTINCT doc_relacionado.tipo_doc_relac::text || '-' || doc_relacionado.serie_doc_relac::text || '-' || doc_relacionado.correlativo_relac::text,', ')::text as num_relacionado,
                '-' as num_relacionado ,
                tb_comprobante_ruc AS ruc,
                tb_comprobante_id,
                pse.tipo_cpe as  tipo_doc,
                pse.serie as seriecomprobante,
                pse.correlativo AS numerocomprobante,
                pse.tb_comprobante_monper,
                split_part(pse.tb_comprobante_codbar, '|'::text, 5) AS igv,
                split_part(pse.tb_comprobante_codbar, '|'::text, 6) AS monto,
                split_part(pse.tb_comprobante_codbar, '|'::text, 7) :: TEXT AS fechadoc,
                split_part(pse.tb_comprobante_codbar, '|'::text, 8) AS tipoidentificacioncliente,
                split_part(pse.tb_comprobante_codbar, '|'::text, 9) as numeroidentificacioncliente,
                tb_comprobante_ruc AS rucemi,
                temi.tb_emisor_nombres AS rsocial,
                ( pse.serie || '-' || pse.correlativo ) AS num_doc,
                " . $replaceObservacion . " AS estado_cpe,
                tb_comprobante_emailto email_cpe,
                tb_comprobante_emailstaus AS emailto_status,
                tb_comprobante_nomextarccomele AS filezip,
                tb_comprobante_respre AS respre,
                null as identif,
                  null as identifcb,
                  null as identifnc,
                  null as fechanc,
                 --cpenc.cpe_num_ruc || '-07-' || cpenc.cpe_num_serie || '-' || cpenc.cpe_num_correl AS identifnc,
                tb_comprobante_codres AS codres,
                " . $replaceEstado . " AS obsres,
                tb_comprobante_moneda AS moneda,

                tb_razon_social :: TEXT
                
                FROM
                sh_cloud_pse.tb_emisor temi
                INNER JOIN sh_cloud_pse.tb_comprobante pse ON temi.tb_emisor_ruc = pse.tb_comprobante_ruc
              --LEFT JOIN sh_cloud_cpe.tb_comunicacionbaja_detalle cpecb ON split_part( pse.tb_comprobante_nomextarccomele, '.' :: TEXT, 1 ) = cpecb.tb_comudeta_nombrecpe -- left join sh_cloud_pse.tb_doc_referenciados on pse.serie = tb_doc_referenciados.serie_doc_relac-- and pse.correlativo = tb_doc_referenciados.correlativo_relac-- and pse.tb_comprobante_ruc = tb_doc_referenciados.ruc::text-- LEFT JOIN sh_cloud_pse.tb_doc_referenciados as doc_relacionado ON pse.serie = doc_relacionado.serie-- and pse.correlativo = doc_relacionado.correlativo-- and pse.tb_comprobante_ruc = doc_relacionado.ruc::text
              --  LEFT JOIN sh_cloud_cpe.tb_resumendiario_detalle cperd ON split_part( tb_comprobante_nomextarccomele, '.' :: TEXT, 1 ) = cperd.tb_resumdeta_nombrecpe
              --LEFT JOIN sh_cloud_cpe.tb_nc_cab cpenc ON split_part( pse.tb_comprobante_nomextarccomele, '.' :: TEXT, 1 ) = ( cpenc.cpe_num_ruc :: TEXT || '-' || cpenc.modif_tipo_doc :: TEXT || '-' || cpenc.modif_serie_corre :: TEXT ) 
                WHERE
                  1 = 1 " ;
      $script.= $filters;
      
      $script .= "GROUP BY 
                  tb_comprobante_nomextarccomele,tb_comprobante_ruc,tb_comprobante_id,tipo_cpe,pse.serie ,pse.correlativo
                  ,igv,monto,fechadoc,tipoidentificacioncliente,tb_comprobante_monper,numeroidentificacioncliente,rucemi,rsocial,estado_cpe,email_cpe,emailto_status,respre,identif,identifcb,identifnc,fechanc, codres,obsres,tb_razon_social,tb_comprobante_moneda";
      $script .= " ORDER BY  tb_comprobante_nomextarccomele asc,tb_comprobante_respre asc, tb_comprobante_id desc ";
      // echo $script;
      // exit;
      $result = $this->queryPersonalizate( $script );
      return $result;

    }
    /**
    * $filters Filtros obtenido con anterioridad de la funcion getWithFilters()
    */
    public function fb_search_cpe( $filters,$searchValue='' , $limit = 10, $offset = 0, $sortBy = "", $sortDir = "asc")
    {
      $replaceObservacion =" REPLACE(tb_comprobante_obsres, '" . '"' . "', '''')";
      $replaceEstado =" REPLACE(tb_comprobante_desres, '" . '"' . "', '''')";
      $script = "SELECT
                DISTINCT ON
                ( tb_comprobante_nomextarccomele ) tb_comprobante_nomextarccomele :: TEXT,
               -- string_agg( DISTINCT  ( tb_doc_referenciados.tipo_documento::text || '-' || tb_doc_referenciados.serie::text || '-' || tb_doc_referenciados.correlativo::text), ', ' )::TEXT AS num_doc_referenciado,
               '-' as num_doc_referenciado,
               -- string_agg( DISTINCT doc_relacionado.tipo_doc_relac::text || '-' || doc_relacionado.serie_doc_relac::text || '-' || doc_relacionado.correlativo_relac::text,', ')::text as num_relacionado,
                '-' as num_relacionado ,
                tb_comprobante_ruc AS ruc,
                tb_comprobante_id,
                pse.tipo_cpe as  tipo_doc,
                pse.serie as seriecomprobante,
                pse.correlativo AS numerocomprobante,
                split_part(pse.tb_comprobante_codbar, '|'::text, 5) AS igv,
                tb_comprobante_total monto,
                tb_comprobante_fecha_emision fechadoc,
                split_part(pse.tb_comprobante_codbar, '|'::text, 8) AS tipoidentificacioncliente,
                tb_comprobante_num_identificacion_cli numeroidentificacioncliente,
                tb_comprobante_ruc AS rucemi,
                temi.tb_emisor_nombres AS rsocial,
                ( pse.serie || '-' || pse.correlativo ) AS num_doc,
                " . $replaceObservacion . " AS estado_cpe,
                tb_comprobante_emailto email_cpe,
                tb_comprobante_emailstaus AS emailto_status,
                tb_comprobante_nomextarccomele AS filezip,
                tb_comprobante_respre AS respre,
                null as identif,
                  null as identifcb,
                  null as identifnc,
                  null as fechanc,
                --  cperd.tb_resumdeta_identif AS identif,
                  --cpecb.tb_comudeta_identif AS identifcb,
                  --cpenc.cpe_num_ruc || '-07-' || cpenc.cpe_num_serie || '-' || cpenc.cpe_num_correl AS identifnc,
                  --cpenc.cpe_fecha :: TEXT AS fechanc,
                tb_comprobante_codres AS codres,
                " . $replaceEstado . " AS obsres,
                case 
                when tb_comprobante_moneda = 'PEN'
                then 'SOLES'
                when tb_comprobante_moneda = 'USD'
                then 'DOLARES'
                else tb_comprobante_moneda
                end
                AS moneda,

                tb_razon_social :: TEXT
                
                FROM
                sh_cloud_pse.tb_emisor temi
                INNER JOIN sh_cloud_pse.tb_comprobante pse ON temi.tb_emisor_ruc = pse.tb_comprobante_ruc
              --LEFT JOIN sh_cloud_cpe.tb_comunicacionbaja_detalle cpecb ON split_part( pse.tb_comprobante_nomextarccomele, '.' :: TEXT, 1 ) = cpecb.tb_comudeta_nombrecpe -- left join sh_cloud_pse.tb_doc_referenciados on pse.serie = tb_doc_referenciados.serie_doc_relac-- and pse.correlativo = tb_doc_referenciados.correlativo_relac-- and pse.tb_comprobante_ruc = tb_doc_referenciados.ruc::text-- LEFT JOIN sh_cloud_pse.tb_doc_referenciados as doc_relacionado ON pse.serie = doc_relacionado.serie-- and pse.correlativo = doc_relacionado.correlativo-- and pse.tb_comprobante_ruc = doc_relacionado.ruc::text
              --  LEFT JOIN sh_cloud_cpe.tb_resumendiario_detalle cperd ON split_part( tb_comprobante_nomextarccomele, '.' :: TEXT, 1 ) = cperd.tb_resumdeta_nombrecpe
              --LEFT JOIN sh_cloud_cpe.tb_nc_cab cpenc ON split_part( pse.tb_comprobante_nomextarccomele, '.' :: TEXT, 1 ) = ( cpenc.cpe_num_ruc :: TEXT || '-' || cpenc.modif_tipo_doc :: TEXT || '-' || cpenc.modif_serie_corre :: TEXT ) 
                WHERE
                  1 = 1 " ;
      $script.= $filters;
      if ( !empty( $searchValue ) )
      {
        $columnsToSearch = array_values( $this->dataMask );
        $script .= ' and (';
        foreach ( $columnsToSearch as $column )
        {
          $script .= ' ' . $column . ' like ' . "'%" . $searchValue . "%'"  . ' or ';
        }
        $script = substr( $script, 0, -3 );
        $script .= ")";
      }
      if ( $sortBy == 'monto' )
      {
        $sortBy = ' cast( cast( monto as numeric(10,4) )  AS INT) ';
      }
      $script .= "GROUP BY 
                  tb_comprobante_nomextarccomele,tb_comprobante_ruc,tb_comprobante_id,tipo_cpe,pse.serie ,pse.correlativo
                  ,igv,monto,fechadoc,tipoidentificacioncliente,numeroidentificacioncliente,rucemi,rsocial,estado_cpe,email_cpe,emailto_status,respre,identif,identifcb,identifnc,fechanc, codres,obsres,tb_razon_social, tb_comprobante_moneda";
      $script .= " ORDER BY  tb_comprobante_nomextarccomele asc,tb_comprobante_respre asc, tb_comprobante_id desc ";
      $script_generados_pse = " select * from  (" .$script .") as generados_pse";
      if ( !empty( $sortBy )  ) 
      {
        $script_generados_pse .= " ORDER BY " . $sortBy . "  " .  $sortDir ;
      }
      if ( $limit >0 ) 
      {
        $script_generados_pse  .= " LIMIT " . $limit ;
      }
      if ( $offset > 0 )
      {
        $script_generados_pse  .= " offset " . $offset ;
      }

      // echo $script_generados_pse;
      // exit;
      $result = $this->queryPersonalizate( $script_generados_pse );
      return $result;

  }
  /*
  * realiza el filtrado necesario para las diferentes consultas en la tabla comprobantes
   *$doc_tipo tipo del comprobante "01", "03"
    * $status_email estado del email
    * $allFilter tiene todos los filtros
  */
  public function getWithFilters( $ruc , $doc_tipo, $doc_serie, $correlativo , $fecha_ini, $fecha_fin , $estados, $num_doc, $status_email,Array $allFilter )
  {
    // $aux_estado = $estado;
    $estados_implode =   implode(",", $estados)  ;
    $doc_tipo =   implode(",", $doc_tipo)  ;
    $script = " ";
    if ( $ruc ){
      $script .= " AND tb_comprobante_ruc='" . $ruc  . "'";
    }
    if ( $allFilter['razon_social_adquiriente'] ) 
    {
      $script .= " AND LOWER(pse.tb_razon_social) LIKE LOWER('%" .  $allFilter['razon_social_adquiriente']  . "%') ";
    }
    if ( $doc_tipo ){
      $script .= " AND pse.tipo_cpe in (" . $doc_tipo . ") ";
    }
    if ( $doc_serie ) 
    {
      $script .= " AND pse.serie='" . $doc_serie . "' ";
    }
    if ( $correlativo ) 
    {
      $script .= " AND pse.correlativo LIKE'%" . $correlativo . "%' ";
    }

    if (  $fecha_ini and $fecha_fin ) 
    {
      $script .= " AND (tb_comprobante_fecha_emision >='" . $fecha_ini . "' and tb_comprobante_fecha_emision <='" . $fecha_fin . "' )";
    }elseif( !$doc_serie && !$correlativo  )
    {
      $fecha_fin = date( 'Y-m-d' );
      $fecha_ini = date( 'Y-m-d', strtotime( $fecha_fin."- 3 month" ) );
      $script .= " AND (tb_comprobante_fecha_emision >='" . $fecha_ini . "' and tb_comprobante_fecha_emision <='" . $fecha_fin . "' )";  
    }
    if ( $num_doc ) 
    {
      $script .= " AND ( pse.serie || '-' || pse.correlativo ) = '" . $num_doc . "' ";
    }
    if ( $status_email != '' ) 
    {
      if( $status_email == '0' )
        $script .= " AND pse.tb_comprobante_emailstaus IS NULL";
      else
        $script .= " AND pse.tb_comprobante_emailstaus='" . $status_email ."'";
    }
    // var_dump( in_array('NULL', $estados));
    // exit;
    if ( $estados_implode  ) 
    {
      if ( in_array('NULL', $estados) ) 
      {
          $script .= " and (tb_comprobante_respre in ( ". $estados_implode . " ) OR tb_comprobante_respre IS NULL )";
      }else
      {
        $script .= " and tb_comprobante_respre in ( ". $estados_implode . " ) ";
      }
      $script .= $this->getQueryFilterStatus( $estados );
       // si solo filtra por RECHAZADO entonces activamos la bandera si ese comprobante ya fue aceptado
       

    }
    if ( $allFilter['ruc_adquiriente']  ) 
    {
       $script .= " and tb_comprobante_num_identificacion_cli like '%". $allFilter['ruc_adquiriente'] . "%'  ";
    }
    // echo $script;
    // exit;
    return $script;
  }

  public function getQueryFilterStatus( $estados )
  {

    //$dist = distinto
    $distA = "  pse.tb_comprobante_es_aceptado <> 1";//
    $distO = "  pse.tb_comprobante_es_aceptado <> 2";
    $distB = "  pse.tb_comprobante_es_aceptado <> 3";
    $distR = "  pse.tb_comprobante_es_aceptado <> 0";

    $igualA = "  pse.tb_comprobante_es_aceptado = 1";//
    $igualO = "  pse.tb_comprobante_es_aceptado = 2";//
    $igualB = "  pse.tb_comprobante_es_aceptado = 3";//
    $igualR = "  pse.tb_comprobante_es_aceptado = 0";//
    $script = "";
    // si tiene todos los filtros trae todo.
     if (  $estados[0] == "'A'" &&  in_array("'O'", $estados) && in_array("'R'", $estados) && in_array("'B'", $estados)  )
     {
        $script .= " ";
     }elseif ( $estados[0] == "'O'" && in_array("'R'", $estados)  && in_array("'B'", $estados)  ) 
     {
       $script .= "  AND ( $igualO or $igualR or $igualB and $distA ) ";
     }
     elseif ( $estados[0] == "'O'" && in_array("'R'", $estados)  ) 
     {
       $script .= "  AND ( $igualO or $igualR or $distA   or $distB ) ";
     }
     elseif ( $estados[0] == "'R'" && in_array("'B'", $estados)   ) 
     {
       $script .= " AND   (  $distO and $distA or $igualR   or $igualB )  ";
     }
     elseif ( $estados[0] == "'R'" && in_array("NULL", $estados)    ) 
     {
       $script .= " AND   (  $distO and $distA or $igualR   or $distB )  ";
     }
     elseif ( $estados[0] == "'R'" &&  !isset( $estados[1] )    ) 
     {
       $script .= " AND   (  $distO and $distA and $distB  or $igualR    )  ";
     }
     elseif ( $estados[0] == "'A'" && in_array("'R'", $estados)  && in_array("'B'", $estados)  ) 
     {
       $script .= " AND   (  $distO or $igualA or $igualR  or $igualB )  ";
     }

      return $script;
  }
  public  function countDataTable ($searchValue = "", $filter )
  {
    // $fieldColumn =array_diff( Schema::getColumnListing($table) ,[ 'id','created_at','updated_at','deleted_at' ]);
    $table = 'SELECT DISTINCT ON
              ( tb_comprobante_nomextarccomele ) tb_comprobante_nomextarccomele :: TEXT,
              tb_comprobante_ruc,
              tipo_cpe,
              tb_comprobante_emailstaus,
              tb_comprobante_total ,
              tb_comprobante_num_identificacion_cli,
              tb_comprobante_fecha_emision,
              tb_comprobante_desres,
              tb_comprobante_emailto
              from sh_cloud_pse.tb_emisor temi
              INNER JOIN sh_cloud_pse.tb_comprobante pse ON temi.tb_emisor_ruc = pse.tb_comprobante_ruc
              where 1=1 
              ';
    $table .= $filter;
    $table .= "ORDER BY  tb_comprobante_nomextarccomele,  tb_comprobante_respre  ";

    $fieldColumn = array_values( $this->dataMask );
    $sql = 'select count( tb_comprobante_nomextarccomele ) as total from (' .  $table  .') pse';
    $sql .= ' where 0=0 ';
    if ( !empty( $searchValue ) )
    {
      $sql .= " and (";
      foreach ( $fieldColumn as $column )
      {
        $sql .= ' ' . $column . ' like ' . "'%" . $searchValue . "%'"  . ' or ';
      }

      $sql = substr( $sql, 0, -3 );
      $sql .= ")";
    }

    // echo $sql;
    // exit;
    $query = $this->queryPersonalizate( $sql );

    return $query[0]['total'];
  }
    /**
    * @return
    * @param {string} $ruc = rucn del emisor
    * @param {Array} $doc_tipo = tipo de documentos a filtrar
    * @param {string} $doc_serie =serie del documento 
    * @param {date} $fecha_ini =fecha inicio
    * @param {date} $fecha_fin =fecha fin
    * @param {array} $estados = estado a filtrar ej: ["'A'", "'O'"]
    */
    public function fb_search_cpe_ncg( $ruc , Array $doc_tipo, $doc_serie, $correlativo , $fecha_ini, $fecha_fin ,Array $estados, $num_doc , $tb_relacion_with_comprobante)
    {
      $estados = implode(',', $estados);
      $doc_tipo = implode(",", $doc_tipo);
      $script = "SELECT
                tipocomprobante,
                  ruc AS rucemi,
                  temi.tb_emisor_nombres AS rsocial,
                  fecha :: TEXT AS fechadoc,
                  tipocomprobante tipo_doc,
                  ( seriecomprobante || '-' || numerocomprobante ) AS num_doc,
                  total AS monto,
                  tb_comprobante_desres AS estado_cpe,
                  tb_comprobante_emailto email_cpe,
                  tb_comprobante_emailstaus AS emailto_status,
                  tb_comprobante_nomextarccomele AS filezip,
                  tb_comprobante_respre AS respre,
                  cpe.cpe_num_ruc || '-' || cpe.tipo_nc || '-' || cpe.cpe_num_serie || '-' || cpe.cpe_num_correl AS identif,
                  cperd.tb_resumdeta_identif :: TEXT AS identifrd,
                  cpecb.tb_comudeta_identif :: TEXT AS identifcb 
                FROM
                  sh_cloud_pse.tb_emisor temi
                  INNER JOIN sh_cloud_pse.vw_codigobarra_comprobante pse ON temi.tb_emisor_ruc = pse.ruc";
        // $tb_relation_left = [  "tb_nc_cab" => " LEFT JOIN sh_cloud_cpe.tb_nc_cab cpe ON split_part( pse.tb_comprobante_nomextarccomele, '.' :: TEXT, 1 ) = ( cpe.cpe_num_ruc :: TEXT || '-' || cpe.modif_tipo_doc :: TEXT || '-' || cpe.modif_serie_corre :: TEXT ) " ];
        // foreach ( $tb_relacion_with_comprobante as $value ) 
        // {
        //   $script .= self::TB_RELATION_LEFT[ $value ];
        // }

        $script .= " LEFT JOIN sh_cloud_cpe."  .$tb_relacion_with_comprobante.  " cpe ON split_part( pse.tb_comprobante_nomextarccomele, '.' :: TEXT, 1 ) = ( cpe.cpe_num_ruc :: TEXT || '-' || cpe.modif_tipo_doc :: TEXT || '-' || cpe.modif_serie_corre :: TEXT ) 
                  LEFT JOIN sh_cloud_cpe.tb_resumendiario_detalle cperd ON split_part( pse.tb_comprobante_nomextarccomele, '.' :: TEXT, 1 ) = cperd.tb_resumdeta_nombrecpe
                  LEFT JOIN sh_cloud_cpe.tb_comunicacionbaja_detalle cpecb ON split_part( pse.tb_comprobante_nomextarccomele, '.' :: TEXT, 1 ) = cpecb.tb_comudeta_nombrecpe 
                WHERE
                  1 = 1   " ;
                 
      if ( $ruc ) 
      {
        $script .= " AND ruc in ('" . $ruc  . "') ";
      }
      if ( $doc_tipo ) 
      {
        $script .= " AND tipocomprobante in (" . $doc_tipo . ") ";
      }
      if ( $doc_serie ) 
      {
        $script .= " AND seriecomprobante='" . $doc_serie . "' ";
      }
      if ( $correlativo ) 
      {
        $script .= " AND numerocomprobante='" . $correlativo . "' ";
      }

      if (  $fecha_ini and $fecha_fin ) 
      {
        $script .= " AND (fecha>='" . $fecha_ini . "' and fecha<='" . $fecha_fin . "' )";
      }
      //F01-0002456
      if ( $num_doc ) 
      {
        $script .= " AND ( seriecomprobante || '-' || numerocomprobante ) = '" . $num_doc . "' ";
        
      }
      if ( count( $estados ) ) 
      {
         $script .= " and tb_comprobante_respre in ( ". $estados . " ) ";
      }
       // echo $script;
      $result = $this->queryPersonalizate( $script );
      return $result;

    }
    /** 
    * retorna una lista sin importar que su respre sea null
    *@return Retorna la tda de la tabla tb_comprobante_all dado el nombre del cpe por parametro para resuemnes de baja
    * @param string $nombre_cpe = nombre del comprobante "20517182673-01-F102-00003064.zip"
    */
    public function getComprobanteAllByNameCpeForResumenes( $nombre_cpe )
    {

      $query = "select tb_comprobante_json, tb_comprobante_id from sh_cloud_pse.tb_comprobante_all where tb_comprobante_nomextarccomele = '" . $nombre_cpe . "' order by tb_comprobante_fechorpub  desc ;";
      $result = $this->queryPersonalizate( $query );
      return $result;
    }

    /** retorna el comprobante cuando la columna respre es nula
    *@return Retorna la tda de la tabla tb_comprobante_all dado el nombre del cpe por parametro
    * @param string $nombre_cpe = nombre del comprobante "20517182673-01-F102-00003064.zip"
    */
    public function getComprobanteAllByNameCpe( $nameFile,$ruc,$tipo_cpe, $serie, $correlativo )
    {

      $query = "select tb_comprobante_json, tb_comprobante_id from sh_cloud_pse.tb_comprobante_all where tb_comprobante_nomextarccomele='" .$nameFile. "' and tb_comprobante_ruc = '" . $ruc . "' and tipo_cpe='" .$tipo_cpe. "' and correlativo='". $correlativo ."' and serie='" . $serie . "' and tb_comprobante_respre is null order by tb_comprobante_fechorpub  desc ;";
      $result = $this->queryPersonalizate( $query );
      return $result;
    }
     public function getComprobanteByNameZipTb_Comprobante( $nameFile,  $ruc,$tipo_cpe, $serie, $correlativo )
    {
        // $script = "select *
        //               from sh_cloud_pse.tb_comprobante 
        //               where  tb_comprobante_nomextarccomele='" .$nameFile. "' and tb_comprobante_ruc = '" . $ruc . "' and tipo_cpe='" .$tipo_cpe. "' and correlativo='". $correlativo ."' and serie='" . $serie . "' and tb_comprobante_respre is not null order by tb_comprobante_fechorpub desc ";
        // $result = DB::select($script);
        $result = $this->
        whereRaw( ' tb_comprobante_nomextarccomele=? and tb_comprobante_ruc=? and tipo_cpe=? and  correlativo=? and serie=? and tb_comprobante_respre is null', [ $nameFile, $ruc, $tipo_cpe, $correlativo, $serie ] )
        ->orderBy('tb_comprobante_fechorpub', 'DESC')->get()->toArray()
        ;
        return $result;
    }

  private function isAcceptedOrObserver( $responseCpe ,$nameCpe )
    {
      $name = explode( '.' , $nameCpe) ;
      $porciones = explode('-', $name [0] );
      $ruc = $porciones[0];
      $typeCpe = $porciones[1];
      $serie = $porciones[2];
      $correlativo = $porciones[3];
      $status = false;
      $statusCpe = $this->verifyAcceptedStatusAndNameCpe( $nameCpe, $ruc,$typeCpe, $serie, $correlativo );
      
      if ( $statusCpe == 'A' || $statusCpe == 'O' ) 
      {
          $status = $statusCpe == 'A' ? 1 : 2 ;
      }
      return  $status;
    }
  /**
  * verifica si tiene estado accetado
  * @return retorna 'A' o 'O' , 'R' si existe, caso contrario false
  */ 
  function verifyAcceptedStatusAndNameCpe(  $nameFile, $ruc,$tipo_cpe, $serie, $correlativo  )
  {
    $data = $this->getComprobanteByNameZipTb_Comprobante(  $nameFile, $ruc,$tipo_cpe, $serie, $correlativo  );
    foreach ( $data as  $value ) 
    {
      if ( $value[ 'tb_comprobante_respre' ] == 'A' || $value[ 'tb_comprobante_respre' ] == 'O' || $value[ 'tb_comprobante_respre' ] == 'R' )
      {
        return $value[ 'tb_comprobante_respre' ];
      }
    }
    return false;
  }
  /**
  * @param string $idComprobante = id del comprobante
  * @param string $nameFile = nombre del comprobante "20517182673-01-F102-00003064.zip"
  * @param string $response_service = "ACEPTADO", "ACEPTADO CON OBSERVADO", "RECHAZADO"
  * 
  */
  function updateCompobante_ComprobanteAll( $idComprobante ,String $nameFile, $response_service , $cod_service , $message_service, $observation_service ,$xmlCDR, $comprobante_desexc = NULL, $numeroidentificacionCliente, $montoTotal, $fechaEmision, $status_code_email = NULL, $tipoMoneda = NULL)
  {
      $name = explode( '.' , $nameFile) ;
      $porciones = explode('-', $name [0] );
      $ruc = $porciones[0];
      $tipo_cpe = $porciones[1];
      $serie = $porciones[2];
      $correlativo = $porciones[3];
      $arrayComprobanteAllResult = $this->getComprobanteAllByNameCpe( $nameFile,$ruc,$tipo_cpe, $serie, $correlativo );
      // verifica si el comprobante ya ha sido aceptado
      $statusCpe = $this->verifyAcceptedStatusAndNameCpe( $nameFile, $ruc,$tipo_cpe, $serie, $correlativo );
      
      $response = [];
      $estado = 0;
      switch ( $response_service ) 
      {
        case 'A':
              $estado = 1;
          break;
        case 'O':
              $estado = 2;
          break;
        case 'R':
              $estado = 0;
          break;
      }
      if ( $statusCpe == 'A' || $statusCpe == 'O' ) 
      {
        $estado = $statusCpe =='A' ? 1 : 2 ;
      }
      $prefijo = "tb_comprobante_";
      $dataUpdateComprobante = [
         $prefijo . "respre" => $response_service ,
         $prefijo . "codres" => $cod_service,
         $prefijo . "desres" => $message_service,
         $prefijo . "obsres" => $observation_service,
         $prefijo . "base64cdr" => $xmlCDR,
         $prefijo . "desexc" => $comprobante_desexc,
         $prefijo . "emailstaus" => $status_code_email,
         $prefijo . "num_identificacion_cli" => $numeroidentificacionCliente,
         $prefijo . "total" => $montoTotal,
         $prefijo . "fecha_emision" => $fechaEmision,
         $prefijo . "es_aceptado" => $estado,
         $prefijo . "moneda" => $tipoMoneda,

      ];
      $whereComprobante = [
          $prefijo."id" =>  $idComprobante,
          $prefijo."nomextarccomele" =>  $nameFile,
          "correlativo" =>  $correlativo,
          "serie" =>  $serie,
          "tipo_cpe" =>  $tipo_cpe
      ];
      
      $response['tb_comprobante_update_respre'] = $this->CI_UPDATE( $dataUpdateComprobante, $whereComprobante );
      if ( $statusCpe == 'R' ) 
      {
          if ( $estado !== 0 ) 
          {
            $dataUpdateComprobante_es_aceptado = 
                                  [
                                    $prefijo . "es_aceptado" => $estado 
                                  ];
            $whereComprobante_es_aceptado = 
                              [
                                $prefijo . "nomextarccomele" =>  $nameFile,
                                $prefijo . "ruc" =>  $ruc,
                                "serie" =>  $serie,
                                "correlativo" =>  $correlativo,
                                "tipo_cpe" =>  $tipo_cpe,
                              ];
            $response['tb_comprobante_update_estado_status'] =  $this->CI_UPDATE( $dataUpdateComprobante_es_aceptado, $whereComprobante_es_aceptado );
            $response['tb_comprobante_update_estado'] = 'se actualizo correctamente tb_comprobante ID:'. $idComprobante;
          }
      }
      $response['tb_comprobante'] = 'se actualizo correctamente tb_comprobante ID:'. $idComprobante;

      if (  count($arrayComprobanteAllResult) ) 
      {
          // $idComprobante = $arrayComprobanteResult[0][ 'tb_comprobante_id' ];
          $idComprobanteAll = $arrayComprobanteAllResult[0][ 'tb_comprobante_id' ];
          
         
          $dataUpdateComprobanteAll = [
             $prefijo . "respre" => $response_service ,
             $prefijo . "codres" => $cod_service,
             $prefijo . "desres" => $message_service,
             $prefijo . "emailstaus" => $status_code_email,
             $prefijo . "obsres" => $observation_service,
             $prefijo . "desexc" => $comprobante_desexc,
             $prefijo . "base64cdr" => $xmlCDR,
          ];

          $whereComprobanteAll = [
              $prefijo."id" =>  $idComprobanteAll
          ];
          $response['tb_comprobante_all_respre'] =  $this->CI_UPDATE( $dataUpdateComprobanteAll, $whereComprobanteAll, "sh_cloud_pse.tb_comprobante_all" );
          $response['tb_comprobante_all']  = 'se actualizo correctamente tb_comprobante_all ID:'. $idComprobanteAll;
      }else
      {
          $response['tb_comprobante'] .="\n no se encontro el archivo en la tabla tb_comprobante_all para actualizar". $idComprobante;
      }
      return $response;
  }

  


  private function getMontoByTypeCpe( $data,$tipoCpe )
  {
    switch ( $tipoCpe ) {
      case '20':
          $montoTotal = $data->montoTotalRetenido;
        break;
      case '40':
          $montoTotal = $data->montoTotalPercibido;
        break;
      default:
          $montoTotal = $data->montoTotalMonedaOriginal ?? null;
        break;
    }
    return $montoTotal;
  }

  // /**
  // * 
  // * @param $idComprobante
  // */
  // function insertJobObservations( $idComprobante, $nameFile )
  // {
  //   $data [
  //     'idComprobante' => $idComprobante,
  //     'idComprobante' => $idComprobante,
  //   ];
  // }
  /**
  * @param array $dataCPE [ 'tb_comprobante_id','tb_comprobante_all_id'. fecha_emision,numero_identificacion_client,monto_total,codigo_moneda ]
  * @param array $responseService [ 'responseService','responseCode', 'description', 'obsService', 'desexcService'  ]
  * @param string $response_service = "ACEPTADO", "ACEPTADO CON OBSERVADO", "RECHAZADO"
  * 
  */
  function updateCompobante( $dataCPE , $responseService , $nameFile,  $emailStatusCode, $jsonEntrada ,$isAcceptedOrObserver = false )
  {

      $name = explode( '.' , $nameFile) ;
      $porciones = explode('-', $name [0] );
      $ruc = $porciones[0];
      $tipo_cpe = $porciones[1];
      $serie = $porciones[2];
      $correlativo = $porciones[3];
      $prefijo = "tb_comprobante_";
      $response = [];
      $response['inicio'] = date('Y-m-d H:m:s.').round(microtime(true) * 1000) .'  Inicio de actualizacion cpe'  ;
      
      $estado = 0;
      switch ( $responseService[ 'responseService' ] ) 
      {
        case 'A':
              $estado = 1;
          break;
        case 'O':
              $estado = 2;
          break;
        case 'R':
              $estado = 0;
          break;
      }
      $estado =  $isAcceptedOrObserver !== false ? $isAcceptedOrObserver : $estado; 
     
      $observaciones = $responseService[ 'obsService' ] ?? NULL;
      $desexc = $responseService[ 'desexcService' ] ?? NULL;
      $dataUpdateComprobante = [
         $prefijo . "respre" => $responseService[ 'responseService' ] ,
         $prefijo . "codres" => $responseService[ 'responseCode' ],
         $prefijo . "desres" => $responseService[ 'description' ],
         $prefijo . "obsres" => $observaciones ,
         $prefijo . "base64cdr" => '',
         $prefijo . "desexc" =>  $desexc,
         $prefijo . "emailstaus" => $emailStatusCode,
         $prefijo . "total" => $this->getMontoByTypeCpe( $jsonEntrada, $tipo_cpe ),
         $prefijo . "es_aceptado" => $estado,
      ];
      $whereComprobante = [
          $prefijo."id" =>  $dataCPE[ 'tb_comprobante_id' ],
          $prefijo."ruc" =>  $ruc,
          $prefijo."nomextarccomele" =>  $nameFile,
          "correlativo" =>  $correlativo,
          "serie" =>  $serie,
          "tipo_cpe" =>  $tipo_cpe,
      ];
      $resultUpdate = $this->where( $whereComprobante  )->update( $dataUpdateComprobante );
      $response['end'] = date('Y-m-d H:m:s.').round(microtime(true) * 1000) .'  Fin de actualizacion cpe se actualizo correctamente tb_comprobante ID'  ;
      if ( $isAcceptedOrObserver == 'R' ) 
      {
          if ( $estado != 0 ) 
          {
           $response['tb_comprobante_update_estado_ini'] = date('Y-m-d H:m:s.').round(microtime(true) * 1000) .'  Inicio de actualizacion cpe status'  ;
            $dataUpdateComprobante_es_aceptado = 
                                  [
                                    $prefijo . "es_aceptado" => $estado 
                                  ];
            $whereComprobante_es_aceptado = 
                              [
                                $prefijo . "nomextarccomele" =>  $nameFile,
                                $prefijo . "ruc" =>  $ruc,
                                "serie" =>  $serie,
                                "correlativo" =>  $correlativo,
                                "tipo_cpe" =>  $tipo_cpe,
                              ];
            $response['tb_comprobante_update_estado_status'] =  $this->where( $whereComprobante_es_aceptado  )->update( $dataUpdateComprobante_es_aceptado );;
            $response['tb_comprobante_update_estado_end'] = date('Y-m-d H:m:s.').round(microtime(true) * 1000) .  'se actualizo correctamente tb_comprobante ID:'.$dataCPE[ 'tb_comprobante_id' ];
          }
      }
     
      
     
      return $response;
  }
  /**
  * @param array $dataCPE [ 'tb_comprobante_id','tb_comprobante_all_id'. fecha_emision,numero_identificacion_client,monto_total,codigo_moneda ]
  * @param array $responseService [ 'responseService','responseCode', 'description', 'obsService', 'desexcService'  ]
  * @param string $response_service = "ACEPTADO", "ACEPTADO CON OBSERVADO", "RECHAZADO"
  * 
  */
  public function updateCompobanteByJob( $dataCPE  )
  {

      list( $ruc, $tipo_cpe, $serie, $correlativo ) = porciones_cpe( $dataCPE[ 'cpe_name' ] );
      $prefijo = "tb_comprobante_";
      $response = [];
      $response['inicio'] = date('Y-m-d H:m:s.').round(microtime(true) * 1000) .'  Inicio de actualizacion cpe'  ;
      $estado = 0;
      switch ( $dataCPE[ 'responseService' ] ) 
      {
        case 'A':
              $estado = 1;
          break;
        case 'O':
              $estado = 2;
          break;
        case 'R':
              $estado = 0;
          break;
      }
      // $estado =  $isAcceptedOrObserver !== false ? $isAcceptedOrObserver : $estado; 
     
      $observaciones = $dataCPE[ 'obsService' ] ?? NULL;
      $desexc = $dataCPE[ 'desexcService' ] ?? NULL;
      $dataUpdateComprobante = [
         $prefijo . "respre" => $dataCPE[ 'responseService' ] ,
         $prefijo . "codres" => $dataCPE[ 'responseCode' ],
         $prefijo . "desres" => $dataCPE[ 'description' ],
         $prefijo . "obsres" => $observaciones ,
         $prefijo . "base64cdr" => '',
         $prefijo . "desexc" =>  $desexc,
         // $prefijo . "emailstaus" => $emailStatusCode,
         $prefijo . "num_identificacion_cli" => $dataCPE[ 'num_identificacion_cli' ],
         $prefijo . "total" => $dataCPE[ 'monto_total' ],
         $prefijo . "fecha_emision" => $dataCPE[ 'fecha_emision' ],
         $prefijo . "es_aceptado" => $estado,
         $prefijo . "moneda" =>  $dataCPE[ 'moneda' ],

      ];
      $whereComprobante = [
          // $prefijo."id" =>  $dataCPE[ 'tb_comprobante_id' ],
          $prefijo."ruc" =>  $ruc,
          $prefijo."nomextarccomele" =>   $dataCPE[ 'cpe_name' ]. '.zip',
          "correlativo" =>  $correlativo,
          "serie" =>  $serie,
          "tipo_cpe" =>  $tipo_cpe,
      ];
      if ( isset( $dataCPE['tb_comprobante_id'] ) ) 
      {
        $whereComprobante[ $prefijo."id" ] =  $dataCPE['tb_comprobante_id'];
      }
      $resultUpdate = $this->where( $whereComprobante  )
      ->whereRaw('tb_comprobante_respre is NULL')
      ->update( $dataUpdateComprobante );
      $response['end'] = date('Y-m-d H:m:s.').round(microtime(true) * 1000) .'  Fin de actualizacion cpe se actualizo correctamente tb_comprobante ID'  ;
      // if ( $isAcceptedOrObserver == 'R' ) 
      // {
          if ( $estado != 0 ) 
          {
           $response['tb_comprobante_update_estado_ini'] = date('Y-m-d H:m:s.').round(microtime(true) * 1000) .'  Inicio de actualizacion cpe status'  ;
            $dataUpdateComprobante_es_aceptado = 
                                  [
                                    $prefijo . "es_aceptado" => $estado 
                                  ];
            $whereComprobante_es_aceptado = 
                              [
                                $prefijo . "nomextarccomele" =>  $dataCPE[ 'cpe_name' ],
                                $prefijo . "ruc" =>  $ruc,
                                "serie" =>  $serie,
                                "correlativo" =>  $correlativo,
                                "tipo_cpe" =>  $tipo_cpe,
                              ];
            $response['tb_comprobante_update_estado_status'] =  $this->where( $whereComprobante_es_aceptado  )->update( $dataUpdateComprobante_es_aceptado );;
            $response['tb_comprobante_update_estado_end'] = date('Y-m-d H:m:s.').round(microtime(true) * 1000) .  'se actualizo correctamente tb_comprobante Name:'.$dataCPE[ 'cpe_name' ];
          }
      // }
     
      return $response;
  }
  
  /**
  * @param string $idComprobante = id del comprobante
  * @param string $nameFile = nombre del comprobante "20517182673-01-F102-00003064.zip"
  * @param string $response_service = "ACEPTADO", "ACEPTADO CON OBSERVADO", "RECHAZADO"
  * 
  */
  function updateCompobante_ComprobanteAll_regeneracion( $idComprobante ,String $nameFile, $response_service , $cod_service , $message_service, $observation_service ,$xmlCDR, $comprobante_desexc = NULL, $numeroidentificacionCliente, $montoTotal, $fechaEmision, $status_code_email)
  {
     
      // $arrayComprobanteAllResult = $this->getComprobanteAllByNameCpe( $nameFile );
      $response = [];
      // if (  count($arrayComprobanteAllResult) ) 
      // {
          // $idComprobante = $arrayComprobanteResult[0][ 'tb_comprobante_id' ];
          // $idComprobanteAll = $arrayComprobanteAllResult[0][ 'tb_comprobante_id' ];
          $estado = 0;
          switch ( $response_service ) 
          {
            case 'A':
                  $estado = 1;
              break;
            case 'O':
                  $estado = 2;
              break;
            case 'R':
                  $estado = 0;
              break;
          }
          $prefijo = "tb_comprobante_";
          $dataUpdateComprobante = [
             $prefijo . "respre" => $response_service ,
             $prefijo . "codres" => $cod_service,
             $prefijo . "desres" => $message_service,
             $prefijo . "obsres" => $observation_service,
             $prefijo . "base64cdr" => $xmlCDR,
             $prefijo . "desexc" => $comprobante_desexc,
             $prefijo . "emailstaus" => $status_code_email,
             $prefijo . "num_identificacion_cli" => $numeroidentificacionCliente,
             $prefijo . "total" => $montoTotal,
             $prefijo . "fecha_emision" => $fechaEmision,
             $prefijo . "es_aceptado" => $estado
          ];
          $whereComprobante = [
              $prefijo."id" =>  $idComprobante
          ];

          $this->CI_UPDATE( $dataUpdateComprobante, $whereComprobante );
          $reponse = 'se actualizo correctamente tb_comprobante ID:'. $idComprobante;
          $dataUpdateComprobanteAll = [
             $prefijo . "respre" => $response_service ,
             $prefijo . "codres" => $cod_service,
             $prefijo . "desres" => $message_service,
             $prefijo . "emailstaus" => $status_code_email,
             $prefijo . "obsres" => $observation_service,
             $prefijo . "desexc" => $comprobante_desexc,
             $prefijo . "base64cdr" => $xmlCDR,
          ];

          $whereComprobanteAll = [
              $prefijo."nomextarccomele" =>  $nameFile
          ];
          $this->CI_UPDATE( $dataUpdateComprobanteAll, $whereComprobanteAll, "sh_cloud_pse.tb_comprobante_all" );
          $reponse = 'se actualizo correctamente tb_comprobante_all Nombre cpe:' . $nameFile;
      // }else
      // {
      //     $response ="no se encontro el archivo en la tabla para actualizar". $idComprobante;
      // }
      return $response;
  }
  /**
  * @return retorna la lista de comprobantes dada una lista de documentos
  * @param $numDocs lista de numero de docuemtos EJ: 'F001-1000'
  * @param $ruc ruc del emisor
  * @param $respre 
  */
  public function getComprobatesByNumDocs(Array $numDocs,String $ruc ,Array $respre)
  {
      $strDocs = implode("','", $numDocs);
      $serie = explode('-', $strDocs)[0];
      $correlativo = explode('-', $strDocs)[1];
      $result = DB::table($this->table)
      ->whereRaw('tb_comprobante_ruc = ? and serie = ? and correlativo = ?' , [$ruc,$serie, $correlativo])
      ->whereIn('tb_comprobante_respre' , $respre)
      ->get()
      ->toArray()
    ;
    return $result;
  }
  /**
  * @return retorna la lista de documentos en cola
  * @param lista de tipo de documentos EJ: ['01', '03', ]
  * @param $isIN si es verdadero solo se agrega el in en la condicion caso contrario se agrega el NOT
  * @param $isNullTicket si es verdadero entonces el atributo tb_comprobante_ticket tiene que ser nulo
  */
  public function getCpeCola(Array $tipoCpe, $isIN = false, $rucsPersonalizates= [], $isNotNullTicket = false)
  {
    $ruc_in = count( $rucsPersonalizates['ruc_in']) ?  implode("','", $rucsPersonalizates['ruc_in'] ) :'';
    $ruc_not_in = count( $rucsPersonalizates['ruc_not_in']) ?  implode("','", $rucsPersonalizates['ruc_not_in'] ) :'';
    $strTipoCpe = implode("','", $tipoCpe);
    $whereIn = $isIN ? 'IN' : 'NOT IN';
    $query ="SELECT 
                tb_comprobante_id,
                tb_comprobante_nomextarccomele,
                tb_comprobante_codbar,
                tb_comprobante_ticket,
                tb_razon_social,
                tb_comprobante_emailto,
                split_part(tb_comprobante_codbar, '|'::text, 9) as numeroidentificacioncliente,
                tb_emisor_ws,
                url_servicio_retenciones,
                url_servicio_guias,
                tb_emisor_user_sol,
                tb_emisor_clave_sol,
                tb_emisor_tipo_operador,
                nroticket,
                tb_comprobante_fechorpub
              FROM  sh_cloud_pse.tb_comprobante, sh_cloud_pse.tb_emisor
              WHERE 
                tb_comprobante_respre IS NULL
                and TO_CHAR ( tb_comprobante_fechorpub::date, 'yyyy-mm-dd' ) >= TO_CHAR ( current_date-2, 'yyyy-mm-dd' )
                and tb_comprobante_ruc = tb_emisor_ruc
                and tipo_cpe " . $whereIn . " ('" . $strTipoCpe . "') 
                ";
    if ($ruc_in !='') 
    {
      $query .= "  and tb_comprobante_ruc in ('" . $ruc_in . "')";
    }
    if ($ruc_not_in !='') 
    {
      $query .= "  and tb_comprobante_ruc not in ('" . $ruc_not_in . "')";
    }
    if ( $isNotNullTicket ) 
    {
        $query .= "  and tb_comprobante_ticket is not null";
    }else
    {
      $query .= "  and tb_comprobante_ticket is  null";
    }


    $query .= ' order by tb_comprobante_id desc 
                limit 1';
    $result = $this->queryPersonalizate( $query );
    return  count( $result ) ? $result[0] : [];
  }
  
  /**
  * @return retorna la lista de documentos en cola
  * @param lista de tipo de documentos EJ: ['01', '03', ]
  * @param $isIN si es verdadero solo se agrega el in en la condicion caso contrario se agrega el NOT
  */
  public function getCola( $nroCola )
  {
  
    // $query = 'select * from get_cola_update_cpe('.$nroCola.') ; ';
    $query ="SELECT tb_razon_social,tb_comprobante_emailto,split_part(tb_comprobante_codbar, '|'::text, 9) as numeroidentificacioncliente,tb_comprobante_id, tb_comprobante_nomextarccomele, tb_comprobante_codbar,tb_comprobante_base64pdf, tb_comprobante_base64zip, tb_emisor_ws, url_servicio_retenciones, url_servicio_guias, tb_emisor_user_sol, tb_emisor_clave_sol, tb_emisor_tipo_operador, nroticket, tb_comprobante_fechorpub
        FROM  sh_cloud_pse.tb_comprobante, sh_cloud_pse.tb_emisor
        WHERE tb_comprobante_ruc = tb_emisor_ruc  and tipo_cpe in ('01', '03', '07', '08','20', '09', '40') and tb_comprobante_respre is null 
        AND exists (select 1 from cola_envio c where c.nro_cola = ".$nroCola." and c.tb_comprobante_respre is null and tb_comprobante.tb_comprobante_id = c.tb_comprobante_id and reintentos=0 ) 
         limit 1;
                ";
 
    $result = $this->queryPersonalizate( $query );
    return  count( $result ) ? $result[0] : [];
  }
  
  public function getComprobanteByTipoCreacion( $filters, $tipoCreacion, $fechaIni, $fechaFin, $ruc )
  {
    $queryRecibidos ="SELECT 
        id_receptor  ruc, 
          
        fecha_emision::text  fechadoc, 
        'R' as respre,
        CASE 
        when tipo_dte='01' then 'FAC' 
        when tipo_dte='03'  then 'BOL' 
        when tipo_dte='07' and substr( serie,0,2 ) ='F' then 'NCF'
        when tipo_dte='07' and substr( serie,0,2 ) ='B' then 'NCB'
        when tipo_dte='08' and substr( serie,0,2 ) ='F' then 'NDF'
        when tipo_dte='08' and substr( serie,0,2 ) ='B' then 'NDB'
        when tipo_dte='20' then 'CRE'
        when tipo_dte='40' then 'CRP'
        when tipo_dte='RR' then 'TCR'
        when tipo_dte='RC' then 'RSB'
        when tipo_dte='RA' then 'CMB'
        ELSE  'SIN'
        END as tipo_cpe_doc,
        count( fecha_emision ) as cantidad
        from sh_cloud_pse.fe_recibidos,
            sh_cloud_pse.tb_emisor
        where 1=1
        and tb_emisor_ruc = id_receptor 
        and fecha_emision >= '".$fechaIni."' and fecha_emision<='".$fechaFin."'
         ";
    if( $ruc )
    {
      $queryRecibidos .= " and id_receptor ='".$ruc."'";
    }
    $queryRecibidos .=" GROUP BY id_receptor, tipo_dte, fecha_emision, tipo_cpe_doc";

    $query = "SELECT ruc, to_char(fechadoc::date, 'YYYYMMDD') fechadoc,respre,tipo_cpe_doc, cantidad FROM ( 
    SELECT 
        tb_comprobante_ruc AS ruc,
        tb_comprobante_fecha_emision::text AS fechadoc,
        tipo_creacion as respre,
       tipo_cpe_doc,
        count( tb_comprobante_fecha_emision ) as cantidad
        
      FROM
       (". $this->getViewComprobanteGroup( $filters ) .") pse 
      WHERE
        1 = 1 
       
        GROUP BY
        tb_comprobante_ruc,
        tipo_cpe_doc,
        fechadoc,
        tipo_creacion
        
        
        UNION
        ". $queryRecibidos ."
        ) as tb_emitidos_recibidos where 1=1 ";
        if ( !empty($tipoCreacion) ) 
        {
          $query .=  " and respre= '" . $tipoCreacion . "'";
        }
        $query .= " order by ruc, fechadoc asc ";
   
    $result = $this->queryPersonalizate( $query );
    return  $result;
  }

  public function getViewComprobanteGroup( $filters )
  {
    $query = "SELECT DISTINCT ON
                    ( tb_comprobante_nomextarccomele ) tb_comprobante_nomextarccomele :: TEXT,
                    CASE
                    WHEN tb_comprobante_respre = 'A' 
                    OR tb_comprobante_respre = 'O' 
                    OR tb_comprobante_respre = 'B' THEN
                      'E' ELSE'X' 
                    END AS tipo_creacion ,
                     CASE 
                    when tipo_cpe='01' then 'FAC' 
                    when tipo_cpe='03'  then 'BOL' 
                    when tipo_cpe='07' and substr( pse.serie,0,2 ) ='F' then 'NCF'
                    when tipo_cpe='07' and substr( pse.serie,0,2 ) ='B' then 'NCB'
                    when tipo_cpe='08' and substr( pse.serie,0,2 ) ='F' then 'NDF'
                    when tipo_cpe='08' and substr( pse.serie,0,2 ) ='B' then 'NDB'
                    when tipo_cpe='20' then 'CRE'
                    when tipo_cpe='40' then 'CRP'
                    when tipo_cpe='RR' then 'TCR'
                    when tipo_cpe='RC' then 'RSB'
                    when tipo_cpe='RA' then 'CMB'
                    ELSE  'SIN'
                    END as tipo_cpe_doc,
                    tb_comprobante_ruc,
                    tb_comprobante_fecha_emision 
                FROM
                    sh_cloud_pse.tb_comprobante pse,
                    sh_cloud_pse.tb_emisor
                WHERE
                   tb_comprobante_ruc = tb_emisor_ruc
                    ";
    $query.= $filters;
    $query .= "ORDER BY  tb_comprobante_nomextarccomele,tb_comprobante_respre";
    return $query;
  }

  public function getJobCpe( $nroCola )
  {
    // $query = 'select * from get_cola_update_job('.intval( $nroCola ).');';
    $query = "select job_info.*,
    tb_emisor_ws::text, 
    url_servicio_retenciones::text ,
    url_servicio_guias::text,
    tb_emisor_user_sol::text, 
    tb_emisor_clave_sol::text, 
    tb_emisor_tipo_operador::text 
    from job_info,sh_cloud_pse.tb_emisor where tb_emisor.tb_emisor_ruc=job_info.ruc_emisor and estado is null and nro_cola='".$nroCola."' and tipo_job='E' limit 1";
    $result = $this->queryPersonalizate($query);
    return count($result) > 0 ? $result[0] : [];
  }

  public function getGetStatusJobCpe( $nroCola )
  {
    $query = "SELECT
            job_info.*,
            tb_emisor_ws :: TEXT,
            url_servicio_retenciones :: TEXT,
            url_servicio_guias :: TEXT,
            tb_emisor_user_sol :: TEXT,
            tb_emisor_clave_sol :: TEXT,
            tb_emisor_tipo_operador :: TEXT 
          FROM
            job_info,
            sh_cloud_pse.tb_emisor 
          WHERE
            tb_emisor.tb_emisor_ruc = job_info.ruc_emisor 
            AND estado IS NULL 
            and tipo_job='1033'  
            ";
    if ( $nroCola !== false ) 
    {
        $query .= " and nro_cola='".$nroCola."'";
    }
    $query .= " LIMIT 1";
    $result = $this->queryPersonalizate($query);
    return count($result) > 0 ? $result[0] : [];
  }
  public function getCpeReportExcel( $filters )
  {
   
      $script = "SELECT DISTINCT ON
                ( tb_comprobante_nomextarccomele ) tb_comprobante_nomextarccomele :: TEXT as tb_comprobante_nomextarccomele,
                '-' AS num_doc_referenciado,
                '-' AS num_relacionado,
                tb_comprobante_ruc AS ruc,
                tb_comprobante_id,
                pse.tipo_cpe AS tipo_doc,
                pse.serie || '-' || pse.correlativo AS serie_correlativo ,
                pse.serie AS seriecomprobante,
                pse.correlativo AS numerocomprobante,
                split_part( pse.tb_comprobante_codbar, '|' :: TEXT, 5 ) AS igv,
                tb_comprobante_total monto,
                tb_comprobante_fecha_emision fechadoc,
                split_part( pse.tb_comprobante_codbar, '|' :: TEXT, 8 ) AS tipoidentificacioncliente,
                tb_comprobante_num_identificacion_cli numeroidentificacioncliente,
                tb_comprobante_ruc AS rucemi,
                temi.tb_emisor_nombres AS rsocial,
                ( pse.serie || '-' || pse.correlativo ) AS num_doc,
               
                tb_comprobante_emailto email_cpe,
                tb_comprobante_emailstaus AS emailto_status,
                tb_comprobante_nomextarccomele AS filezip,
                tb_comprobante_respre AS respre,
          
         
             
                case 
                when tb_comprobante_moneda = 'PEN'
                then 'SOLES'
                when tb_comprobante_moneda = 'USD'
                then 'DOLARES'
                else tb_comprobante_moneda
                end
                AS moneda,
                tb_razon_social :: TEXT
                FROM
                sh_cloud_pse.tb_emisor temi
                INNER JOIN sh_cloud_pse.tb_comprobante pse ON temi.tb_emisor_ruc = pse.tb_comprobante_ruc
                WHERE
                  1 = 1 and es_migrado is null " ;
      $script.= $filters;
      $script_generados_pse = "
                SELECT fechadoc, 
                serie_correlativo,
                seriecomprobante, 
                numerocomprobante,
                  igv, monto , 
                  tipo_doc, 
                  respre,
                tb_razon_social,
                numeroidentificacioncliente,
                ruc,moneda, emailto_status  from  (" .$script .") as generados_pse";
     
      $result  = $this->queryPersonalizate($script_generados_pse);
      return $result; 
  }


  public function updateStatusCpeSent( $fileSent )
  {
    foreach ($fileSent as  $value) 
    {
        $this->where('id', $fileSent['tb_comprobante_id'])->update([
          'ws_invoice_id' => $value['invoiceId'],
          'ws_invoice_sent' => true,
        ]);
    }
  }
}
?>
