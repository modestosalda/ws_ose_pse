<?php 
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
/**
 * Modelo para Factura Cab CPE
 */
class FacturaCabCPE extends Model
{
	// const TABLE_NAME = 'sh_cloud_pse.vw_codigobarra_comprobante';
    public $table  = 'sh_cloud_cpe.tb_factura_cab';
     protected $primaryKey = 'num_id';
    protected $fillable = [
            'cpe_num_ruc' ,
            'cpe_num_serie' ,
            'cpe_num_correl' ,
            'cpe_fecha' ,
            'cpe_cod_opera' ,
            'cpe_cod_mone' ,
            'clie_tip_doc' ,
            'clie_num_doc' ,
            'clie_ubigeo' ,
            'clie_rz_social' ,
            'clie_distrito' ,
            'clie_provincia' ,
            'clie_codpais' ,
            'clie_dpto' ,
            'clie_urb' ,
            'clie_dir' ,
            'mnt_imp' ,
            'mnt_importe' ,
            'mnt_total' ,
            'cargodes_cod_reg_per' ,
            'cargodes_porcentaje' ,
            'cargodes_mnt_perc' ,
            'cargodes_mntbase_perc' ,
            'cargodes_importe_con_perc' ,
            'orden_compra' ,
            'tipo_doc_guia' ,
            'guia_remi' ,

        ];
	
    /**
    * Actualiza los campos "tipo_doc_rela, estado_doc_rela , num_doc_rela"
    */
    public function actualizarFacturaComunicacionBajaByFile( $tipo_doc_rela, $estado, $numIdentidad, $file )
    {
        $arrayFile = explode('-', $file);
        $correlativo = str_replace('.zip', '', $arrayFile[ 3 ]);
        $pg_script = "update " . self::TABLE_NAME . "
                    set tipo_doc_rela='" . $tipo_doc_rela . "', estado_doc_rela = '".  $estado ."', num_identidad_doc_rela = '" .  $numIdentidad . "'
                    where cpe_num_ruc = '" .  $arrayFile[ 0 ] . "' and cpe_num_serie  = '" .  $arrayFile[ 2 ] . "' and cpe_num_correl  = '" . $correlativo  . "'";
        $result = $this->InsertOrUpdate( $pg_script );
        return $result;
        
    }

    /*
    * esta funcion reemplaza a la vista vw_factura
    * retorna la lista de factura 
    * $ruc ruc del emisor 
    * $serie serie del comprobante
    * $correlativo  del comprobante
    */
    public function getFactura_Cab_Det_Nota( $ruc = '', $serie = '', $correlativo = '' ){
        $query = " SELECT tb_factura_cab.cpe_num_ruc,
                    tb_factura_cab.cpe_num_serie,
                    tb_factura_cab.cpe_num_correl,
                    tb_factura_cab.cpe_fecha,
                    tb_factura_cab.cpe_cod_opera,
                    tb_factura_cab.cpe_cod_mone,
                    tb_factura_cab.clie_tip_doc,
                    tb_factura_cab.clie_num_doc,
                    tb_factura_cab.clie_ubigeo,
                    tb_factura_cab.clie_rz_social,
                    tb_factura_cab.clie_distrito,
                    tb_factura_cab.clie_provincia,
                    tb_factura_cab.clie_codpais,
                    tb_factura_cab.clie_dpto,
                    tb_factura_cab.clie_urb,
                    tb_factura_cab.clie_dir,
                    tb_factura_cab.mnt_imp,
                    tb_factura_cab.mnt_importe,
                    tb_factura_cab.mnt_total,
                    tb_factura_cab.cargodes_cod_reg_per,
                    tb_factura_cab.cargodes_porcentaje,
                    tb_factura_cab.cargodes_mnt_perc,
                    tb_factura_cab.cargodes_mntbase_perc,
                    tb_factura_cab.cargodes_importe_con_perc,
                    tb_factura_cab.orden_compra,
                    tb_factura_cab.tipo_doc_guia,
                    tb_factura_cab.guia_remi,
                    tb_factura_cab.tipo_doc_rela,
                    tb_factura_cab.estado_doc_rela,
                    tb_factura_cab.num_doc_rela,
                    tb_factura_cab.num_identidad_doc_rela,
                    tb_factura_det.item_des,
                    tb_factura_det.item_cod,
                    tb_factura_det.item_cod_sunat,
                    tb_factura_det.item_und,
                    tb_factura_det.item_cant,
                    tb_factura_det.item_pu,
                    tb_factura_det.item_mnt_unt,
                    tb_factura_det.item_mnt_valor_total,
                    tb_factura_det.imp_cod_afec_trib,
                    tb_factura_det.imp_mnt_total_trib,
                    tb_factura_det.imp_mnt_base,
                    tb_factura_det.imp_nombre,
                    tb_factura_det.imp_cod_inter,
                    tb_factura_det.imp_cod_trib,
                    tb_factura_det.imp_porc,
                    tb_factura_notas.codigo,
                    tb_factura_notas.descripcion
                    --tb_factura_tri.imp_mnt_total_tri,
                    --tb_factura_tri.imp_mnt_base AS imp_mnt_base_tri,
                    --tb_factura_tri.imp_nombre AS imp_nombre_tri,
                    --tb_factura_tri.imp_cod_inter AS imp_cod_inter_tri,
                    --tb_factura_tri.imp_cod_trib AS imp_cod_trib_tri
                   FROM sh_cloud_cpe.tb_factura_cab ,
                        sh_cloud_cpe.tb_factura_det,
                        --sh_cloud_cpe.tb_factura_tri,
                        sh_cloud_cpe.tb_factura_notas where sh_cloud_cpe.tb_factura_cab.num_id = tb_factura_det.num_id
                        and sh_cloud_cpe.tb_factura_cab.num_id = tb_factura_notas.num_id
                        --and sh_cloud_cpe.tb_factura_cab.num_id = tb_factura_tri.num_id 
                        ";
        $query .= !empty( $ruc ) ? " AND cpe_num_ruc='" . $ruc . "'" : "";
        $query .= !empty( $ruc ) ? " AND cpe_num_serie='" . $serie . "'" : "";
        $query .= !empty( $ruc ) ? " AND cpe_num_correl='" . $correlativo . "'" : "";
        $result = $this->queryPersonalizate( $query );
        return $result;
    }

    public function getImpuestosTrib( $ruc = '', $serie = '', $correlativo = '' )
    {
        $query = "
                    SELECT 
                    imp_cod_afec_trib,
                    sum(imp_mnt_total_trib) as mnt_total_trib,
                    sum( imp_mnt_base ) as mnt_base,
                    imp_cod_inter,
                    imp_cod_trib,
                    imp_nombre,
                    imp_porc
                    --tb_factura_tri.imp_mnt_total_tri,
                    --tb_factura_tri.imp_mnt_base AS imp_mnt_base_tri,
                    --tb_factura_tri.imp_nombre AS imp_nombre_tri,
                    --tb_factura_tri.imp_cod_inter AS imp_cod_inter_tri,
                    --tb_factura_tri.imp_cod_trib AS imp_cod_trib_tri
                    FROM sh_cloud_cpe.tb_factura_cab ,
                    sh_cloud_cpe.tb_factura_det
                    --sh_cloud_cpe.tb_factura_tri,
                    --sh_cloud_cpe.tb_factura_notas

                    where  
                    sh_cloud_cpe.tb_factura_cab.num_id = tb_factura_det.num_id
                    --and sh_cloud_cpe.tb_factura_cab.num_id = tb_factura_notas.num_id
                    --and sh_cloud_cpe.tb_factura_cab.num_id = tb_factura_tri.num_id
                   ";
        $query .= !empty( $ruc ) ? " AND cpe_num_ruc='" . $ruc . "'" : "";
        $query .= !empty( $ruc ) ? " AND cpe_num_serie='" . $serie . "'" : "";
        $query .= !empty( $ruc ) ? " AND cpe_num_correl='" . $correlativo . "'" : "";
        $query .= " GROUP BY imp_cod_afec_trib,imp_cod_inter,imp_cod_trib,imp_nombre, imp_porc";
        // echo $query;
         $result = $this->queryPersonalizate( $query );
        return $result;
    }
    /**
    * guarda la informacion seteada desde un xml tranformada en array
    */
    public function saveCabXml(Array $dataXml )
    {
        $datosEmisor = $dataXml[ 'datos_emisor' ];
        $detalleCpe = $dataXml[ 'detalle_cpe' ];
        $datosAdquiriente = $dataXml[ 'datos_adquiriente' ];
        $datosGlobales = $dataXml[ 'datos_globales' ];
        $datosPercepcion = $dataXml[ 'percepcion' ];
        $datosOrdenCompra = $dataXml[ 'ordenes_compra_guia' ];

        $dataSave=[
            'cpe_num_ruc' => $datosEmisor['ruc_emisor'],
            'cpe_num_serie' => $detalleCpe['numeroSerie'],
            'cpe_num_correl' => $detalleCpe['numeroCorrelativo'],
            'cpe_fecha' => $detalleCpe['fecha'] . ' ' . $detalleCpe['hora'],
            'cpe_cod_opera' => $detalleCpe['codigoTipoOperacion'],
            'cpe_cod_mone' => $detalleCpe['codigoTipoMoneda'],

            'clie_tip_doc' => $datosAdquiriente['tipodocumentocliente'],
            'clie_num_doc' => $datosAdquiriente['numeroDocumentoIdentidad'],
            'clie_ubigeo' => $datosAdquiriente['clie_ubigeo'],
            'clie_rz_social' => $datosAdquiriente['clie_razonsocial'],
            'clie_distrito' => $datosAdquiriente['distrito'],
            'clie_provincia' => $datosAdquiriente['provincia'],
            'clie_codpais' => $datosAdquiriente['cod_pais'],
            'clie_dpto' => $datosAdquiriente['departamento'],
            'clie_urb' => $datosAdquiriente['urbanizacion'],
            'clie_dir' => $datosAdquiriente['dir_completa'],

            'mnt_imp' => $datosGlobales['montoImpuestoMonedaOriginal'],
            'mnt_importe' => $datosGlobales['montoValorVentaMonedaOriginal'],
            'mnt_total' => $datosGlobales['montoTotalMonedaOriginal'],

            'cargodes_cod_reg_per' => $datosPercepcion['codigoRegimenPercepcion'],
            'cargodes_porcentaje' => $datosPercepcion['porcentajePercepcion'],
            'cargodes_mnt_perc' => $datosPercepcion['montoPercepcionMonedaNacional'],
            'cargodes_mntbase_perc' => $datosPercepcion['montoBasePercepcionMonedaNacional'],
            'cargodes_importe_con_perc' => $datosPercepcion['importeTotalConPercepcionMonedaNacional'],

            'orden_compra' => $datosOrdenCompra['ordenes_compra'],
            'tipo_doc_guia' => $datosOrdenCompra['tipo_doc_guiaremi'],
            'guia_remi' => $datosOrdenCompra['guia_remi'],

        ];
        $returning = [
            'num_id'
        ];
        $result = self::create( $dataSave );

        
        return $result;
    }
    /**
    * guarda la informacion seteada desde un xml tranformada en array
    */
    public function saveDetXml(Array $dataXml )
    {
        foreach ($dataXml as  $itemData) 
        {
            $dataSave = [
                'num_id' => $itemData[ 'num_id' ],
                'item_des' => $itemData[ 'item_des' ],
                'item_cod' => $itemData[ 'item_cod' ],
                'item_cod_sunat' => $itemData[ 'item_cod_sunat' ],
                'item_und' => $itemData[ 'codigoUnidadMedida' ],
                'item_cant' => $itemData[ 'cantidad' ],
                'item_pu' => $itemData[ 'PriceAmountXX' ],
                'item_mnt_unt' => $itemData[ 'montoValorVentaUnitarioMonedaOriginal' ],
                'item_mnt_valor_total' => $itemData[ 'montoValorVentaTotalMonedaOriginal' ],
                'imp_cod_afec_trib' => $itemData[ 'TaxExemptionReasonCode' ],
                'imp_mnt_total_trib' => $itemData[ 'montoTotalMonedaOriginalXX' ],
                'imp_mnt_base' => $itemData[ 'montoBaseMonedaOriginalXX' ],
                'imp_nombre' => $itemData[ 'nombreXX' ],
                'imp_cod_inter' => $itemData[ 'codigoInternacionalXX' ],
                'imp_cod_trib' => $itemData[ 'IDXX' ],
                'imp_porc' => $itemData[ 'PercentXX' ]
            ];
            $result = DB::table('sh_cloud_cpe.tb_factura_det')->insert($dataSave);

            // $result = $this->CI_SAVE( $dataSave ,[],);
        }

        return $result;
    }
    /**
    * guarda la informacion seteada desde un xml tranformada en array
    */
    public function saveNotaXml(Array $dataXml )
    {
        foreach ($dataXml as  $itemData) 
        {

            $dataSave = [
                'num_id' => $itemData[ 'num_id' ],
                'codigo' => $itemData[ 'codigo_nota' ],
                'descripcion' => $itemData[ 'descripcion_nota' ]
            ];
            $result = DB::table('sh_cloud_cpe.tb_factura_notas')->insert($dataSave);
        }

        return $result;
    }
    /**
    * guarda la informacion seteada desde un xml tranformada en array
    */
    public function saveTributoXml(Array $dataXml )
    {
        foreach ($dataXml as  $itemData) 
        {
            $dataSave = 
            [
                'num_id' => $itemData[ 'num_id' ],
                'imp_mnt_total_tri'=> $itemData[ 'montoTotaltributo' ],
                'imp_mnt_base'=> $itemData[ 'montoBaseMonedaOriginal' ] ,
                'imp_nombre'=>$itemData[ 'nombre' ] ,
                'imp_cod_inter'=> $itemData[ 'codigoInternacional' ],
                'imp_cod_trib'=> $itemData[ 'codigo_impuesto' ] 
            ];
            $result = DB::table('sh_cloud_cpe.tb_factura_tri')->insert($dataSave);
        }

        return $result;
    }

}
 ?>