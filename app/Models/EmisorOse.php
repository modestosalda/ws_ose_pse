<?php 
// namespace Modelos;
// require '../Config/autoload.php';
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\CategoriaEmisor;
/**
 * Modelo para comprobantes
 * Created By Modesto Saldaña Michalec<modesto_salda_m93@gmail.com>
 */
class EmisorOse extends Model
{
    protected $connection = 'mysql';

    protected $table = 'ose_emisor';
    public $timestamps = false;
	protected $fillable =[ 'estatus', 'estatus', 'descripcion', 'ruc', 'encabezado', 'pie', 'remitente', 'asunto', 'logo' ];
    const _PROPERTIES = ['estatus', 'estatus', 'descripcion', 'ruc', 'encabezado', 'pie', 'remitente', 'asunto', 'logo'];
    const ESTADO_ACTIVO = 'A';
    const DEFAULT_CATEGORIA_NEW_EMISOR = 4;
    // public function getEmisorByRuc( $ruc )
    // {
    //     $result = $this->where('ruc',$ruc)->get()->toArray();
    //     return $result;
    // }
    public function getUsuarioEmisorByRuc( $ruc )
    {
        // $query = "SELECT username,password_2 from ose_aso_emisores_usuarios rel 
        //             inner join ose_usuario_emisor ou on rel.usuario_id=ou.id 
        //             inner join ose_emisor oe on rel.emisor_id=oe.id
        //             where ruc='" . $ruc . "' ";
        // $result = $this->queryPersonalizate( $query );
        $result = $this ->select('ose_emisor.*', 'ose_usuario_emisor.*')
                        ->join('ose_usuario_emisor', 'emisor_id', '=', 'ose_emisor.id')
                        ->where('ruc', $ruc)
                        ->get()->first();
        return $result;
    }

     public function getEmisorByRuc( String $ruc )
    {
        // $script = "select * from " . self::_TABLE . " where tb_emisor_ruc = '" .$ruc. "' " ;
        $result = $this->where('ruc', $ruc)->get()->toArray();
        return $result ? $result[0] : [];
    }

    public function getUsuarioByUsername($username){
        $result = $this ->select('ose_emisor.*', 'ose_usuario_emisor.*')
                        ->join('ose_usuario_emisor', 'emisor_id', '=', 'ose_emisor.id')
                        ->where('username', $username)
                        ->get()->toArray();
        return $result  ? $result[0] : [];
    }

    public function getCategoriaEmisorByRuc($ruc){
        $result = $this->select('ose_emisor.*', 'ose_categoria_emisor.*')
                        ->join('ose_categoria_emisor', 'ose_categoria_emisor.emisor_id', '=', 'ose_emisor.id')
                        ->where('ruc', $ruc)
                        ->get()->toArray();
        return $result ? $result[0] : [];   
    }

    public function saveEmisorWithCategory(array $data){
        $newData = $this->filterProperties($data);

        $newEmisor = self::create([
            'ruc' => $newData['ruc'],
            'descripcion' => $newData['descripcion'],
            'estatus' => $newData['estatus']
        ]);
        if ($newEmisor) {
       
            $dataCategoria = [
                'emisor_id' => $newEmisor['id'],
                'categoria_id' => isset($data['categoria_id'])
                    ? $data['categoria_id']
                    : self::DEFAULT_CATEGORIA_NEW_EMISOR,
                'estado'        => 1,
                'visible'       => 1
            ];
            $result = CategoriaEmisor::create($dataCategoria);
            // echo "hola como estas";
            // dd("hojñlajs");
        }
        return $newEmisor;       
    }

     private function filterProperties(array $data)
    {
        $properties = self::_PROPERTIES;
        $newData = array_filter(
            $data,
            function($key) use ($properties) {
                return in_array($key, $properties);
            },
            ARRAY_FILTER_USE_KEY
        );
        
        if ( isset( $newData['estatus'] ) )
        {
            if (empty($newData['estatus']) && $newData['estatus'] !== "0") 
            {
                $newData['estatus'] = self::ESTADO_ACTIVO;
            }
        }
        
        return $newData;
    }
}
 ?>