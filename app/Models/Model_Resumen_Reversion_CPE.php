<?php 
// namespace Modelos;
// require '../Config/autoload.php';
Include_once   $_SERVER[ 'DOCUMENT_ROOT' ] . '/ws/Modelos/CI_Model.php';
namespace Models;

// use CI_Model;
/**
 * Modelo Resumen Diario
 * Created By Modesto Saldaña Michalec<modesto_salda_m93@gmail.com>
 */
class Model_Resumen_Reversion_CPE extends CI_Model
{
    const TABLE_NAME = 'sh_cloud_cpe.tb_resumenreversiones';
    function __construct()
    {
        parent::__construct(self::TABLE_NAME);  

    }

    public function getResumenReversionCabAndDetByIdentificador( $identificador, $ruc )
    {
        $query = "SELECT
                        (tb_resumenreversiones_det.tb_reverdeta_identif)::text AS identificacion,
                        (tb_resumenreversiones.tb_reversiones_fechemi)::text AS fecha_emision,
                        (tb_resumenreversiones_det.tb_reverdeta_nombrecpe)::text AS nombrecpe,
                        (tb_resumenreversiones_det.tb_reverdeta_estado)::text AS estado,
                        split_part((tb_resumenreversiones_det.tb_reverdeta_nombrecpe)::text, '-'::text, 1) AS ruc,
                        split_part((tb_resumenreversiones_det.tb_reverdeta_nombrecpe)::text, '-'::text, 2) AS tipocomprobante,
                        split_part((tb_resumenreversiones_det.tb_reverdeta_nombrecpe)::text, '-'::text, 3) AS serie,
                        split_part((tb_resumenreversiones_det.tb_reverdeta_nombrecpe)::text, '-'::text, 4) AS numerocomprobante ,tb_reverdeta_nombrecpe nombrecpe
                    FROM
                        sh_cloud_cpe.tb_resumenreversiones,
                        sh_cloud_cpe.tb_resumenreversiones_det 
                    WHERE
                        sh_cloud_cpe.tb_resumenreversiones.tb_reversiones_identif = tb_resumenreversiones_det.tb_reverdeta_identif 
                        AND tb_reversiones_identif = '" . $identificador . "'
                        AND tb_reversiones_ruc = '" . $ruc . "'
                        and tb_reverdeta_nombrecpe like '%".$ruc."%'
                        ";

        $result = $this->queryPersonalizate( $query );
        return $result;       
    }

    public function saveDataResumenReversionCab($data)
    {
        echo "<br>LLEGÓ AQUÍ CAB<br>";
        $valores =   implode(",",array_keys( $data ) )  ;
        $values =  implode("','",array_values( $data ) )  ;

        $query = "INSERT INTO sh_cloud_cpe.tb_resumenreversiones(".$valores.") VALUES('".str_replace(".","",$values)."')";     
        echo "<br>QUERY tb_resumenreversiones CAB :: ".$query;

        $result = $this->InsertOrUpdate( $query );
        return $result;
    }


    public function saveDataResumenReversionDet($data)
    {
        echo "<br>LLEGÓ AQUÍ DET<br>";
        $valores =   implode(",",array_keys( $data ) )  ;
        $values =  implode("','",array_values( $data ) )  ;

        $query = "INSERT INTO sh_cloud_cpe.tb_resumenreversiones_det(".$valores.") VALUES('".str_replace(".","",$values)."')";    
        echo "<br>QUERY tb_resumenreversiones_det DET :: ".$query;

        $result = $this->InsertOrUpdate( $query );
        return $result;
    }



}
?>