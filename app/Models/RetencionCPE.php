<?php 
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

// use CI_Model;
/**
 * Modelo para RetencionCPE
 * Created By Modesto Salda�a Michalec<modesto_salda_m93@gmail.com>
 */
class RetencionCPE extends Model
{
	public $table = 'sh_cloud_cpe.tb_retencion_cab';
    protected $primaryKey = 'num_id';
    protected $fillable = [
            'cpe_num_ruc',
            'cpe_num_serie' ,
            'cpe_num_correl' ,
            'cpe_fecha',
            'cpe_reg',

            'clie_tip_doc' ,
            'clie_num_doc' ,
            'clie_ubigeo',
            'clie_rz_social' ,
            'clie_distrito' ,
            'clie_provincia' ,
            'clie_codpais',
            'clie_dpto' ,
            'clie_urb' ,
            'clie_dir' ,

            'mnt_ret',
            'mnt_pag' ,
            'cpe_cod_mone' ,
        ];
    /**
    * Actualiza los campos "tipo_doc_rela, estado_doc_rela , num_doc_rela"
    */
    public function actualizarRetencionByFileAndType( $tipo_doc_rela, $estado, $numIdentidad, $file )
    {
        $arrayFile = explode('-', $file);
        $correlativo = str_replace('.zip', '', $arrayFile[ 3 ]);
        $pg_script = "update " . self::TABLE_NAME . "
                    set tipo_doc_rela='" . $tipo_doc_rela . "', estado_doc_rela = '".  $estado ."', num_identidad_doc_rela = '" .  $numIdentidad . "'
                    where cpe_num_ruc = '" .  $arrayFile[ 0 ] . "' and cpe_num_serie  = '" .  $arrayFile[ 2 ] . "' and cpe_num_correl  = '" . $correlativo  . "'";
        $result = $this->InsertOrUpdate( $pg_script );
        return $result;
        
    }

    public function getRetenciones( $ruc, $serie, $correlativo )
    {
        $query = " SELECT tb_retencion_cab.num_id,
                    tb_retencion_cab.cpe_num_ruc AS ruc,
                    tb_retencion_cab.cpe_num_serie AS serie,
                    tb_retencion_cab.cpe_num_correl AS correlativo,
                    tb_retencion_cab.cpe_fecha AS fecha,
                    tb_retencion_cab.cpe_reg AS regional,
                    tb_retencion_cab.cpe_cod_mone AS codigo_moneda,
                    tb_retencion_cab.clie_tip_doc AS cliente_tipo_doc,
                    tb_retencion_cab.clie_rz_social AS cliente_ranzon_social,
                    tb_retencion_det.fecha_pago AS fecha_cp,
                    tb_retencion_det.rela_importe_total AS importe_total,
                    tb_retencion_det.importe_sin_ret AS importe_sin,
                    tb_retencion_cab.mnt_ret AS mtn_ret_per,
                    tb_retencion_cab.mnt_pag AS mtn_pag_cob,
                    tb_retencion_det.importe_ret AS importe,
                    tb_retencion_det.total_pagar_neto AS total_neto
                   FROM sh_cloud_cpe.tb_retencion_cab,
                    sh_cloud_cpe.tb_retencion_det
                  WHERE ((tb_retencion_cab.num_id)::text = (tb_retencion_det.num_id)::text)";
        $query .= " and cpe_num_ruc ='". $ruc. "' AND cpe_num_serie='" . $serie."' AND cpe_num_correl='". $correlativo."'";
        $result = $this->queryPersonalizate($query);
        return $result;


    }
    /**
    * guarda la informacion seteada desde un xml tranformada en array
    */
    public function saveCabXmlRET(Array $dataXml )
    {

        $datosEmisor = $dataXml[ 'datos_emisor' ];
        $detalleCpe = $dataXml[ 'detalle_cpe' ];
        $datosAdquiriente = $dataXml[ 'datos_adquiriente' ];
        $datosGlobales = $dataXml[ 'datos_globales' ];
        $dataSave=[
            'cpe_num_ruc' => $datosEmisor['ruc_emisor'],
            
            'cpe_num_serie' => $detalleCpe['numeroSerie'],
            'cpe_num_correl' => $detalleCpe['numeroCorrelativo'],
            'cpe_fecha' => $detalleCpe['fecha'] . ' ' . $detalleCpe['hora'],
            'cpe_reg' => $detalleCpe['cpe_reg'],

            'clie_tip_doc' => $datosAdquiriente['tipodocumentocliente'],
            'clie_num_doc' => $datosAdquiriente['numeroDocumentoIdentidad'],
            'clie_ubigeo' => $datosAdquiriente['clie_ubigeo'],
            'clie_rz_social' => $datosAdquiriente['clie_razonsocial'],
            'clie_distrito' => $datosAdquiriente['distrito'],
            'clie_provincia' => $datosAdquiriente['provincia'],
            'clie_codpais' => $datosAdquiriente['cod_pais'],
            'clie_dpto' => $datosAdquiriente['departamento'],
            'clie_urb' => $datosAdquiriente['urbanizacion'],
            'clie_dir' => $datosAdquiriente['dir_completa'],

            'mnt_ret' => $datosGlobales['mnt_per'],
            'mnt_pag' => $datosGlobales['mnt_cob_per'],
            'cpe_cod_mone' =>$datosGlobales['cpe_cod_mone'],
        ];
        $result = self::create( $dataSave );
        return $result;
    }

    /**
    * guarda la informacion seteada desde un xml tranformada en array
    */
    public function saveDetXmlRET(Array $dataXml )
    {

        foreach ($dataXml as  $itemData) 
        {
            $dataSave = $itemData;
            $result = DB::table('sh_cloud_cpe.tb_retencion_det')->insert($dataSave);
        }

    }

    /**
    * guarda la informacion seteada desde un xml tranformada en array
    */
    public function saveNotaXmlRET(Array $dataXml )
    {

        foreach ($dataXml as  $itemData) 
        {
            $dataSave = $itemData;
            $result = DB::table('sh_cloud_cpe.tb_retencion_notas')->insert($dataSave);
        }

    }
}
