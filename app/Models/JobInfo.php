<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class JobInfo extends Model
{
	// const TABLE_NAME = 'sh_cloud_pse.vw_codigobarra_comprobante';
	public $table = 'job_info';
	

    protected $fillable = ['id', 'nro_cola', 'tb_comprobante_id', 'tb_comprobante_all_id', 'ruc_emisor', 'estado', 'tipo_job', 'base64zip' ];
    /**
    *   
    */
    public function getJobActiveByType( $type = '' )
    {
        return self::whereRaw( "estado is null and tipo_job='E' " )
        ->with('emisor')
        ->orderBy('id', 'DESC')
        ->get();
    }
    public function getByType( $type )
    {
       
    }

    public function getJobsSends(  )
    {
        return self::whereRaw( "estado estado='PE' and tipo_job='E' " )
        ->with('emisor')
        ->orderBy('id', 'DESC')
        ->get();
    }
    public function updateJobIn( $ids, $data )
    {
        try {
            // self::whereIn( 'tb_comprobante_id', $ids )->update( $data );    
        } catch (Exception $e) {
            throw new Exception( $e->getMessage() , 1);
        }
        
    }
    public function updateJobByStatus( $tbComprobanteId, $estado )
    {
        echo "actualizando";
        $data = [ 'estado' => $estado  ];
        $this->where( 'tb_comprobante_id', $tbComprobanteId )->update( $data );
    }

    public function emisor()
    {
        return $this->belongsTo( \App\Models\EmisorPSE::class, 'ruc_emisor', 'tb_emisor_ruc' );
    }
    // function deleteJob( $id )
    // {
    //     $query = "delete from ". self::TABLE_NAME . " where id= '" .$id. "'";
    //     $this->queryPersonalizate($query);
    // }
    // public function getJobCpe( $nroCola = false )
    // {
    //     // $query = 'select * from get_cola_update_job('.intval( $nroCola ).');';
    //     $query = "SELECT job_info.*,
    //     tb_emisor_ws::text, 
    //     url_servicio_retenciones::text ,
    //     url_servicio_guias::text,
    //     tb_emisor_user_sol::text, 
    //     tb_emisor_clave_sol::text, 
    //     tb_emisor_tipo_operador::text 
    //     from job_info,sh_cloud_pse.tb_emisor where tb_emisor.tb_emisor_ruc=job_info.ruc_emisor and estado is null  and tipo_job='E' ";
    //     if ( $nroCola !== false ) {

    //         $query = " and nro_cola='".$nroCola."'";
    //     }
    //     $result = $this->queryPersonalizate($query);
    //     return count($result) > 0 ? $result : [];
    // }
    // public function updateJob( $data )
    // {
    //     $tb_comprobante_id = array_map( function( $element ){ return $element['tb_comprobante_id'] ;} , $data );
    //     $tb_comprobante_id = implode(',', $tb_comprobante_id);
       
    //     $query = "UPDATE job_info 
    //     SET estado = 'P'
    //     where tb_comprobante_id in ($tb_comprobante_id)
    //     ";
    //     $result = $this->InsertOrUpdate($query);
    // }
    public function deleteJobById( $id )
    {
        // $queryJobInfo = "delete from job_info where tb_comprobante_id ='". $dataCPE[ 'tb_comprobante_id' ]. "' and tb_comprobante_all_id = '". $dataCPE['tb_comprobante_all_id' ] ."'";
        // $this->InsertOrUpdate( $queryJobInfo );
        // $response['job_info'] = 'Se elimino correctamente En la table job_info comprobantesID:' . $dataCPE[ 'tb_comprobante_id' ];
        $this->where( 'id','=',$id )->delete();
        return 'Se elimino correctamente En la table job_info ID:' . $id;
    }
    public function updateJobByIdComprobante( $idComprobante, $data )
    {
        $this->where( 'tb_comprobante_id', $idComprobante )->update(  $data );
    }
}
 ?>