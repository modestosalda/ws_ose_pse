<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
// use DB;
use Illuminate\Support\Facades\DB;
/**
 * Modelo para Modelo UserEmisor OSE
 * Created By Modesto Saldaña Michalec<modesto_salda_m93@gmail.com>
 */

class UserEmisorOSE extends Model
{
   	public $timestamps = false;
	protected $connection = 'mysql';
	public $table = 'ose_usuario_emisor';

	protected $fillable = ['password_2', 'password', 'emisor_id', 'username', 'correo', 'direccion','telefono', 'fecha', 'verificacion', 'perfil', 'estado', "nombres", "apellidos", "dni", "tipo"];
	/**
	* @return retorna la categoria segun el nombre
	*/
	public function getAdminByRuc($ruc){
		 
        $result = $this->select('ose_emisor.*', 'ose_usuario_emisor.*')
        		->join('ose_emisor', 'ose_usuario_emisor.emisor_id', '=', 'ose_emisor.id')
                        ->where(['ruc'=> $ruc, 'perfil'=>'2'])
                        ->get()->toArray();
        return $result  ? $result[0] : [];
	}

	public function saveUserEmisor(array $data, array $exclude=[]){
		
		$newUser = self::create($data);

		return $newUser;
	}


}