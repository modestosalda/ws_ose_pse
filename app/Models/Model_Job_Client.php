<?php 
// namespace Modelos;
// require '../Config/autoload.php';
namespace Models;

// use CI_Model;
/**
 * Modelo para comprobantes
 */
class Model_Job_Client extends CI_Model
{
	// const TABLE_NAME = 'sh_cloud_pse.vw_codigobarra_comprobante';
	const TABLE_NAME = 'job_client';
	function __construct()
	{
		parent::__construct(self::TABLE_NAME);	
	}
    /**
    *   
    */
    function getJobActiveByType( $type = '' ):Array
    {
        $query = "SELECT * from ". self::TABLE_NAME . " where status is null ";
        if ( $type != '' )
        {
            $query.= " and type = '" . $type. "' ";
        }
        $query .= 'limit 1';
        $result = $this->queryPersonalizate( $query );
        if ( count($result) > 0) 
        {
            $id = $result[ 0 ][ 'id' ];
            $data = [
                'status' => 'P'
            ];
            $where = [
                'id' => $id
            ];
            // $this->CI_UPDATE( $data, $where );
        }
        return  count( $result ) > 0 ? $result[0] : [];
    }
    function deleteJob( $id )
    {
        $query = "delete from ". self::TABLE_NAME . " where id= '" .$id. "'";
        $this->queryPersonalizate($query);
    }



}
 ?>