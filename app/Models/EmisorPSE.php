<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * Modelo para Emisores PSE
 * Created By Modesto Saldaña Michalec<modesto_salda_m93@gmail.com>
 */

class EmisorPSE extends Model
{

    
    public $table = 'sh_cloud_pse.tb_emisor';
    protected $primaryKey  = 'tb_emisor_cod';
    protected $fillable = [ 'tb_emisor_ruc', 'tb_emisor_user', 'tb_emisor_pass','tb_emisor_fechreg', 'tb_emisor_fechmod', 'tb_emisor_fechbaja', 'tb_emisor_key','tb_emisor_ws', 'tb_emisor_user_sol',  'tb_emisor_clave_sol', 'tb_emisor_tipo_operador',  'url_servicio_retenciones',  'url_servicio_guias','tb_emisor_nombres','tb_emisor_nombrecomercial', 'tb_emisor_codigopais', 'tb_emisor_ubigeo', 'tb_emisor_departamento', 'tb_emisor_provincia', 'tb_emisor_distrito', 'tb_emisor_urbanizacion', 'tb_emisor_plantilla' ];

    const _PROPERTIES_REMOTE = ['ruc' => 'tb_emisor_ruc', 'ws' => 'tb_emisor_ws', 'user' => 'tb_emisor_user_sol', 'password' => 'tb_emisor_clave_sol', 'tipo_operador' => 'tb_emisor_tipo_operador', 'url_servicio_retenciones'=> 'url_servicio_retenciones', 'url_servicio_guias' => 'url_servicio_guias'];
	
    /**
    * retorna el emisor por ruc
    */
    // function getEmisorByRuc( String $ruc )
    // {
    //     $script = "select * from " . self::TABLE_NAME . " where tb_emisor_ruc = '" .$ruc. "' " ;
    //     $result = $this->queryPersonalizate( $script );
    //     return count( $result ) ? $result[0] : [];
    // }
    /**
    * setea el array del emisor para el envio al servicio de emision
    * retorna el array 
    */

    public function createEmisorPSE( $data )
    {
        try {
            $emisor = self::create( $data );
            return $emisor;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getEmisorByRuc( $ruc )
    {
        return $this
        ->where('tb_emisor_ruc', $ruc)
        ->get()->first();
    }
    // public function setOEmisor( $data, $codigoTipoDocumentoIdentidad = "6", $codigoEstablecimientoAnexo= "0000" )
    // {
    //     $array = 
    //     [
    //         'apellidosNombresDenominacionRazonSocial' => $data[ 'tb_emisor_nombres' ],
    //         'codigoTipoDocumentoIdentidad' =>$codigoTipoDocumentoIdentidad,
    //         'codigoEstablecimientoAnexo' =>$codigoEstablecimientoAnexo,
    //         'urbanizacion' => $data[ 'tb_emisor_urbanizacion' ],
    //         'codigoUbicacionGeografica' => $data[ 'tb_emisor_ubigeo' ],
    //         'numeroDocumentoIdentidad' => $data[ 'tb_emisor_ruc' ],
    //         'nombreComercial' => $data[ 'tb_emisor_nombrecomercial' ],
    //         'distrito' => $data[ 'tb_emisor_distrito' ],
    //         'provincia' => $data[ 'tb_emisor_provincia' ],
    //         'departamento' => $data[ 'tb_emisor_departamento' ],
    //         'codigoPais' => $data[ 'tb_emisor_codigopais' ],
    //         'direccion' => $data[ 'tb_emisor_direccion' ]
    //     ];
    //     return $array;
    // }

}
