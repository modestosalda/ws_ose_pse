<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * Modelo para Emisores PSE
 * Created By Modesto Saldaña Michalec<modesto_salda_m93@gmail.com>
 */

class ComprobanteAllPSE extends Model
{

    public $table = 'sh_cloud_pse.tb_comprobante_all';

    protected $fillable = [ 'tb_emisor_ruc' ];
	
    public function updateComprobanteAll( $dataCPE , $responseService , $nameFile,  $emailStatusCode )
    {
        $prefijo = 'tb_comprobante_';
        $name = explode( '.' , $nameFile) ;
        $porciones = explode('-', $name [0] );
        $ruc = $porciones[0];
        $tipo_cpe = $porciones[1];
        $serie = $porciones[2];
        $correlativo = $porciones[3];
        $dataUpdateComprobanteAll = [
         $prefijo . "respre" => $responseService[ 'responseService' ] ,
         $prefijo . "codres" => $responseService[ 'responseCode' ],
         $prefijo . "desres" => $responseService[ 'description' ],
         $prefijo . "emailstaus" => $emailStatusCode,
         $prefijo . "obsres" => $responseService[ 'obsService' ] ?? NULL,
         $prefijo . "desexc" => $responseService[ 'desexcService' ] ?? NULL,
         $prefijo . "base64cdr" => '',
      ];

      $whereComprobanteAll = [
          $prefijo."id" =>  $dataCPE[ 'tb_comprobante_all_id' ],
          $prefijo."ruc" =>  $ruc,
          $prefijo."nomextarccomele" =>  $nameFile,
          "correlativo" =>  $correlativo,
          "serie" =>  $serie,
          "tipo_cpe" =>  $tipo_cpe,
      ];
      $response['inicio'] = date('Y-m-d H:m:s.').round(microtime(true) * 1000) .'  Inicio de actualizacion cpe all'  ;
      $response['tb_comprobante_all_respre'] =  $this->where(  $whereComprobanteAll )->update($dataUpdateComprobanteAll);
      $response['end']  =date('Y-m-d H:m:s:').round(microtime(true) * 1000) . 'Fin actualizacion se actualizo correctamente tb_comprobante_all ID:'. $dataCPE[ 'tb_comprobante_all_id' ];
      return $response;
    }

  public function updateCompobanteByJob( $dataCPE  )
  {

      list( $ruc, $tipo_cpe, $serie, $correlativo ) = porciones_cpe( $dataCPE[ 'cpe_name' ] );
      $prefijo = "tb_comprobante_";
      $response = [];
      $response['inicio'] = date('Y-m-d H:m:s.').round(microtime(true) * 1000) .'  Inicio de actualizacion cpe'  ;
     
      $observaciones = $dataCPE[ 'obsService' ] ?? NULL;
      $desexc = $dataCPE[ 'desexcService' ] ?? NULL;
      $dataUpdateComprobante = [
         $prefijo . "respre" => $dataCPE[ 'responseService' ] ,
         $prefijo . "codres" => $dataCPE[ 'responseCode' ],
         $prefijo . "desres" => $dataCPE[ 'description' ],
         $prefijo . "obsres" => $observaciones ,
         $prefijo . "base64cdr" => '',
         $prefijo . "desexc" =>  $desexc,
         // $prefijo . "emailstaus" => $emailStatusCode,

      ];
      $whereComprobante = [
          // $prefijo."id" =>  $dataCPE[ 'tb_comprobante_id' ],
          $prefijo."ruc" =>  $ruc,
          $prefijo."nomextarccomele" =>   $dataCPE[ 'cpe_name' ]. '.zip',
          "correlativo" =>  $correlativo,
          "serie" =>  $serie,
          "tipo_cpe" =>  $tipo_cpe,
      ];
      if ( isset( $dataCPE['tb_comprobante_id'] ) ) 
      {
        $whereComprobante[ $prefijo."id" ] =  $dataCPE['tb_comprobante_id'];
      }
      $resultUpdate = $this->where( $whereComprobante  )
      ->whereRaw('tb_comprobante_respre is NULL')
      ->update( $dataUpdateComprobante );
      $response['end'] = date('Y-m-d H:m:s.').round(microtime(true) * 1000) .'  Fin de actualizacion cpe se actualizo correctamente tb_comprobanteall ID'  ;
     
      return $response;
  }
}
