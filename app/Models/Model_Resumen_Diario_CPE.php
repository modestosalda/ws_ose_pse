<?php 
// namespace Modelos;
// require '../Config/autoload.php';
Include_once   $_SERVER[ 'DOCUMENT_ROOT' ] . '/ws/Modelos/CI_Model.php';
namespace Models;

// use CI_Model;
/**
 * Modelo Resumen Diario
 * Created By Modesto Saldaña Michalec<modesto_salda_m93@gmail.com>
 */
class Model_Resumen_Diario_CPE extends CI_Model
{
	const TABLE_NAME = 'sh_cloud_cpe.tb_resumendiario';
    const PREFIX = 'tb_resumend';
	function __construct()
	{
		parent::__construct(self::TABLE_NAME);	
	}
    
    public function getResumenDiarioCabAndDetByIdentificador( $identificador ,$ruc)
    {
        $query = "SELECT
                        (tb_resumendiario_detalle.tb_resumdeta_identif)::text AS identificacion,
                        (tb_resumendiario.tb_resumend_fechemi)::text AS fecha_emision,
                        (tb_resumendiario_detalle.tb_resumdeta_nombrecpe)::text AS nombrecpe,
                        (tb_resumendiario_detalle.tb_resumdeta_estado)::text AS estado,
                        split_part((tb_resumendiario_detalle.tb_resumdeta_nombrecpe)::text, '-'::text, 1) AS ruc,
                        split_part((tb_resumendiario_detalle.tb_resumdeta_nombrecpe)::text, '-'::text, 2) AS tipocomprobante,
                        split_part((tb_resumendiario_detalle.tb_resumdeta_nombrecpe)::text, '-'::text, 3) AS serie,
                        split_part((tb_resumendiario_detalle.tb_resumdeta_nombrecpe)::text, '-'::text, 4) AS numerocomprobante,
                        tb_resumdeta_nombrecpe nombrecpe
                    FROM
                        sh_cloud_cpe.tb_resumendiario,
                        sh_cloud_cpe.tb_resumendiario_detalle 
                    WHERE
                        sh_cloud_cpe.tb_resumendiario.tb_resumend_identif = tb_resumendiario_detalle.tb_resumdeta_identif 
                        AND tb_resumend_identif = '" . $identificador . "'
                        AND tb_resumend_ruc = '" .$ruc. "' 
                        and tb_resumdeta_nombrecpe like '%".$ruc."%'
                        ";

        $result = $this->queryPersonalizate( $query );
        return $result;       
    }
    public function saveDataCab(array $data  )
    {
        // $dataSave = [ 'tb_resumend_ruc' => 2121336546 , 'tb_resumend_identif' => 'RR-202321-000001' ];
        $valores =   implode("','", array_values( $data ) )  ;
        $values =  implode(",", array_keys( $data ) )  ;

        $query = "INSERT INTO sh_cloud_cpe.tb_resumendiario(".  $values. " ) VALUES( '" .  $valores . "' ) ";
        $result = $this->InsertOrUpdate( $query );
        return $result;
    }
    public function saveDataDet( array $data )
    {
        $valores =   implode("','", array_values( $data ) )  ;
        $values =  implode(",", array_keys( $data ) )  ;

        $query = "INSERT INTO sh_cloud_cpe.tb_resumendiario_detalle(".  $values. " ) VALUES( '" .  $valores . "' )";
        $result = $this->InsertOrUpdate( $query );
        return $result;
    }

}
 ?>