<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
/**
 * Modelo para Emisores PSE
 * Created By Modesto Saldaña Michalec<modesto_salda_m93@gmail.com>
 */

class Model_perfil extends Model
{
	protected $connection = 'mysql';
	public $table = 'ose_perfil';
    	public $timestamps = false;

	/**
	* @return retorna la categoria segun el nombre
	*/
	public function getAdminByRuc($ruc){
		 
        $result = $this->select('ose_emisor.*', 'ose_usuario_emisor.*')
        				->join('ose_usuario_emisor', 'ose_usuario_emisor.emisor_id', '=', 'ose_emisor.id')
                        ->where(['ruc'=> $ruc, 'perfil'=>'2'])
                        ->get()->toArray();
        return $result  ? $result[0] : [];
	}

	public function getIdPerfilbyDescripcion($descripcion){
		$result = $this->whereRaw('lower(descripcion) = ? ', [ strtolower( $descripcion) ])->get()->toArray();
        return $result ? $result[0] : [];	
	}


}