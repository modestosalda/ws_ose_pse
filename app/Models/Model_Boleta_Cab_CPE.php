<?php 
// namespace Modelos;
// require '../Config/autoload.php';
Include_once   $_SERVER[ 'DOCUMENT_ROOT' ] . '/ws/Modelos/CI_Model.php';
namespace Models;

// use CI_Model;
/**
 * Modelo para comprobantes
 */
class Model_Boleta_Cab_CPE extends CI_Model
{
	// const TABLE_NAME = 'sh_cloud_pse.vw_codigobarra_comprobante';
	const TABLE_NAME = 'sh_cloud_cpe.tb_boleta_cab';
	function __construct()
	{
		parent::__construct(self::TABLE_NAME);	
	}
    /**
    * Actualiza los campos "tipo_doc_rela, estado_doc_rela , num_doc_rela"
    */
    public function actualizarBoletaByFile( $tipo_doc_rela, $estado, $numIdentidad, $file )
    {
        $arrayFile = explode('-', $file);
        $correlativo = str_replace('.zip', '', $arrayFile[ 3 ]);
        $pg_script = "update " . self::TABLE_NAME . "
                    set tipo_doc_rela='" . $tipo_doc_rela . "', estado_doc_rela = '".  $estado ."', num_identidad_doc_rela = '" .  $numIdentidad . "'
                    where cpe_num_ruc = '" .  $arrayFile[ 0 ] . "' and cpe_num_serie  = '" .  $arrayFile[ 2 ] . "' and cpe_num_correl  = '" . $correlativo  . "'";
        $result = $this->InsertOrUpdate( $pg_script );
        return $result;
        
    }
    /*
    * esta funcion reemplaza a la vista vw_boleta
    * retorna la lista de factura 
    * $ruc ruc del emisor 
    * $serie serie del comprobante
    * $correlativo  del comprobante
    */
    public function getBoleta_Cab_Det_Nota( $ruc = '', $serie = '', $correlativo = '' )
    {
        $query = "SELECT tb_boleta_cab.cpe_num_ruc,
                    tb_boleta_cab.cpe_num_serie,
                    tb_boleta_cab.cpe_num_correl,
                    tb_boleta_cab.cpe_fecha,
                    tb_boleta_cab.cpe_cod_opera,
                    tb_boleta_cab.cpe_cod_mone,
                    tb_boleta_cab.clie_tip_doc,
                    tb_boleta_cab.clie_num_doc,
                    tb_boleta_cab.clie_ubigeo,
                    tb_boleta_cab.clie_rz_social,
                    tb_boleta_cab.clie_distrito,
                    tb_boleta_cab.clie_provincia,
                    tb_boleta_cab.clie_codpais,
                    tb_boleta_cab.clie_dpto,
                    tb_boleta_cab.clie_urb,
                    tb_boleta_cab.clie_dir,
                    tb_boleta_cab.mnt_imp,
                    tb_boleta_cab.mnt_importe,
                    tb_boleta_cab.mnt_total,
                    tb_boleta_cab.cargodes_cod_reg_per,
                    tb_boleta_cab.cargodes_porcentaje,
                    tb_boleta_cab.cargodes_mnt_perc,
                    tb_boleta_cab.cargodes_mntbase_perc,
                    tb_boleta_cab.cargodes_importe_con_perc,
                    tb_boleta_cab.orden_compra,
                    tb_boleta_cab.tipo_doc_guia,
                    tb_boleta_cab.guia_remi,
                    tb_boleta_cab.tipo_doc_rela,
                    tb_boleta_cab.estado_doc_rela,
                    tb_boleta_cab.num_doc_rela,
                    tb_boleta_cab.num_identidad_doc_rela,
                    tb_boleta_det.item_des,
                    tb_boleta_det.item_cod,
                    tb_boleta_det.item_cod_sunat,
                    tb_boleta_det.item_und,
                    tb_boleta_det.item_cant,
                    tb_boleta_det.item_pu,
                    tb_boleta_det.item_mnt_unt,
                    tb_boleta_det.item_mnt_valor_total,
                    tb_boleta_det.imp_cod_afec_trib,
                    tb_boleta_det.imp_mnt_total_trib,
                    tb_boleta_det.imp_mnt_base,
                    tb_boleta_det.imp_nombre,
                    tb_boleta_det.imp_cod_inter,
                    tb_boleta_det.imp_cod_trib,
                    tb_boleta_det.imp_porc,
                    tb_boleta_notas.codigo,
                    tb_boleta_notas.descripcion
                    --tb_boleta_tri.imp_mnt_total_tri,
                    --tb_boleta_tri.imp_mnt_base AS imp_mnt_base_tri,
                    --tb_boleta_tri.imp_nombre AS imp_nombre_tri,
                    --tb_boleta_tri.imp_cod_inter AS imp_cod_inter_tri,
                    --tb_boleta_tri.imp_cod_trib AS imp_cod_trib_tri
                    FROM sh_cloud_cpe.tb_boleta_cab ,
                    sh_cloud_cpe.tb_boleta_det,
                    --sh_cloud_cpe.tb_boleta_tri,
                    sh_cloud_cpe.tb_boleta_notas where sh_cloud_cpe.tb_boleta_cab.num_id = tb_boleta_det.num_id
                    and sh_cloud_cpe.tb_boleta_cab.num_id = tb_boleta_notas.num_id
                    --and sh_cloud_cpe.tb_boleta_cab.num_id = tb_boleta_tri.num_id
                    ";
        $query .= !empty( $ruc ) ? " AND cpe_num_ruc='" . $ruc . "'" : "";
        $query .= !empty( $ruc ) ? " AND cpe_num_serie='" . $serie . "'" : "";
        $query .= !empty( $ruc ) ? " AND cpe_num_correl='" . $correlativo . "'" : "";
        $result = $this->queryPersonalizate( $query );
        return $result;
    }
    public function getImpuestosTrib( $ruc = '', $serie = '', $correlativo = '' )
    {
        $query = "
                    SELECT 
                    imp_cod_afec_trib,
                    sum(imp_mnt_total_trib) as mnt_total_trib,
                    sum( imp_mnt_base ) as mnt_base,
                    imp_cod_inter,
                    imp_cod_trib,
                    imp_nombre,
                    imp_porc
                    --tb_factura_tri.imp_mnt_total_tri,
                    --tb_factura_tri.imp_mnt_base AS imp_mnt_base_tri,
                    --tb_factura_tri.imp_nombre AS imp_nombre_tri,
                    --tb_factura_tri.imp_cod_inter AS imp_cod_inter_tri,
                    --tb_factura_tri.imp_cod_trib AS imp_cod_trib_tri
                    FROM sh_cloud_cpe.tb_boleta_cab ,
                    sh_cloud_cpe.tb_boleta_det
                    --sh_cloud_cpe.tb_factura_tri,
                    --sh_cloud_cpe.tb_factura_notas

                    where  
                    sh_cloud_cpe.tb_boleta_cab.num_id = tb_boleta_det.num_id
                    --and sh_cloud_cpe.tb_boleta_cab.num_id = tb_factura_notas.num_id
                    --and sh_cloud_cpe.tb_boleta_cab.num_id = tb_factura_tri.num_id
                   ";
        $query .= !empty( $ruc ) ? " AND cpe_num_ruc='" . $ruc . "'" : "";
        $query .= !empty( $ruc ) ? " AND cpe_num_serie='" . $serie . "'" : "";
        $query .= !empty( $ruc ) ? " AND cpe_num_correl='" . $correlativo . "'" : "";
        $query .= " GROUP BY imp_cod_afec_trib,imp_cod_inter,imp_cod_trib,imp_nombre, imp_porc";
        // echo $query;
         $result = $this->queryPersonalizate( $query );
        return $result;
    }


}
 ?>