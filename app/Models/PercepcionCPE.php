<?php 
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

// use CI_Model;
/**
 * Modelo para PercepcionCPE
 * Created By Modesto Salda�a Michalec<modesto_salda_m93@gmail.com>
 */
class PercepcionCPE extends Model
{
	public $table = 'sh_cloud_cpe.tb_percepcion_cab';
    protected $primaryKey = 'num_id';
	protected $fillable = [
            'cpe_num_ruc',
            'cpe_num_serie',
            'cpe_num_correl',
            'cpe_fecha',
            'cpe_cod_mone',
            'cpe_reg',
            'clie_tip_doc',
            'clie_num_doc',
            'clie_ubigeo',
            'clie_rz_social',
            'clie_distrito',
            'clie_provincia',
            'clie_codpais',
            'clie_dpto',
            'clie_urb',
            'clie_dir',
            'mnt_per',
            'mnt_cob',
            'cpe_cod_mone',
        ];
    /**
    * Actualiza los campos "tipo_doc_rela, estado_doc_rela , num_doc_rela"
    */
    public function actualizarPercepcionByFileAndType($tipo_doc_rela, $estado, $numIdentidad, $file )
    {
        $arrayFile = explode('-', $file);
        $correlativo = str_replace('.zip', '', $arrayFile[ 3 ]);
        $pg_script = "update " . self::TABLE_NAME . "
                    set tipo_doc_rela='" . $tipo_doc_rela . "', estado_doc_rela = '".  $estado ."', num_identidad_doc_rela = '" .  $numIdentidad . "'
                    where cpe_num_ruc = '" .  $arrayFile[ 0 ] . "' and cpe_num_serie  = '" .  $arrayFile[ 2 ] . "' and cpe_num_correl  = '" . $correlativo  . "'";
        $result = $this->InsertOrUpdate( $pg_script );
        return $result;
        
    }

    
        /**
    * Actualiza el estado"
    */
    public function actualizarEstadoPercepcionByFileAndType( $tipo_doc_rela, $estado, $numIdentidad, $file )
    {
        $arrayFile = explode('-', $file);
        $correlativo = str_replace('.zip', '', $arrayFile[ 3 ]);
        $pg_script = "update " . self::TABLE_NAME . "
                    set tipo_doc_rela='" . $tipo_doc_rela . "', estado_doc_rela = '".  $estado ."', num_identidad_doc_rela = '" .  $numIdentidad . "'
                    where cpe_num_ruc = '" .  $arrayFile[ 0 ] . "' and cpe_num_serie  = '" .  $arrayFile[ 2 ] . "' and cpe_num_correl  = '" . $correlativo  . "'";
        $result = $this->InsertOrUpdate( $pg_script );
        return $result;
        
    }

    /**
    * guarda la informacion seteada desde un xml tranformada en array
    */
    public function saveCabXmlPER(Array $dataXml )
    {
        $datosEmisor = $dataXml[ 'datos_emisor' ];
        $detalleCpe = $dataXml[ 'detalle_cpe' ];
        $datosAdquiriente = $dataXml[ 'datos_adquiriente' ];
        $datosGlobales = $dataXml[ 'datos_globales' ];
        $dataSave=[
            'cpe_num_ruc' => $datosEmisor['ruc_emisor'],
            
            'cpe_num_serie' => $detalleCpe['numeroSerie'],
            'cpe_num_correl' => $detalleCpe['numeroCorrelativo'],
            'cpe_fecha' => $detalleCpe['fecha'] . ' ' . $detalleCpe['hora'],
            'cpe_cod_mone' => $detalleCpe['codigoTipoMoneda'],
            'cpe_reg' => $detalleCpe['cpe_reg'],

            'clie_tip_doc' => $datosAdquiriente['tipodocumentocliente'],
            'clie_num_doc' => $datosAdquiriente['numeroDocumentoIdentidad'],
            'clie_ubigeo' => $datosAdquiriente['clie_ubigeo'],
            'clie_rz_social' => $datosAdquiriente['clie_razonsocial'],
            'clie_distrito' => $datosAdquiriente['distrito'],
            'clie_provincia' => $datosAdquiriente['provincia'],
            'clie_codpais' => $datosAdquiriente['cod_pais'],
            'clie_dpto' => $datosAdquiriente['departamento'],
            'clie_urb' => $datosAdquiriente['urbanizacion'],
            'clie_dir' => $datosAdquiriente['dir_completa'],

            'mnt_per' => $datosGlobales['mnt_per'],
            'mnt_cob' => $datosGlobales['mnt_cob_per'],
            'cpe_cod_mone' => $datosGlobales['cpe_cod_mone'],
        ];
       
        $result = self::create( $dataSave );
        
        return $result;
    }

    /**
    * guarda la informacion seteada desde un xml tranformada en array
    */
    public function saveDetXmlPER(Array $dataXml )
    {

        foreach ($dataXml as  $itemData) 
        {
            $result = DB::table('sh_cloud_cpe.tb_percepcion_det')->insert($itemData);
        }

    }

    /**
    * guarda la informacion seteada desde un xml tranformada en array
    */
    public function saveNotaXmlPER(Array $dataXml )
    {

        foreach ($dataXml as  $itemData) 
        {
            $result = DB::table('sh_cloud_cpe.tb_percepcion_notas')->insert($itemData);
        }

    }
    public function saveDataCab( array $data )
    {
        $valores =   implode("','", array_values( $data ) )  ;
        $values =  implode(",", array_keys( $data ) )  ;

        $query = "INSERT INTO " . self::TABLE_NAME ."(".  $values. " ) VALUES( '" .  $valores . "' ) RETURNING  num_id";
        $result = $this->InsertOrUpdate( $query );
        return $result;
    }
    public function saveDataDet( array $data )
    {
        $valores =   implode("','", array_values( $data ) )  ;
        $values =  implode(",", array_keys( $data ) )  ;

        $query = "INSERT INTO sh_cloud_cpe.tb_percepcion_det(".  $values. " ) VALUES( '" .  $valores . "' ) RETURNING  id";
        $result = $this->InsertOrUpdate( $query );
        return $result;
    }
    public function saveDataNotas( array $data )
    {
        $valores =   implode("','", array_values( $data ) )  ;
        $values =  implode(",", array_keys( $data ) )  ;

        $query = "INSERT INTO sh_cloud_cpe.tb_percepcion_notas(".  $values. " ) VALUES( '" .  $valores . "' ) ";
        $result = $this->InsertOrUpdate( $query );
        return $result;
    }

}
 ?>