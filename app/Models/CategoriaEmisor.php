<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
/**
 * Modelo para Emisores PSE
 * Created By Modesto Saldaña Michalec<modesto_salda_m93@gmail.com>
 */

class CategoriaEmisor extends Model
{
    public $timestamps = false;
	protected $connection = 'mysql';
	public $table = 'ose_categoria_emisor';
	protected $fillable = ['emisor_id', 'categoria_id','visible', 'estado'];



}

