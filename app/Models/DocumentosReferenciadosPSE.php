<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
// use CI_Model;
/**
 * Modelo para comprobantes
 * Created By Modesto Saldaña Michalec<modesto_salda_m93@gmail.com>
 */
class DocumentosReferenciadosPSE extends Model
{
    public $table = 'sh_cloud_pse.tb_doc_referenciados';
    /**
    * guarda en la tabla tb_doc_referenciados
    * @param $dataDoc lista de datos de docuemtos referenciados 
    * @param $dataDocRela lista de datos de documentos relacionados Extraido del XML
    */
    public function saveDocRef(Array $dataDoc, $dataDocRelaXML, $comprobante_id_relac, $tipoRelacion )
    {
        $datosEmisor = $dataDocRelaXML[ 'datos_emisor' ];
        $detalleCpe = $dataDocRelaXML[ 'detalle_cpe' ];
        foreach ($dataDoc as  $value) {
            
            $data = [
                'ruc'=> $datosEmisor[ 'ruc_emisor' ] ,
                'tipo_documento' => $value[ 'tipo_cpe' ] ,
                'serie' =>  $value[ 'serie' ],
                'correlativo' =>   $value[ 'correlativo' ],
                'comprobante_id' => $value[ 'tb_comprobante_id' ] ,
                'tipo_relacion' => $tipoRelacion ,
                'tipo_doc_relac' => $detalleCpe['codigoTipoDocumento'] ,
                'serie_doc_relac' =>  $detalleCpe['numeroSerie'],
                // 'correlativo_relac' =>   $detalleCpe['numeroCorrelativo'],
                'comprobante_id_relac' =>  $comprobante_id_relac ,
            ];
            DB::table( $this->table )->insert($data);
        }
        
        return [];
    }
    /**
    * guarda en la tabla tb_doc_referenciados
    * @param $dataDoc lista de datos de docuemtos referenciados 
    * @param $dataDocRela lista de datos de documentos relacionados Extraido del XML
    */
    public function saveXml(Array $dataDocXml, $dataDocRelaXML, $comprobante_id_relac, $tipoRelacion )
    {

        $datosEmisor = $dataDocRelaXML[ 'datos_emisor' ];
        $detalleCpe = $dataDocRelaXML[ 'detalle_cpe' ];
        foreach ($dataDocXml as  $value) 
        {
           
            $data = [
                'ruc'=> $datosEmisor[ 'ruc_emisor' ] ,
                // 'tipo_documento' => $value[ 'tipo_cpe' ] ,
                'tipo_documento' => $value[ 'tipo_doc_rela' ] ,
                'serie' =>  $value[ 'serie_rela' ],
                'correlativo' =>   $value[ 'correl_rela' ],
                // 'comprobante_id' => $value[ 'tb_comprobante_id' ] ,
                'tipo_relacion' => $tipoRelacion ,
                'tipo_doc_relac' => $detalleCpe['codigoTipoDocumento'] ,
                'serie_doc_relac' =>  $detalleCpe['numeroSerie'],
                'correlativo_relac' =>   $value['num_identidad_doc_rela'],
                'comprobante_id_relac' =>  $comprobante_id_relac ,
            ];
            DB::Table( $this->table )->insert($data);
        }
        return [];
    }
}