<?php 
// namespace Modelos;
// require '../Config/autoload.php';

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\EmisorOse;
use Sentry;
use Illuminate\Support\Facades\Cache;
use App\Strategies\ServiceEmailContext;
// use CI_Model;
/**
 * Modelo para comprobantes
 */
class CorreoPlantillaPSE extends Model
{

  
	// const TABLE_NAME = 'sh_cloud_pse.vw_codigobarra_comprobante';
  public $table = 'sh_cloud_pse.tb_correo_plantilla';
  const DEFAULT_TIPO_CORREO = 7;
  protected $fillable = [ 'id','cuerpo', 'asunto', 'con_copia' ,'emisor', 'id_tipo_correo'];
	

  public function getCorreoPlantillaByTypeAndRuc( $tipoCorreo = self::DEFAULT_TIPO_CORREO ,$codigoElmento = 0 ,  $ruc = null )
  {
    $query = "SELECT
              * 
            FROM
              sh_cloud_pse.tb_correo_plantilla,
              sh_cloud_pse.tb_tipo_correo 
            WHERE
              sh_cloud_pse.tb_correo_plantilla.id_tipo_correo = sh_cloud_pse.tb_tipo_correo.id_tipo_correo 
              AND sh_cloud_pse.tb_correo_plantilla.id_tipo_correo = '" . $tipoCorreo . "' 
              AND cod_elemento = '" . $codigoElmento . "' 
              ";
   
    $query .= $ruc ? " AND emisor = '" . $ruc . "'" : '';
    $result = DB::select($query);
    if ( !count( $result ) ) 
    {
       $query = "SELECT
              * 
            FROM
              sh_cloud_pse.tb_correo_plantilla,
              sh_cloud_pse.tb_tipo_correo 
            WHERE
              sh_cloud_pse.tb_correo_plantilla.id_tipo_correo = sh_cloud_pse.tb_tipo_correo.id_tipo_correo 
              AND sh_cloud_pse.tb_correo_plantilla.id_tipo_correo = '" . $tipoCorreo . "' 
              AND cod_elemento = '" . $codigoElmento . "' AND emisor = '00000000000' ";
   
      $result = DB::select($query);
      if ( !count( $result ) )
      {
        $query = "SELECT
              * 
            FROM
              sh_cloud_pse.tb_correo_plantilla,
              sh_cloud_pse.tb_tipo_correo 
            WHERE
              sh_cloud_pse.tb_correo_plantilla.id_tipo_correo = sh_cloud_pse.tb_tipo_correo.id_tipo_correo 
              AND sh_cloud_pse.tb_correo_plantilla.id_tipo_correo = '" .  self::DEFAULT_TIPO_CORREO . "' 
              AND cod_elemento = '" . $codigoElmento . "' AND emisor = '00000000000' ";
   
         $result = DB::select($query);
      }
    }
   
    return count( $result ) ? [ 'message' => $result[0][ 'valor_elemento' ] ] : [];  
  }

  /**
  * obtiene la configuracion de la plantilla
  */
  public function getPlantillaByRuc( $tipoCorreo = self::DEFAULT_TIPO_CORREO ,  $ruc = null ):Array
  {
    $result = $this->with('tipoCorreo')
    ->where('id_tipo_correo', $tipoCorreo)
    ->where('emisor', $ruc)
    ->get()
    ;
    if ( !$result->count() ) 
    {
      $result = $this->with('tipoCorreo')
      ->where('id_tipo_correo', $tipoCorreo)
      ->where('emisor', '00000000000' )
      ->get()
      ;
      if ( !$result->count() )
      {
         $result = $this->with('tipoCorreo')
          ->where('id_tipo_correo', self::DEFAULT_TIPO_CORREO)
          ->where('emisor', '00000000000' )
          ->get()
          ;
      }
    }
    
    return $result->first()->toArray();  
  }

  public function tipoCorreo()
  {
    return $this->belongsTo( \App\Models\TipoCorreo::class, 'id_tipo_correo','id_tipo_correo' );
  }


   public function get_emisor_impresion_pdf($ruc = '', $serie = '')
  {
    $query = "SELECT * from sh_cloud_pse.tb_emisor_impresion_pdf
              WHERE 
              tb_emisor_impresion_pdf_ruc = '" .$ruc. "'
              and tb_emisor_impresion_pdf_serie = '" . $serie . "'
              ";
    $result = DB::select($query);
    return  count( $result )>0 ? get_object_vars( $result[0]) : [];       
  }
  function get_email_template( Array $record )
  {

    $emisor = Cache::remember('getEmisorByRuc:' . $record['ruc'], now()->addMinutes(5) , function () use ( $record ){
      $modelOseEmisor = new EmisorOse();
      return $modelOseEmisor->getEmisorByRuc( $record['ruc'] );
    });
    if (  !count( $emisor ) )
    {
        throw new \Exception( 'El emisor no esta registrdo en la DB Mysql : '.$record['ruc'] , 1);
    }
    $emisor = $emisor[0];
    $logo = $emisor['logo'] ? env('URL_FRONTED') .'/'. $emisor['logo'] : '';
    // $logo = $modelOseEmisor->getEmisorByRuc( $porciones[0] );
    $logo = $logo ? "<img src='$logo' alt='Logo tipo' style='max-width: 100px; height: auto;'>" : '';

    $plantilla = 
    Cache::remember('getPlantillaByRuc:' . $record[ 'ruc' ] . ':tipo_correo:' . $record[ 'tipo_correo' ] , now()->addMinutes(5) , function () use ( $record ) {
      return $this->getPlantillaByRuc( $record[ 'tipo_correo' ] ,  $record[ 'ruc' ] );//body
    });
    if ( !count( $plantilla ) ) 
    {
        throw new \Exception('El tipo de Email no tiene Plantilla : '. $record['ruc']  , 1);
        Sentry::captureMessage( 'El tipo de Email no tiene Plantilla : '. $record['ruc'] );
    }
    $result_impresion_pdf = 
    Cache::remember('get_emisor_impresion_pdf:' . $record[ 'ruc' ] . ':serie:' . $record['serie'], now()->addMinutes(60) , function () use ( $record ) {
      return$this->get_emisor_impresion_pdf( $record[ 'ruc' ] ,  $record[ 'serie' ] );//body
    });
    $con_copia_template = count($plantilla) > 0 ? strtolower( str_replace( " " , "", $plantilla[ 'con_copia' ] ) )  : [];
    $con_copia_serie = '';
    if ( isset( $result_impresion_pdf[ 'tb_emisor_impresion_pdf_correo' ] ) ) 
    {
      $con_copia_serie = count( $result_impresion_pdf ) > 0 ?  strtolower( str_replace( " ", "", $result_impresion_pdf[ 'tb_emisor_impresion_pdf_correo' ] ) )   : [];  
    }
    $con_copia_serie_anulacion = '';
    if ( isset( $result_impresion_pdf[ 'tb_emisor_impresion_pdf_correo_anulacion' ] ) ) {
      $con_copia_serie_anulacion = count( $result_impresion_pdf ) > 0 ?  strtolower( str_replace( " ", "", $result_impresion_pdf[ 'tb_emisor_impresion_pdf_correo_anulacion' ] ) )   : [];
    }
   
    // var_dump($plantilla);
    // exit;
        // $subject = $this->getCorreoPlantillaByTypeAndRuc( $record[ 'tipo_correo' ] , $record[ 'ruc' ]);
        $nombreDocumento = get_type_doc_by_type( $record['tipo_cpe']  );

        if (!count($plantilla)) 
        {

            return ['message' => '', 'subject' => ''];
        }
        $subject = $plantilla[ 'asunto' ];
        $template['message'] = $plantilla[ 'cuerpo' ];
        $template['subject'] = $plantilla[ 'asunto' ];
        $template['con_copia'] = $con_copia_template;
        $template['con_copia_serie'] = $con_copia_serie;
        $template['con_copia_serie_anulacion'] = $con_copia_serie_anulacion;
        $alertaError =  isset( $record['alertaError'] ) ? $record['alertaError'] : '';
        $nombreProveedor =  isset( $record['nombre_proveedor'] ) ? $record['nombre_proveedor'] : '';

        $numeroDocumento = "{$record['serie']}-{$record['numero']}";
        $template['message'] = str_replace('{{fecha}}', $record['fecha'], $template['message']);
        $template['message'] = str_replace('{{numeroDocumento}}', $numeroDocumento, $template['message']);
        $template['message'] = str_replace('{{logotipo}}', $logo, $template['message']);
        $template['message'] = str_replace('{{nombreEmpresa}}', $emisor['descripcion'] ?: '-', $template['message']);
        $template['message'] = str_replace('{{nombreCliente}}', $record['razon_social'] ?: '-', $template['message']);
        $template['message'] = str_replace('{{nombreDocumento}}', $nombreDocumento, $template['message']);
        $template['message'] = str_replace('{{alertaError}}', $alertaError, $template['message']);
        $template['message'] = str_replace('{{nombreProveedor}}', $nombreProveedor, $template['message']);

        $styleColor_a = '';
        if (  $record['ruc'] == '20202380621' ||  $record['ruc'] == '20517182673' || $record['ruc']  == '20418896915' ) 
        {
          $styleColor_a = "style='color:black'";
        }
        $template['message'] = str_replace(
          '{{token_registro}}',  
          isset( $record['token_registro'] ) ? $record['token_registro'] : '', 
          $template['message']
        );
        $template['message'] = str_replace(
          '{{enlace_front_sesion_adquiriente}}',  
          (isset( $record['enlace_front_sesion_adquiriente'] ) && $record['enlace_front_sesion_adquiriente'] )? 
          "<a ".$styleColor_a." href='". env('URL_FRONTED')."/inicio/ingresar'>Click aqui para ingresar al portal</a>" : 
          '', 
          $template['message']
        );
        $template['message'] = str_replace(
          '{{enlace_front_registro_adquiriente}}',  
          ( isset( $record['enlace_front_registro_adquiriente'] ) && $record['enlace_front_registro_adquiriente'] )? 
          "<a ".$styleColor_a." href='". env('URL_FRONTED')."/inicio/registro_adquiriente'>Click aqui para registrarse en el Portal</a>" : 
          '', 
          $template['message']
        );
      // $template['message'] = str_replace('{{token_registro}}', $ruc_adquiriente_encrypt ?: '', $template['message']);

        if ( isset( $record['subject'] ) ) 
        {
           $numeroDocumento = "{$record['subject']['serie']}-{$record['subject']['numero']}";
        }
        $template['subject'] = str_replace('{{nombreDocumento}}', $nombreDocumento, $template['subject']);
        $template['subject'] = str_replace('{{numeroDocumento}}', $numeroDocumento, $template['subject']);
        $template['subject'] = str_replace('{{nombreEmpresa}}', $emisor['descripcion'], $template['subject']);
        $template['status']=true;
        return $template;
  }

  /**
  *
  */
  public function sendEmailCPE($cpeName,$correo ,$fechaEmision, $numeroDocumentoIdentidadReceptor , $base64PDF, $pathXMLENVIO,$pathFileCDR, $responseService, $apellidosNombresDenominacionRazonSocial , $nroTicket = '')
  {
    /** por el momento contamos con 2 :sendingblue, sendgrid */
    $serviceEmailCpe = env('SERVICE_EMAIL_COLA_ENVIO', 'sendinblue') ;
    $this->serviceEmailContext = new ServiceEmailContext( $serviceEmailCpe );
    try {
       // echo "hola";
       // exit();
      if ( env('SEND_EMAIL_ACTIVE', true) !== true ) {
        return 'Configurado para no enviar email';
      }
      $ruc_adquiriente_encrypt = openssl_encrypt( $numeroDocumentoIdentidadReceptor , env( 'OPENSSL_METHOD_CIPTHERING' ), env( 'DECRYPTION_KEY' ), env( 'OPENSSL_OPTION' ), env( 'OPENSSL_IV' ) );
      list( $this->_ruc,$this->_cpeType, $this->_serie , $this->_correlativo  ) = porciones_cpe( $cpeName );
      $recordEmail =
        [
          'ruc' =>  $this->_ruc,
          'serie' => $this->_serie ,
          'numero' => $this->_correlativo ,
          'tipo_cpe' =>  $this->_cpeType ,
          'razon_social'  =>  $apellidosNombresDenominacionRazonSocial ,
          'fecha' => $fechaEmision,
          'token_registro' => $ruc_adquiriente_encrypt,
          'enlace_front_sesion_adquiriente' => true,
          'enlace_front_registro_adquiriente' => true,
          'to_email' => $correo
        
        ];
        $file_cpe = [];
        if ( $responseService !== 'R' ) 
        {
          $fileAnexo =   get_anexo_by_ticket(  $this->_ruc,  $nroTicket ) ;
          
          $file_cpe = [
            ['file_name' => $cpeName , 'path_file' =>  $pathFileCDR , 'formato' => 'zip'], 
            ['file_name' => str_replace('zip', 'pdf', $cpeName ) , 'base64' =>  $base64PDF  ,'formato'=> 'pdf' ], 
            ['file_name' => str_replace('zip', 'xml', $cpeName ) , 'path_file' => $pathXMLENVIO  , 'formato' => 'xml']  
          ];
          if ( count( $fileAnexo ) > 0) 
          {
            if ( $fileAnexo['status'] && !empty(  trim( $fileAnexo['file'] )  ) && !empty(  $fileAnexo['name_file']  ) )  
            {
            $fileElement = [ 'file_name' => $fileAnexo['name_file'], 'base64' =>  $fileAnexo['file'] ];
              array_push($file_cpe , $fileElement );
            }
          }
        }

        $recordEmail[ 'tipo_correo' ] = $responseService == 'R' ? 4 : 2;
        $template = $this->get_email_template( $recordEmail );

        $bodyHtml = $template[ 'message' ];
        $subject = $template[ 'subject' ];
        $toEmail = $correo . ','. $template[ 'con_copia' ];
        $sender = get_subjet_by_ruc( $this->_ruc );
        // 
        $providerServiceEmail = $this->serviceEmailContext->instaceService( $toEmail, $sender, $subject, $file_cpe , $bodyHtml  );
        $responseEmail = $providerServiceEmail->sendEmail();
        $responseEmail[ 'record' ] = $recordEmail;
        return [ 'response' => $responseEmail,  'status_code' =>  $responseEmail[ 'code' ] ?? '404' ];

    } catch (\Exception $e) 
    {
      echo $e->getMessage();
      $message[ 'status' ] = false;
      $message[ 'response' ] = $e->getMessage();
      Sentry::captureException($e);
        return   ['response' => $e->getMessage(),  'status_code' => '404'];
    }
    
  }
}