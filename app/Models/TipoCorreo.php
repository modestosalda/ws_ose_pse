<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * Modelo para Emisores PSE
 * Created By Modesto Saldaña Michalec<modesto_salda_m93@gmail.com>
 */

class TipoCorreo extends Model
{

    
    public $table = 'sh_cloud_pse.tb_tipo_correo';

    protected $fillable = [ 'id_tipo_correo' ];

}
