<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NotificationEmailCpeEmision
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    public $dataCpe;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct( $dataCpe )
    {
        $this->dataCpe = $dataCpe;
        logger("ejecutando queue3");
        echo "hola como estas?";
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
