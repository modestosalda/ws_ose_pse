<?php
namespace App\Handlers;
use App\Models\FacturaCabCPE ;
use App\Models\BoletaCabCPE ;
use App\Models\NotaCreditoCPE;
use App\Models\NotaDebitoPSE;
use App\Models\PercepcionCPE;
use App\Models\RetencionCPE;
use App\Models\ComprobantesPSE;
use App\Models\ComprobanteAllPSE;

use App\Models\DocumentosReferenciadosPSE;
use App\Models\JobInfo;
use App\Providers\ProviderServiceSunatOse;
use App\Models\CorreoPlantillaPSE;
use App\Strategies\ServiceEmailContext;
use App\Providers\Client\SendBill;
// use Exception;
use App\Events\NotificationEmailCpeEmision;
use DB;
use Sentry;
use Illuminate\Support\Facades\Cache;
// use function Illuminate\Events\queueable;
// use Illuminate\Support\Facades\Event;
use App\Jobs\SendEmailCpeEmision;
// $file_cola =$directorio_cola. "cola.json";
/**
 * 
 */
class HandlerColaEnvio
{
	private $_modelJob ;
	private $_comprobanteId;
	private $_serie;
	private $_ruc;
	private $_correlativo;
	private $_cpeType;
	private $_config;
	private $serviceEmailContext ;
	private $_handlerManagementSaveCPE ;
	private $_jobDelete ;
	function __construct()
	{
		$this->_modelJob = new JobInfo();
		$this->_modelComprobantePSE = new ComprobantesPSE();
		$this->_modelComprobanteAllPSE = new ComprobanteAllPSE();
		$this->_modelCorrePlantilla = new CorreoPlantillaPSE();
		$this->_handlerManagementSaveCPE = new HandlerManagementSaveCPE();
		/** por el momento contamos con 2 :sendingblue, sendgrid */
		/* Si vemos que el job de envio de email funcion, entonces elminamos esto*/
		$serviceEmailCpe = env('SERVICE_EMAIL_COLA_ENVIO', 'sendingblue') ;
		$this->serviceEmailContext = new ServiceEmailContext( $serviceEmailCpe );
	}
	public function setFileName( $fileName )	{		$this->_fileName = trim($fileName);	}
	public function getFileName()	{	return	$this->_fileName ;	}
	/**
	* Envia el cpe a OSE-SUNAT
	*/
	public function sendCpeEmitidos()
	{
		$jobs =  $this->_modelJob->getJobActiveByType();
		
		$response = '';
		$resultEmail= '';
		$resultUpdateCPE = '';
		$resultUpdateAllCPE = '';
		$saveCpe = '';
		$emailJobs = [];
		if ( $jobs->count() )
		{

			$data = [ 'estado' => 'P' ];
			$jobs->toQuery()->update( $data );
			$inicio   = 'Inicio de envio de comprobantes : '. date('Y-m-d H:m:s.').round(microtime(true) * 1000);
			foreach ( $jobs as  $value ) 
			{
				$providersSunatOse = false;
				try {

					$this->_comprobanteId = $value->tb_comprobante_id;
					echo $this->_comprobanteId . "\n";
					$this->setFileName( $value->tb_comprobante_nomextarccomele );
					$this->validateProperties( $value );
					//** EL DOCUMENTO CAMBIA DE ESTADO A PR CUANDO SE ESTA PROCESANDO *///
					$jobs->toQuery()->where('tb_comprobante_id',$this->_comprobanteId)->update( [ 'estado' => 'PR' ]);
					list( $this->_ruc,$this->_cpeType, $this->_serie , $this->_correlativo  ) = porciones_cpe( $value->tb_comprobante_nomextarccomele );
					$jsonEntrada = json_decode( $value->data_cpe );
					///*Obtiene la url segun el tipo de comprobante*//
					$url = getWsByType($this->_cpeType, $value->emisor ) ;
					// crea el servicio y manda la peticion
					$providersSunatOse = new ProviderServiceSunatOse('sendBill', $url );
					$providersSunatOse->setUser( $value->emisor[ 'tb_emisor_user_sol' ] );
					$providersSunatOse->setPassword( $value->emisor[ 'tb_emisor_clave_sol' ] );
					$providersSunatOse->setBase64( $value->base64zip );
					$providersSunatOse->setFileName( $value->tb_comprobante_nomextarccomele );
					$providersSunatOse->requestService();
					//** EL DOCUMENTO CAMBIA DE ESTADO A SO YA SE ENVIO A SUNAT *///
	                $jobs->toQuery()->where('tb_comprobante_id',$this->_comprobanteId)->update( [ 'estado' => 'SO' ]);
					/** obtenemos la infomracion de la respuesta ya seteada */
					$sendBill = new SendBill( $this->getFileName() );
					$response = $sendBill->getDataXmlResponse( $providersSunatOse->getResponse(),  $value['base64zip'], substr($jsonEntrada->fechaEmision, 0, 10 ) );
					//** EL DOCUMENTO CAMBIA DE ESTADO A SO YA SE ENVIO A SUNAT *///
					$responseCpe = $response['data'];
					
					/** True se manda a reprocesar el documento */
					if ( !$sendBill->getReprocess() ) 
					{

						$fechaEmision =  substr($jsonEntrada->fechaEmision, 0, 10 ) ;
						$pathPDF = get_path_comprobante(  str_replace('.zip', '',  $value->tb_comprobante_nomextarccomele )  ,  'pdf' ,  $fechaEmision, $this->_ruc, $this->_cpeType );

						$nroTicket  = property_exists( $jsonEntrada, 'nroTicket' )  ?  $jsonEntrada->nroTicket : '';
						$emailJobs = [
							'tb_comprobante_id' => $this->_comprobanteId,
							'name_cpe' => $this->getFileName(),
				 			'correo' => $jsonEntrada->correo,
				 			'fecha_emision' => $fechaEmision, 
				 			'nro_documento_identidad' => $jsonEntrada->oAdquiriente->numeroDocumentoIdentidad , 
				 			'base64pdf' => $value[ "base64pdf" ],
				 			'path_file_pdf' => $pathPDF,
				 			'path_file_xml' => $response[ 'path_file_xml' ], 
				 			'path_file_cdr' => $response[ 'path_file_cdr' ], 
				 			'response_service' => $responseCpe[ 'responseService' ],
				 			'razon_social' =>  $jsonEntrada->oAdquiriente->apellidosNombresDenominacionRazonSocial , 
				 			'nro_ticket' => $nroTicket
			 			];
				 		/* ENVIA EL EMAIL **/
						// SendEmailCpeEmision::dispatch( $emailJobs );
						$resultEmail = $this->sendEmailCPE( $jsonEntrada,  $value[ "base64pdf" ], $response[ 'path_file_xml' ] ,$response[ 'path_file_cdr' ], $responseCpe[ 'responseService' ] );
						//** EL DOCUMENTO CAMBIA DE ESTADO A EM YA SE ENVIO EL DOCUMENTO POR EMAIL *///
		                $jobs->toQuery()->where('tb_comprobante_id',$this->_comprobanteId)->update( [ 'estado' => 'EM' ]);

						$isAcceptedOrObserver = $this->isAcceptedOrObserver(  $responseCpe[ 'responseService' ], $this->getFileName() );
     					$isColaGetStatus = $this->isColaGetStatus( $responseCpe[ 'responseCode' ], $isAcceptedOrObserver, $responseCpe[ 'responseService' ] );
     					if ( $isColaGetStatus ) 
		            	{
		            		$this->updateJobInfo( $value );
		            	}else
		            	{
		        			/** ACTUALIZACION DE CPE */
			            	$resultUpdateCPE  = $this->_modelComprobantePSE->updateCompobante( 
			            		$value,
			            		$responseCpe,
			            		$this->getFileName(), 
			            		$resultEmail[ 'status_code' ] ?? '202',
			            		// '202',
			            		$jsonEntrada,
			            		$isAcceptedOrObserver
			            	);
			            	/** ACTUALIZACION DE CPE ALL */
			            	$resultUpdateAllCPE  = $this->_modelComprobanteAllPSE->updateComprobanteAll( 
			            		$value,
			            		$responseCpe,
			            		$this->getFileName(), 
			            		$resultEmail[ 'status_code' ] ?? '202', 
			            		// '202', 
			            		$jsonEntrada
			            	);
		            		$message['job_deleted'] =  env('APP_ENV') !== 'local' ?   $this->_modelJob->deleteJobById( $value->id ) : 'LOCAL';
			            	if ( $responseCpe[ 'responseService' ] == 'A' || $responseCpe[ 'responseService' ] == 'O' ) 
			            	{	
		            			$saveCpe = $this->_handlerManagementSaveCPE->saveCpeEmititos( $response[ 'path_file_xml' ] , $this->_cpeType , $this->_comprobanteId );
		            			//** EL DOCUMENTO CAMBIA DE ESTADO A SV CUANDO SE GUARDA EN SU TABLA *///
			            		$jobs->toQuery()->where('tb_comprobante_id',$this->_comprobanteId)->update( [ 'estado' => 'SV' ]);
			            	}else{
			            		$jobs->toQuery()->where('tb_comprobante_id',$this->_comprobanteId)->update( [ 'estado' => 'RE' ]);
			            	}
		            	}
					}else{
							//** EL DOCUMENTO CAMBIA DE ESTADO A RE CUANDO ES RECHAZADO *///
	            		$jobs->toQuery()->where('tb_comprobante_id',$this->_comprobanteId)->update( [ 'estado' => 'RP' ]);
						$message['message_cpe'] = 'El documento se reprocesara';
						$message['sin_procesar'] = $this->reProcesarCpe();
					}
					$message['start'] = $inicio;

					$message['end'] = 'Final de envio de comprobantes : '. date('Y-m-d H:m:s.').round(microtime(true) * 1000);
					$message['curl_info'] = 'Inicio: ' . $providersSunatOse->getInitCurl() . ' Fin : ' . $providersSunatOse->getEndCurl();
					$message['cpe'] = $response;
					$message['info_email'] = $resultEmail ;
			        $message['update_cpe'] = $resultUpdateCPE;
			        $message['update_cpe_all'] = $resultUpdateAllCPE;
				    $message['save_cpe'] = $saveCpe;
					\Log::stack(['cola_envio'])->info( json_encode( $message , JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES) );
				} catch (\Exception $e) {
					$jobs->toQuery()->where('tb_comprobante_id',$this->_comprobanteId)->update( [ 'estado' => 'ERROR' , 'message' => $e->getMessage()]);
					// echo $e->getMessage() . "\n";
					$this->isReprocese( $providersSunatOse  );
					\Log::stack(['cola_envio'])->error( $e->getMessage() );
					Sentry::captureException( $e );
				}
			}
			
			
		}
		return ['status' => 'false', 'message' => 'no existen CPE de envio'];
		
		
	}
	private function validateProperties( $data )
	{
		if ( !$data->emisor ) 
		{
			throw new \Exception("No cuenta con la configuracion de URL del servicio para el emisor : ". $data->ruc_emisor . " CPE: "  . $data->tb_comprobante_nomextarccomele, 1);
		}
		return true;
	}
	private function isReprocese( $providersSunatOse )
	{
		if ( $providersSunatOse !== false ) 
		{
			if ( $providersSunatOse->getReprocese() === true) 
			{
				echo "entro por aqui a reprocesar";
				$this->reProcesarCpe();
			}
		}
	}
	private function isAcceptedOrObserver( $responseCpe ,$nameCpe )
    {
	    $name = explode( '.' , $nameCpe) ;
	    $porciones = explode('-', $name [0] );
	    $ruc = $porciones[0];
	    $typeCpe = $porciones[1];
	    $serie = $porciones[2];
	    $correlativo = $porciones[3];
	    $status = false;
	    $statusCpe = $this->_modelComprobantePSE->verifyAcceptedStatusAndNameCpe( $nameCpe, $ruc,$typeCpe, $serie, $correlativo );
	    
	    if ( $statusCpe == 'A' || $statusCpe == 'O' ) 
	    {
	        $status = $statusCpe == 'A' ? 1 : 2 ;
	    }
	    return $status;
    
    }
    private function isColaGetStatus( $responseCode, $statusCpe, $responseService ){

    	return (  $responseCode =='1033' && $statusCpe === false && $responseService == 'R') ;
    }
	public function updateJobInfo( $dataCPE )
	{
		$prefijo = "tb_comprobante_";
		$data_info = [
	        'tipo_job' => '1033',// codigo de error : El comprobante fue registrado previamente con otros datos  
	        'estado' => NULL,
	     ]; 
	    $where  = [
	        $prefijo . "id" => $dataCPE[ 'tb_comprobante_id' ],
	        $prefijo . "all_id" => $dataCPE[ 'tb_comprobante_all_id' ]
	    ];

	    $this->_modelJob->where( $where )->update( $data_info );
	    $response['job_info'] ='Se Actualizo la tabla job_info a 1033, idComprobante :' . $dataCPE[ 'tb_comprobante_id' ] . ", NombreComprobante :" . $this->getFileName();
	    $response['message'] ='se procesara nuevamente idComprobante :' . $dataCPE[ 'tb_comprobante_id' ] . ", NombreComprobante :" . $this->getFileName();
	}
	/**
	*
	*/
	private function sendEmailCPE( $jsonEntrada,$base64PDF, $pathXMLENVIO,$pathFileCDR, $responseService )
	{
		try {
			if ( env('SEND_EMAIL_ACTIVE', true) !== true ) {
				return 'Configurado para no enviar email';
			}
			$fechaEmision =  substr($jsonEntrada->fechaEmision, 0, 10 ) ;
			$ruc_adquiriente_encrypt = openssl_encrypt( $jsonEntrada->oAdquiriente->numeroDocumentoIdentidad , env( 'OPENSSL_METHOD_CIPTHERING' ), env( 'DECRYPTION_KEY' ), env( 'OPENSSL_OPTION' ), env( 'OPENSSL_IV' ) );
			$recordEmail =
	    	[
	    		'ruc' =>  $this->_ruc,
	    		'serie' => $this->_serie ,
	    		'numero' => $this->_correlativo ,
	    		'tipo_cpe' =>  $this->_cpeType ,
	    		'razon_social'  =>  $jsonEntrada->oAdquiriente->apellidosNombresDenominacionRazonSocial ,
	    		'fecha' => $fechaEmision,
	    		'token_registro' => $ruc_adquiriente_encrypt,
	    		'enlace_front_sesion_adquiriente' => true,
	    		'enlace_front_registro_adquiriente' => true,
	    		'to_email' => $jsonEntrada->correo
	    	
	    	];
	    	$file_cpe = [];
	    	if ( $responseService !== 'R' ) 
	    	{
    			$fileAnexo = property_exists( $jsonEntrada, 'nroTicket' ) ?
    			get_anexo_by_ticket(  $this->_ruc,  $jsonEntrada->nroTicket ) : [];
	    		
	    		$file_cpe = [
	    			['file_name' => $this->getFileName() , 'path_file' =>  $pathFileCDR , 'formato' => 'zip'], 
	    			['file_name' => str_replace('zip', 'pdf', $this->getFileName() ) , 'base64' =>  $base64PDF  ,'formato'=> 'pdf' ], 
	    			['file_name' => str_replace('zip', 'xml', $this->getFileName() ) , 'path_file' => $pathXMLENVIO  , 'formato' => 'xml']  
	    		];
	    		if ( count( $fileAnexo ) > 0) 
			    {
			    	if ( $fileAnexo['status'] && !empty(  trim( $fileAnexo['file'] )  ) && !empty(  $fileAnexo['name_file']  ) )  
			    	{
						$fileElement = [ 'file_name' => $fileAnexo['name_file'], 'base64' =>  $fileAnexo['file'] ];
			    		array_push($file_cpe , $fileElement );
			    	}
			    }
	    	}
	    	$recordEmail[ 'tipo_correo' ] = $responseService == 'R' ? 4 : 2;
	    	$template = $this->_modelCorrePlantilla->get_email_template( $recordEmail );

	    	$bodyHtml = $template[ 'message' ];
	    	$subject = $template[ 'subject' ];
	    	$toEmail = $jsonEntrada->correo . ','. $template[ 'con_copia' ];
	    	$sender = get_subjet_by_ruc( $this->_ruc );
	    	// 
	    	$providerServiceEmail = $this->serviceEmailContext->instaceService( $toEmail, $sender, $subject, $file_cpe , $bodyHtml  );
	    	$responseEmail = $providerServiceEmail->sendEmail();
	    	$responseEmail[ 'record' ] = $recordEmail;
	    	return [ 'response' => $responseEmail,  'status_code' =>  $responseEmail[ 'code' ] ?? '404' ];

		} catch (\Exception $e) 
		{
			$message[ 'status' ] = false;
			$message[ 'response' ] = $e->getMessage();
			Sentry::captureException($e);
    		return   ['response' => $e->getMessage(),  'status_code' => '404'];
		}
		
	}
	/**
	* cuando el servicio devuelve ciertos estados que son para reprocesar, entonces se prepara el comprobante para volver a enviarse 
	*/
	private function reProcesarCpe() 
	{
		try {
			$dataJob = [
			'estado' => NULL,
			'tipo_job' => 'E'
			];
			$this->_modelJob->updateJobByIdComprobante(  $this->_comprobanteId, $dataJob );
			return 'Se actualiza y manda a reprocesar Comprobante ID:'.$this->_comprobanteId. ' fileName: '.$this->getFileName();
			// \Log::stack(['cola_envio'])->info(  );
		} catch (\Exception $e ) {
			write_log( $e->getMessage(). " en la funcion reProcesarCpe() idComprobante : ". $idComprobante  );
		}
			
	}
	
}