<?php 
namespace App\Handlers;
use App\Models\DocumentosReferenciadosPSE;
use App\Models\FacturaCabCPE ;
use App\Models\BoletaCabCPE ;
use App\Models\NotaCreditoCPE;
use App\Models\NotaDebitoPSE;
use App\Models\PercepcionCPE;
use App\Models\RetencionCPE;
use App\Models\ComprobantesPSE;
use App\Models\ComprobanteAllPSE;
use Sentry;
/**
 * 
 */
class HandlerManagementSaveCPE 
{
	function __construct()
	{
		$this->_modelDocumentosReferenciadosPSE = new DocumentosReferenciadosPSE();
		$this->_modelFacturaCab = new FacturaCabCPE();
		$this->_modelBoletaCab = new BoletaCabCPE();
		$this->_modelNotaCredito = new NotaCreditoCPE();
		$this->_modelNotaDebito = new NotaDebitoPSE();
		$this->_modelPercepcion = new PercepcionCPE();
		$this->_modelRetencion = new RetencionCPE();
		$this->_modelComprobantePSE = new ComprobantesPSE();
	}
	/**
	* guarda la inforamcion del xml dada la ruta
	* @param $fileruta ruta del archivo
	* @param $tipoCpe tipo de documento "01", "03", "20"
	* @param $comprobanteId id del comprobante
	*/
	function saveCpeEmititos( $FileRuta, $tipoCpe , $comprobanteId = 0)
	{
		try {
			
				$message['ruta_archivo'] = $FileRuta;
				if ( !file_exists($FileRuta) ) 
				{
					throw new \Exception("El archivo XML no existe o no tiene permiso para acceder", 2);
				}
				$numId=0;
				$modelDocumentosReferenciados = $this->_modelDocumentosReferenciadosPSE;
				if ( $tipoCpe== '01' || $tipoCpe=='03' || $tipoCpe == '07' || $tipoCpe == '08') 
				{
					$classXmlComprobante = new HandlerXmlCpe( $FileRuta,$tipoCpe );
					$resultArraCab = $classXmlComprobante->setDataXml();
				}elseif( $tipoCpe == '40' || $tipoCpe == '20' )
				{
					$classXmlComprobante = new HandlerXmlCpePerRet( $FileRuta,$tipoCpe );
					$resultArraCab = $classXmlComprobante->setDataXml();
					
				}
				switch ($tipoCpe) 
				{
					case '01':// FACTURA
							$modelFacturaCab =$this->_modelFacturaCab;
							$resultArrayDocRela = $classXmlComprobante->setDataDocRela();
							if ( count( $resultArrayDocRela ) )
							{
								$arrayNumDoc = array_column($resultArrayDocRela, 'num_doc_rela');//extrae todos los numero de documentos relacionados
								$dataDocRela = $this->_modelComprobantePSE->getComprobatesByNumDocs( $arrayNumDoc, $resultArraCab['datos_emisor']['ruc_emisor'], ["'A'", "'O'"] );
								if ( count($dataDocRela) ) 
								{
									$this->_modelDocumentosReferenciadosPSE->saveDocRef( $dataDocRela ,$resultArraCab, $comprobanteId , 'ANT' );
								}else
								{
									$this->_modelDocumentosReferenciadosPSE->saveXml( $resultArrayDocRela ,$resultArraCab, $comprobanteId , 'ANT' );
								}
							}
							$resultSaveCab = $modelFacturaCab->saveCabXml( $resultArraCab );
							$message[ 'resgistro_registro_1' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura';
							$numId = $resultSaveCab['num_id'];

							$resultDet = $classXmlComprobante->setDataDetalle( $numId );
							$resultNota = $classXmlComprobante->setDataNota(  $numId);
							$resultTributo = $classXmlComprobante->setDataTributo(  $numId );

							$modelFacturaCab->saveDetXml( $resultDet );
							$message[ 'resgistro_registro_2' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura detalle';
							$modelFacturaCab->saveNotaXml( $resultNota );
							$message[ 'resgistro_registro_3' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura nota';
							$modelFacturaCab->saveTributoXml( $resultTributo );
							$message[ 'resgistro_registro_4' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura tributo';
						break;
					case '03':// BOLETA
							$modelBoletaCab   = $this->_modelBoletaCab;
							$resultArrayDocRela = $classXmlComprobante->setDataDocRela();
							if ( count( $resultArrayDocRela ) )
							{
								$arrayNumDoc = array_column($resultArrayDocRela, 'num_doc_rela');//extrae todos los numero de documentos relacionados
								$dataDocRela = $this->_modelComprobantePSE->getComprobatesByNumDocs( $arrayNumDoc, $resultArraCab['datos_emisor']['ruc_emisor'], ["'A'", "'O'"] );
								if ( count($dataDocRela) ) 
								{
									$modelDocumentosReferenciados->saveDocRef( $dataDocRela ,$resultArraCab, $comprobanteId , 'ANT' );
								}else
								{
									$modelDocumentosReferenciados->saveXml( $resultArrayDocRela ,$resultArraCab, $comprobanteId , 'ANT' );
								}
							}
							$resultSaveCab = $modelBoletaCab->saveCabXml( $resultArraCab );
							$message[ 'resgistro_registro_1' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura';
							$numId = $resultSaveCab['num_id'];

							$resultDet = $classXmlComprobante->setDataDetalle( $numId );
							$resultNota = $classXmlComprobante->setDataNota(  $numId);
							$resultTributo = $classXmlComprobante->setDataTributo(  $numId );

							$modelBoletaCab->saveDetXml( $resultDet );
							$message[ 'resgistro_registro_2' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura detalle';
							$modelBoletaCab->saveNotaXml( $resultNota );
							$message[ 'resgistro_registro_3' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura nota';
							$modelBoletaCab->saveTributoXml( $resultTributo );
							$message[ 'resgistro_registro_4' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura tributo';
						break;
					case '07':// NOTA DE CREDITO
							$model = $this->_modelNotaCredito;

							$resultArrayDocRela = $classXmlComprobante->setDataDocRela();
						
							$resultArrayDocAfecta = $classXmlComprobante->setDocumentoAfecta();

							if ( count( $resultArrayDocRela ) )
							{
								$arrayNumDoc = array_column($resultArrayDocRela, 'num_doc_rela');//extrae todos los numero de documentos relacionados
								$dataDocRela = $this->_modelComprobantePSE->getComprobatesByNumDocs( $arrayNumDoc, $resultArraCab['datos_emisor']['ruc_emisor'], ["'A'", "'O'"] );
								if ( count($dataDocRela) ) 
								{
									$modelDocumentosReferenciados->saveDocRef( $dataDocRela ,$resultArraCab, $comprobanteId , 'REF' );
								}else
								{
									$modelDocumentosReferenciados->saveXml( $resultArrayDocRela ,$resultArraCab, $comprobanteId , 'REF' );
								}
							}

							$resultSaveCab = $model->saveCabXmlNC( $resultArraCab, $resultArrayDocAfecta );
							
							$message[ 'resgistro_registro_1' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla nota de credito cabecera';
							$numId = $resultSaveCab['num_id'];
							$resultDet = $classXmlComprobante->setDataDetalle( $numId );
							$resultNota = $classXmlComprobante->setDataNota(  $numId);
							$resultTributo = $classXmlComprobante->setDataTributo(  $numId );

							$model->saveDetXml( $resultDet );
							$message[ 'resgistro_registro_2' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla nota de credito  detalle';
							$model->saveNotaXml( $resultNota );
							$message[ 'resgistro_registro_3' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla nota de credito  nota';
							$model->saveTributoXml( $resultTributo );
							$message[ 'resgistro_registro_4' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla nota de credito  tributo';
						break;
					case '08'://NOTA DE DEBITO

							$model = $this->_modelNotaDebito;
							$resultArrayDocRela = $classXmlComprobante->setDataDocRela();
							$resultArrayDocAfecta = $classXmlComprobante->setDocumentoAfecta();
							if ( count( $resultArrayDocRela ) )
							{
								$arrayNumDoc = array_column($resultArrayDocRela, 'num_doc_rela');//extrae todos los numero de documentos relacionados
								$dataDocRela = $this->_modelComprobantePSE->getComprobatesByNumDocs( $arrayNumDoc, $resultArraCab['datos_emisor']['ruc_emisor'], ["'A'", "'O'"] );
								if ( count($dataDocRela) ) 
								{
									$modelDocumentosReferenciados->saveDocRef( $dataDocRela ,$resultArraCab, $comprobanteId , 'REF' );
								}else
								{
									$modelDocumentosReferenciados->saveXml( $resultArrayDocRela ,$resultArraCab, $comprobanteId , 'REF' );
								}
							}
							$resultSaveCab = $model->saveCabXmlND( $resultArraCab, $resultArrayDocAfecta );
							$message[ 'resgistro_registro_1' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura';
							$numId = $resultSaveCab['num_id'];

							$resultDet = $classXmlComprobante->setDataDetalle( $numId );

							
							$resultNota = $classXmlComprobante->setDataNota(  $numId);
							
							$resultTributo = $classXmlComprobante->setDataTributo(  $numId );
							$model->saveDetXml( $resultDet );
							
							$message[ 'resgistro_registro_2' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura detalle';
							$model->saveNotaXml( $resultNota );
							$message[ 'resgistro_registro_3' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura nota';
							$model->saveTributoXml( $resultTributo );
							$message[ 'resgistro_registro_4' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura tributo';
						break;
					case '20'://RETENCION

							$model = $this->_modelRetencion;
							$resultSaveCab = $model->saveCabXmlRET( $resultArraCab );
							$message[ 'resgistro_registro_1' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla RETENCION CABECERA';
							$numId = $resultSaveCab['num_id'];
							
							$resultDet = $classXmlComprobante->setDataDetalle( $numId );
							$resultNota = $classXmlComprobante->setDataNota(  $numId);
							$model->saveDetXmlRET( $resultDet );
							$message[ 'resgistro_registro_2' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla RETENCION detalle';
							$model->saveNotaXmlRET( $resultNota );
							$message[ 'resgistro_registro_3' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla RETENCION nota';
						break;
					case '40'://PERCEPCION
							$model = $this->_modelPercepcion;
							$resultSaveCab = $model->saveCabXmlPER( $resultArraCab );
							$message[ 'resgistro_registro_1' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla PERCPECION CABECERA';
							$numId = $resultSaveCab['num_id'];
							
							$resultDet = $classXmlComprobante->setDataDetalle( $numId );
							$resultNota = $classXmlComprobante->setDataNota(  $numId);
							$model->saveDetXmlPER( $resultDet );
							$message[ 'resgistro_registro_2' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla PERCPECION detalle';
							$model->saveNotaXmlPER( $resultNota );
							$message[ 'resgistro_registro_3' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla PERCPECION nota';
						break;
				}
		} catch (\Exception $e) 
		{
			// echo $e->getMessage();
			Sentry::captureException($e);
			$message = 'No se pudo guardar en las tablas CPE';
		}
		return $message;
	}

	function saveAnulacionCPE( $json, $tipoCpe, $comprobante_id , $nombreCpe)
    {
        $json = json_decode( $json );
        switch ($tipoCpe) 
        {
            case 'RA':
                $modelComunicacionBaja = new Model_Comunicacion_Baja_CPE();
                $ruc = $json->{"oEmisor"}->{"NumeroIdentificacion"};
                $ident = $json->{"Identificador"};
                $fechaGen = $json->{"FechaGeneracion"};
                $fechaEmi = $json->{"FechaEmisionComprobante"};
                $arrayResultComunicacionBaja = $modelComunicacionBaja->getComunicacionBajaCabByIdentificador(  $ident , $ruc ) ;
                if ( count($arrayResultComunicacionBaja) != 0) 
                {
                     $message = 'Ya existe un cpe previamente registrado NOMBRE_CPE:'. $nombreCpe . ' ID:'. $comprobante_id;   
                     echo $message;
                    break;
                }
              
                $comunicacionBajaId = 0;
                $prefijoCab = 'tb_comunicacionb_';

                $dataSaveCab = 
                            [ 
                                $prefijoCab . 'ruc' => $ruc , 
                                $prefijoCab . 'identif' => $ident, 
                                $prefijoCab . 'fechemi' => $fechaEmi , 
                                $prefijoCab . 'fechgen' => $fechaGen , 
                                $prefijoCab . 'comprobante_id' => $comprobante_id , 
                                $prefijoCab . 'fechreg' => 'now()' , 
                                $prefijoCab . 'estado' => 'P' 
                            ];
               
                $comunicacionBajaId = $modelComunicacionBaja->saveDataCab( $dataSaveCab )[ 'last_val' ];
                $detalleResumen = $json->{ "lDetalleComunicacionBaja" };
                foreach($detalleResumen as $cpe )
                {
                    $NumeroOrden=   $cpe->{'NumeroOrden'}; $motivoBaja= $cpe->{'MotivoBaja'}; 
                    $nombreCPE = $ruc . "-" . $cpe->{'TipoDocumento'} . "-" . $cpe->{'NumeroSerie'} . "-" . $cpe->{'NumeroCorrelativo'};
                    // array_push($cpe_anulados  , $nombreCPE);
                    
                    $prefijo = 'tb_comudeta_';
                    $dataSaveDet = 
                                    [
                                        $prefijo.'identif' => $ident,
                                        $prefijo.'numeroorden' => $NumeroOrden,
                                        $prefijo.'nombrecpe' => $nombreCPE,
                                        $prefijo.'estado' => '1',
                                        $prefijo.'comunicacionb_cod' => $comunicacionBajaId,
                                        $prefijo.'motivo' => $motivoBaja
                                    ];
                //      var_dump($dataSaveDet);
                // exit;
                    $modelComunicacionBaja->saveDataDet( $dataSaveDet );
                }  
                $message = 'Se guardo correctamente en la tabla COMUNICACION DE BAJA';
            break;
            case 'RC':

                $modelResumenDiario = new Model_Resumen_Diario_CPE();
                
                $ruc = $json->{"oEmisor"}->{"NumeroIdentificacion"};
                $ident = $json->{"Identificador"};
                $arrayResult= $modelResumenDiario->getResumenDiarioCabAndDetByIdentificador(  $ident , $ruc ) ;
                if ( count($arrayResult ) != 0) 
                {
                     $message = 'Ya existe un cpe previamente registrado NOMBRE_CPE:'. $nombreCpe . ' ID:'. $comprobante_id;   
                     echo $message;
                    break;
                }
               
                // $mensaje[ 'det_resumen' ] =  "Se ha Insertado en tb_resumendiario_detalle";
                $fechaGen = $json->{"FechaGeneracion"};
                $fechaEmi = $json->{"FechaEmisionComprobante"};
                $dataSave = [ 
                                'tb_resumend_ruc' => $ruc , 
                                'tb_resumend_identif' => $ident, 
                                'tb_resumend_fechemi' => $fechaEmi , 
                                'tb_resumend_fechgen' => $fechaGen , 
                                'tb_resumend_fechreg' => 'now()' , 
                                'tb_resumend_estado' => 'P' 
                            ];
                $modelResumenDiario->saveDataCab( $dataSave );
                $detalleResumen = $json->{"lDetalleResumenDiarioV2"};
                foreach($detalleResumen as $cpe )
                {
                    $prefijo = 'tb_resumdeta_'; $NumeroOrden = $cpe->{'NumeroOrden'} ; $Estado=$cpe->{'Estado'};
                    $nombreCPE = $ruc . "-" . $cpe->{'TipoDocumento'} . "-" . $cpe->{'NumeroSerie'} . "-" . $cpe->{'NumeroCorrelativo'};
                    $dataSaveDet = 
                                [
                                    $prefijo.'identif' => $ident,
                                    $prefijo.'numeroorden' => $NumeroOrden,
                                    $prefijo.'nombrecpe' => $nombreCPE,
                                    $prefijo.'estado' => $Estado,
                                ];
                    $modelResumenDiario->saveDataDet( $dataSaveDet );
                }
                  $message = 'Se guardo correctamente en la tabla Resumen diario';
                break;
            case 'RR':
                    $modelResumenReversion = new Model_Resumen_Reversion_CPE();
                    
                    $ruc = $json->{"oEmisor"}->{"NumeroIdentificacion"};
                    $ident = $json->{"Identificador"};

                    $arrayResult= $modelResumenReversion->getResumenReversionCabAndDetByIdentificador( trim( $ident) , trim($ruc)  ) ;
                    if ( count($arrayResult ) != 0) 
                    {
                        $message = 'Ya existe un cpe previamente registrado NOMBRE_CPE:'. $nombreCpe . ' ID:'. $comprobante_id;   
                        echo $message;
                        break;
                    }
                   
                    // $mensaje[ 'det_resumen' ] =  "Se ha Insertado en tb_resumendiario_detalle";
                    $fechaGen = $json->{"FechaGeneracion"};
                    $fechaEmi = $json->{"FechaEmisionComprobante"};
                    $prefijo = "tb_reversiones_";
                    $dataSave = [ 
                                    $prefijo.'ruc' => $ruc , 
                                    $prefijo . 'identif' => $ident, 
                                    $prefijo . 'fechemi' => $fechaEmi , 
                                    $prefijo . 'fechgen' => $fechaGen , 
                                    $prefijo . 'fechreg' => 'now()' , 
                                    $prefijo . 'estado' => 'P' 
                                ];
                    $modelResumenReversion->saveDataResumenReversionCab( $dataSave );
                    $detalleResumen = $json->{"lDetalleReversion"};
                   
                    foreach($detalleResumen as $cpe )
                    {
                        $prefijo = 'tb_reverdeta_'; $NumeroOrden = $cpe->{'NumeroOrden'} ; $Estado=$cpe->{'Estado'};
                        $nombreCPE = $ruc . "-" . $cpe->{'TipoDocumento'} . "-" . $cpe->{'NumeroSerie'} . "-" . $cpe->{'NumeroCorrelativo'};
                        $motivo = $cpe->{'MotivoBaja'};
                        $dataSaveDet = 
                                    [
                                        $prefijo.'identif' => $ident,
                                        $prefijo.'numeroorden' => $NumeroOrden,
                                        $prefijo.'nombrecpe' => $nombreCPE,
                                        $prefijo.'estado' => 3,
                                        $prefijo.'motivo' => $motivo,
                                    ];
                        $modelResumenReversion->saveDataResumenReversionDet( $dataSaveDet );
                    }
                    $message = 'Se guardo correctamente en la tabla Resumen reversiones';
                
                break;
            default:
                # code...
                break;
        }
        return $message;
        
    }
}