<?php
namespace App\Handlers;
use App\Models\FacturaCabCPE ;
use App\Models\BoletaCabCPE ;
use App\Models\NotaCreditoCPE;
use App\Models\NotaDebitoPSE;
use App\Models\PercepcionCPE;
use App\Models\RetencionCPE;
use App\Models\ComprobantesPSE;
use App\Models\ComprobanteAllPSE;

use App\Models\DocumentosReferenciadosPSE;
use App\Models\JobInfo;
use App\Providers\ProviderServiceSunatOse;
use App\Models\CorreoPlantillaPSE;
use App\Strategies\ServiceEmailContext;
use App\Providers\Client\SendSummary;
// use Exception;
use DB;
use Sentry;
// $file_cola =$directorio_cola. "cola.json";
/**
 * 
 */
class HandlerUpdateSendColaEnvio
{
	private $_modelJob ;
	private $_comprobanteId;
	private $_serie;
	private $_ruc;
	private $_correlativo;
	private $_cpeType;
	private $_config;
	private $serviceEmailContext ;
	private $_jobDelete ;
	function __construct()
	{
		$this->_modelJob = new JobInfo();
		$this->_modelComprobantePSE = new ComprobantesPSE();
		$this->_modelComprobanteAllPSE = new ComprobanteAllPSE();
		$this->_modelCorrePlantilla = new CorreoPlantillaPSE;
		$this->_modelFacturaCab = new FacturaCabCPE;
		$this->_modelBoletaCab = new BoletaCabCPE;
		$this->_modelDocumentosReferenciadosPSE = new DocumentosReferenciadosPSE();
		/** por el momento contamos con 2 :sendingblue, sendgrid */
		$serviceEmailCpe = env('SERVICE_EMAIL_COLA_ENVIO', 'sendingblue') ;
		$this->serviceEmailContext = new ServiceEmailContext( $serviceEmailCpe );
	}
	public function setFileName( $fileName )	{		$this->_fileName = trim($fileName);	}
	public function getFileName()	{	return	$this->_fileName ;	}
	/**
	* Envia el cpe a OSE-SUNAT
	*/
	public function sendCpeEmitidos()
	{
		$jobs =  $this->_modelJob->getJobsSends();
		
		$response = '';
		$resultEmail= '';
		$resultUpdateCPE = '';
		$resultUpdateAllCPE = '';
		$saveCpe = '';
		if ( $jobs->count() )
		{
			//ESTADO ENVIANDO
			$data = [ 'estado' => 'EN' ];
			$jobs->toQuery()->update( $data );
			$inicio   = 'Inicio de envio de comprobantes : '. date('Y-m-d H:m:s.').round(microtime(true) * 1000);
			foreach ( $jobs as  $value ) 
			{
				$providersSunatOse = false;
				try {
					$this->_comprobanteId = $value->tb_comprobante_id;
					$this->setFileName( $value->tb_comprobante_nomextarccomele );
					$this->validateProperties( $value );
					echo "Comprobante: ".  $value->tb_comprobante_id . "\n";
					//** EL DOCUMENTO CAMBIA DE ESTADO A PR CUANDO SE ESTA PROCESANDO *///
					
					$jobs->toQuery()->where('tb_comprobante_id',$this->_comprobanteId)->update( [ 'estado' => 'PR' ]);
					list( $this->_ruc,$this->_cpeType, $this->_serie , $this->_correlativo  ) = porciones_cpe( $value->tb_comprobante_nomextarccomele );
					$responseCpe = $response['data'];
					/** True se manda a reprocesar el documento */
			 		/* ENVIA EL EMAIL **/
					$resultEmail = $this->sendEmailCPE( $jsonEntrada,  $value[ "base64pdf" ], $response[ 'path_file_xml' ] ,$response[ 'path_file_cdr' ], $responseCpe[ 'responseService' ] );
					//** EL DOCUMENTO CAMBIA DE ESTADO A EM YA SE ENVIO EL DOCUMENTO POR EMAIL *///
	                $jobs->toQuery()->where('tb_comprobante_id',$this->_comprobanteId)->update( [ 'estado' => 'EM' ]);

					$isAcceptedOrObserver = $this->isAcceptedOrObserver(  $responseCpe[ 'responseService' ], $this->getFileName() );
 					$isColaGetStatus = $this->isColaGetStatus( $responseCpe[ 'responseCode' ], $isAcceptedOrObserver, $responseCpe[ 'responseService' ] );
 					if ( $isColaGetStatus ) 
	            	{
	            		$this->updateJobInfo( $value );
	            	}else
	            	{
	        			/** ACTUALIZACION DE CPE */
		            	$resultUpdateCPE  = $this->_modelComprobantePSE->updateCompobante( 
		            		$value,
		            		$responseCpe,
		            		$this->getFileName(), 
		            		$resultEmail[ 'status_code' ] ?? '202',
		            		$jsonEntrada,
		            		$isAcceptedOrObserver
		            	);
		            	/** ACTUALIZACION DE CPE ALL */
		            	$resultUpdateAllCPE  = $this->_modelComprobanteAllPSE->updateComprobanteAll( 
		            		$value,
		            		$responseCpe,
		            		$this->getFileName(), 
		            		$resultEmail[ 'status_code' ] ?? '202', 
		            		$jsonEntrada
		            	);
	            		$message['job_deleted'] =  env('APP_ENV') !== 'local' ?   $this->_modelJob->deleteJobById( $value->id ) : 'LOCAL';
		            	if ( $responseCpe[ 'responseService' ] == 'A' || $responseCpe[ 'responseService' ] == 'O' ) 
		            	{	
	            			$saveCpe = $this->saveCpe( $response[ 'path_file_xml' ] , $this->_cpeType , $this->_comprobanteId );	
	            			//** EL DOCUMENTO CAMBIA DE ESTADO A SV CUANDO SE GUARDA EN SU TABLA *///
		            		$jobs->toQuery()->where('tb_comprobante_id',$this->_comprobanteId)->update( [ 'estado' => 'SV' ]);
		            	}else{
		            		$jobs->toQuery()->where('tb_comprobante_id',$this->_comprobanteId)->update( [ 'estado' => 'RE' ]);
		            	}
						//** EL DOCUMENTO CAMBIA DE ESTADO A RE CUANDO ES RECHAZADO *///
	            	}
					$message['start'] = $inicio;
					$message['end'] = 'Final de envio de comprobantes : '. date('Y-m-d H:m:s.').round(microtime(true) * 1000);
					$message['cpe'] = $response;
					$message['info_email'] = $resultEmail ;
			        $message['update_cpe'] = $resultUpdateCPE;
			        $message['update_cpe_all'] = $resultUpdateAllCPE;
				    $message['save_cpe'] = $saveCpe;
					\Log::stack(['cola_envio'])->info( json_encode( $message , JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES) );
				} catch (\Exception $e) {
					$jobs->toQuery()->where('tb_comprobante_id',$this->_comprobanteId)->update( [ 'estado' => 'ERROR' , 'message' => $e->getMessage()]);
					$this->isReprocese( $providersSunatOse  );
					\Log::stack(['cola_envio'])->error( $e->getMessage() );
					Sentry::captureException( $e );
				}
			}
		}
		return ['status' => 'false', 'message' => 'no existen CPE de envio'];
		
		
	}
	private function validateProperties( $data )
	{
		if ( !$data->emisor ) 
		{
			throw new \Exception("No cuenta con la configuracion de URL del servicio para el emisor : ". $data->ruc_emisor . " CPE: "  . $data->tb_comprobante_nomextarccomele, 1);
		}
		return true;
	}
	private function isReprocese( $providersSunatOse )
	{
		if ( $providersSunatOse !== false ) 
		{
			var_dump($providersSunatOse->getReprocese());
			if ( $providersSunatOse->getReprocese() === true) 
			{
				echo "entro por aqui a reprocesar";
				$this->reProcesarCpe();
			}
		}
	}
	private function isAcceptedOrObserver( $responseCpe ,$nameCpe )
    {
	    $name = explode( '.' , $nameCpe) ;
	    $porciones = explode('-', $name [0] );
	    $ruc = $porciones[0];
	    $typeCpe = $porciones[1];
	    $serie = $porciones[2];
	    $correlativo = $porciones[3];
	    $status = false;
	    $statusCpe = $this->_modelComprobantePSE->verifyAcceptedStatusAndNameCpe( $nameCpe, $ruc,$typeCpe, $serie, $correlativo );
	    
	    if ( $statusCpe == 'A' || $statusCpe == 'O' ) 
	    {
	        $status = $statusCpe == 'A' ? 1 : 2 ;
	    }
	    return $status;
    
    }
    private function isColaGetStatus( $responseCode, $statusCpe, $responseService ){

    	if (  $responseCode =='1033' && $statusCpe === false && $responseService == 'R') 
	    {
	        return true;
	    }
	    return false;
    }
	public function updateJobInfo( $dataCPE )
	{
		$prefijo = "tb_comprobante_";
		$data_info = [
	        'tipo_job' => '1033',// codigo de error : El comprobante fue registrado previamente con otros datos  
	        'estado' => NULL,
	     ]; 
	    $where  = [
	        $prefijo . "id" => $dataCPE[ 'tb_comprobante_id' ],
	        $prefijo . "all_id" => $dataCPE[ 'tb_comprobante_all_id' ]
	    ];

	    $this->_modelJob->where( $where )->update( $data );
	    $response['job_info'] ='Se Actualizo la tabla job_info a 1033, idComprobante :' . $dataCPE[ 'tb_comprobante_id' ] . ", NombreComprobante :" . $nameFile;
	    $response['message'] ='se procesara nuevamente idComprobante :' . $dataCPE[ 'tb_comprobante_id' ] . ", NombreComprobante :" . $nameFile;
	}
	/**
	* guarda la inforamcion del xml dada la ruta
	* @param $fileruta ruta del archivo
	* @param $tipoCpe tipo de documento "01", "03", "20"
	* @param $comprobanteId id del comprobante
	*/
	function saveCpe( $FileRuta, $tipoCpe , $comprobanteId = 0)
	{

		try {
				$message['ruta del archivo'] = $FileRuta;
				if ( !file_exists($FileRuta) ) 
				{
					throw new \Exception("El archivo XML no existe o no tiene permiso para acceder", 2);
				}
				$numId=0;
				$modelDocumentosReferenciados = $this->_modelDocumentosReferenciadosPSE;
				if ( $tipoCpe== '01' || $tipoCpe=='03' || $tipoCpe == '07' || $tipoCpe == '08') 
				{
					$classXmlComprobante = new HandlerXmlCpe( $FileRuta,$tipoCpe );
					$resultArraCab = $classXmlComprobante->setDataXml();
				}elseif( $tipoCpe == '40' || $tipoCpe == '20' )
				{
					$classXmlComprobante = new HandlerXmlCpePerRet( $FileRuta,$tipoCpe );
					$resultArraCab = $classXmlComprobante->setDataXml();
					
				}
				switch ($tipoCpe) 
				{
					case '01':// FACTURA
							$modelFacturaCab =$this->_modelFacturaCab;
							$resultArrayDocRela = $classXmlComprobante->setDataDocRela();
							if ( count( $resultArrayDocRela ) )
							{
								$arrayNumDoc = array_column($resultArrayDocRela, 'num_doc_rela');//extrae todos los numero de documentos relacionados
								$dataDocRela = $this->_modelComprobantePSE->getComprobatesByNumDocs( $arrayNumDoc, $resultArraCab['datos_emisor']['ruc_emisor'], ["'A'", "'O'"] );
								if ( count($dataDocRela) ) 
								{
									$modelDocumentosReferenciados->saveDocRef( $dataDocRela ,$resultArraCab, $comprobanteId , 'ANT' );
								}else
								{
									$modelDocumentosReferenciados->saveXml( $resultArrayDocRela ,$resultArraCab, $comprobanteId , 'ANT' );
								}
							}
							$resultSaveCab = $modelFacturaCab->saveCabXml( $resultArraCab );
							$message[ 'resgistro_registro_1' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura';
							$numId = $resultSaveCab['num_id'];

							$resultDet = $classXmlComprobante->setDataDetalle( $numId );
							$resultNota = $classXmlComprobante->setDataNota(  $numId);
							$resultTributo = $classXmlComprobante->setDataTributo(  $numId );

							$modelFacturaCab->saveDetXml( $resultDet );
							$message[ 'resgistro_registro_2' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura detalle';
							$modelFacturaCab->saveNotaXml( $resultNota );
							$message[ 'resgistro_registro_3' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura nota';
							$modelFacturaCab->saveTributoXml( $resultTributo );
							$message[ 'resgistro_registro_4' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura tributo';
						break;
					case '03':// BOLETA
							$modelBoletaCab   = $this->_modelBoletaCab;
							$resultArrayDocRela = $classXmlComprobante->setDataDocRela();
							if ( count( $resultArrayDocRela ) )
							{
								$arrayNumDoc = array_column($resultArrayDocRela, 'num_doc_rela');//extrae todos los numero de documentos relacionados
								$dataDocRela = $this->_modelComprobantePSE->getComprobatesByNumDocs( $arrayNumDoc, $resultArraCab['datos_emisor']['ruc_emisor'], ["'A'", "'O'"] );
								if ( count($dataDocRela) ) 
								{
									$modelDocumentosReferenciados->saveDocRef( $dataDocRela ,$resultArraCab, $comprobanteId , 'ANT' );
								}else
								{
									$modelDocumentosReferenciados->saveXml( $resultArrayDocRela ,$resultArraCab, $comprobanteId , 'ANT' );
								}
							}
							$resultSaveCab = $modelBoletaCab->saveCabXml( $resultArraCab );
							$message[ 'resgistro_registro_1' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura';
							$numId = $resultSaveCab['num_id'];

							$resultDet = $classXmlComprobante->setDataDetalle( $numId );
							$resultNota = $classXmlComprobante->setDataNota(  $numId);
							$resultTributo = $classXmlComprobante->setDataTributo(  $numId );

							$modelBoletaCab->saveDetXml( $resultDet );
							$message[ 'resgistro_registro_2' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura detalle';
							$modelBoletaCab->saveNotaXml( $resultNota );
							$message[ 'resgistro_registro_3' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura nota';
							$modelBoletaCab->saveTributoXml( $resultTributo );
							$message[ 'resgistro_registro_4' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura tributo';
						break;
					case '07':// NOTA DE CREDITO
							$model = new NotaCreditoCPE();

							$resultArrayDocRela = $classXmlComprobante->setDataDocRela();
						
							$resultArrayDocAfecta = $classXmlComprobante->setDocumentoAfecta();

							if ( count( $resultArrayDocRela ) )
							{
								$arrayNumDoc = array_column($resultArrayDocRela, 'num_doc_rela');//extrae todos los numero de documentos relacionados
								$dataDocRela = $this->_modelComprobantePSE->getComprobatesByNumDocs( $arrayNumDoc, $resultArraCab['datos_emisor']['ruc_emisor'], ["'A'", "'O'"] );
								if ( count($dataDocRela) ) 
								{
									$modelDocumentosReferenciados->saveDocRef( $dataDocRela ,$resultArraCab, $comprobanteId , 'REF' );
								}else
								{
									$modelDocumentosReferenciados->saveXml( $resultArrayDocRela ,$resultArraCab, $comprobanteId , 'REF' );
								}
							}

							$resultSaveCab = $model->saveCabXmlNC( $resultArraCab, $resultArrayDocAfecta );
							
							$message[ 'resgistro_registro_1' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla nota de credito cabecera';
							$numId = $resultSaveCab['num_id'];
							$resultDet = $classXmlComprobante->setDataDetalle( $numId );
							$resultNota = $classXmlComprobante->setDataNota(  $numId);
							$resultTributo = $classXmlComprobante->setDataTributo(  $numId );

							$model->saveDetXml( $resultDet );
							$message[ 'resgistro_registro_2' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla nota de credito  detalle';
							$model->saveNotaXml( $resultNota );
							$message[ 'resgistro_registro_3' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla nota de credito  nota';
							$model->saveTributoXml( $resultTributo );
							$message[ 'resgistro_registro_4' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla nota de credito  tributo';
						break;
					case '08'://NOTA DE DEBITO

							$model = new NotaDebitoPSE();
							$resultArrayDocRela = $classXmlComprobante->setDataDocRela();
							$resultArrayDocAfecta = $classXmlComprobante->setDocumentoAfecta();
							if ( count( $resultArrayDocRela ) )
							{
								$arrayNumDoc = array_column($resultArrayDocRela, 'num_doc_rela');//extrae todos los numero de documentos relacionados
								$dataDocRela = $this->_modelComprobantePSE->getComprobatesByNumDocs( $arrayNumDoc, $resultArraCab['datos_emisor']['ruc_emisor'], ["'A'", "'O'"] );
								if ( count($dataDocRela) ) 
								{
									$modelDocumentosReferenciados->saveDocRef( $dataDocRela ,$resultArraCab, $comprobanteId , 'REF' );
								}else
								{
									$modelDocumentosReferenciados->saveXml( $resultArrayDocRela ,$resultArraCab, $comprobanteId , 'REF' );
								}
							}
							$resultSaveCab = $model->saveCabXmlND( $resultArraCab, $resultArrayDocAfecta );
							$message[ 'resgistro_registro_1' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura';
							$numId = $resultSaveCab['num_id'];

							$resultDet = $classXmlComprobante->setDataDetalle( $numId );

							
							$resultNota = $classXmlComprobante->setDataNota(  $numId);
							
							$resultTributo = $classXmlComprobante->setDataTributo(  $numId );
							$model->saveDetXml( $resultDet );
							
							$message[ 'resgistro_registro_2' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura detalle';
							$model->saveNotaXml( $resultNota );
							$message[ 'resgistro_registro_3' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura nota';
							$model->saveTributoXml( $resultTributo );
							$message[ 'resgistro_registro_4' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura tributo';
						break;
					case '20'://RETENCION

							$model = new RetencionCPE();
							$resultSaveCab = $model->saveCabXmlRET( $resultArraCab );
							$message[ 'resgistro_registro_1' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla RETENCION CABECERA';
							$numId = $resultSaveCab['num_id'];
							
							$resultDet = $classXmlComprobante->setDataDetalle( $numId );
							$resultNota = $classXmlComprobante->setDataNota(  $numId);
							$model->saveDetXmlRET( $resultDet );
							$message[ 'resgistro_registro_2' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla RETENCION detalle';
							$model->saveNotaXmlRET( $resultNota );
							$message[ 'resgistro_registro_3' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla RETENCION nota';
						break;
					case '40'://PERCEPCION
							$model = new PercepcionCPE();
							$resultSaveCab = $model->saveCabXmlPER( $resultArraCab );
							$message[ 'resgistro_registro_1' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla PERCPECION CABECERA';
							$numId = $resultSaveCab['num_id'];
							
							$resultDet = $classXmlComprobante->setDataDetalle( $numId );
							$resultNota = $classXmlComprobante->setDataNota(  $numId);
							$model->saveDetXmlPER( $resultDet );
							$message[ 'resgistro_registro_2' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla PERCPECION detalle';
							$model->saveNotaXmlPER( $resultNota );
							$message[ 'resgistro_registro_3' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla PERCPECION nota';
						break;
					
				}
		} catch (\Exception $e) 
		{
			Sentry::captureException($e);
			$message = 'No se pudo guardar en las tablas CPE';
		}
		return $message;
	}
	/**
	*
	*/
	private function sendEmailCPE( $jsonEntrada,$base64PDF, $pathXMLENVIO,$pathFileCDR, $responseService )
	{
		try {
			if ( env('SEND_EMAIL_ACTIVE', true) !== true ) {
				return 'Configurado para no enviar email';
			}
			$fechaEmision =  substr($jsonEntrada->fechaEmision, 0, 10 ) ;
			$ruc_adquiriente_encrypt = openssl_encrypt( $jsonEntrada->oAdquiriente->numeroDocumentoIdentidad , env( 'OPENSSL_METHOD_CIPTHERING' ), env( 'DECRYPTION_KEY' ), env( 'OPENSSL_OPTION' ), env( 'OPENSSL_IV' ) );
			$recordEmail =
	    	[
	    		'ruc' =>  $this->_ruc,
	    		'serie' => $this->_serie ,
	    		'numero' => $this->_correlativo ,
	    		'tipo_cpe' =>  $this->_cpeType ,
	    		'razon_social'  =>  $jsonEntrada->oAdquiriente->apellidosNombresDenominacionRazonSocial ,
	    		'fecha' => $fechaEmision,
	    		'token_registro' => $ruc_adquiriente_encrypt,
	    		'enlace_front_sesion_adquiriente' => true,
	    		'enlace_front_registro_adquiriente' => true,
	    		'to_email' => $jsonEntrada->correo
	    	
	    	];
	    	$file_cpe = [];
	    	if ( $responseService !== 'R' ) 
	    	{
    			$fileAnexo = property_exists( $jsonEntrada, 'nroTicket' ) ?
    			get_anexo_by_ticket(  $this->_ruc,  $jsonEntrada->nroTicket ) : [];
	    		
	    		$file_cpe = [
	    			['file_name' => $this->getFileName() , 'path_file' =>  $pathFileCDR , 'formato' => 'zip'], 
	    			['file_name' => str_replace('zip', 'pdf', $this->getFileName() ) , 'base64' =>  $base64PDF  ,'formato'=> 'pdf' ], 
	    			['file_name' => str_replace('zip', 'xml', $this->getFileName() ) , 'path_file' => $pathXMLENVIO  , 'formato' => 'xml']  
	    		];
	    		if ( count( $fileAnexo ) > 0) 
			    {
			    	if ( $fileAnexo['status'] && !empty(  trim( $fileAnexo['file'] )  ) && !empty(  $fileAnexo['name_file']  ) )  
			    	{
						$fileElement = [ 'file_name' => $fileAnexo['name_file'], 'base64' =>  $fileAnexo['file'] ];
			    		array_push($file_cpe , $fileElement );
			    	}
			    }
	    	}
	    	$recordEmail[ 'tipo_correo' ] = $responseService == 'R' ? 4 : 2;
	    	$template = $this->_modelCorrePlantilla->get_email_template( $recordEmail );

	    	$bodyHtml = $template[ 'message' ];
	    	$subject = $template[ 'subject' ];
	    	$toEmail = $jsonEntrada->correo . ','. $template[ 'con_copia' ];
	    	$sender = get_subjet_by_ruc( $this->_ruc );
	    	// 
	    	$providerServiceEmail = $this->serviceEmailContext->instaceService( $toEmail, $sender, $subject, $file_cpe , $bodyHtml  );
	    	$responseEmail = $providerServiceEmail->sendEmail();
	    	$responseEmail[ 'record' ] = $recordEmail;
	    	return [ 'response' => $responseEmail,  'status_code' =>  $responseEmail[ 'code' ] ?? '404' ];

		} catch (\Exception $e) 
		{
			$message[ 'status' ] = false;
			$message[ 'response' ] = $e->getMessage();
			Sentry::captureException($e);
    		return   ['response' => $e->getMessage(),  'status_code' => '404'];
		}
		
	}
	/**
	* cuando el servicio devuelve ciertos estados que son para reprocesar, entonces se prepara el comprobante para volver a enviarse 
	*/
	private function reProcesarCpe() 
	{
		try {
			$dataJob = [
			'estado' => NULL,
			'tipo_job' => 'E'
			];
			$this->_modelJob->updateJobByIdComprobante(  $this->_comprobanteId, $dataJob );
			return 'Se actualiza y manda a reprocesar Comprobante ID:'.$this->_comprobanteId. ' fileName: '.$this->getFileName();
			// \Log::stack(['cola_envio'])->info(  );
		} catch (\Exception $e ) {
			write_log( $e->getMessage(). " en la funcion reProcesarCpe() idComprobante : ". $idComprobante  );
		}
			
	}
	
}