<?php 

// include_once  "../lib/APIv3-php-library/sendinblue.php";//incluimos el servicio
// include_once  "../helpers/common_helper.php";//incluimos el servicio
namespace App\Providers;
use SendGrid;
/**
 * 
 */
class ProviderServiceEmailSendgrid 
{
	private $_config_service ;
	private $_config ;
	private $_sendSmtpEmail ;
	private $_toEmail ;
	private $_sender ;
	private $_subject ;
	private $_bodyHtml ;
	private $_files ;// [ 'file_name' , 'path_file', 'formato', 'base64' ]
	private $_header ;
	private $_typeEmail ;
	private $_listEmail ;
	private $_listFile ;
	private $_listFileSend ;
	private $_apiInstance ;
	private $_emailValidate;
	private $_emailError;
	/**
	* 
	* $toEmail
	* $subject Asunto
	*/
	function __construct( $toEmail, $sender, $subject, $files = [] , $bodyHtml = null, $header = null, $typeEmail = null)
	{
		$this->_files = $files;
		$this->_toEmail = $toEmail;
		$this->_sender = $sender;
		$this->_subject = $subject;
		$this->_typeEmail = $typeEmail;
		$this->_header = $header;
		$this->_bodyHtml = $bodyHtml;
		$this->_listEmail = [];
		$this->_listFile = [];
		$this->_emailValidate = '';
		$this->_emailError = '';
		$this->conex();
		$this->prepareToEmail();
		$this->prepareFile();
		$this->prepareBody();
	}
	/**
	* realiza la conexcion a la api del servicio
	*/
	private function conex()
	{
		$this->_apiInstance = new SendGrid( env('TOKEN_EMAIL_SENDGRID') );
		$this->_sendSmtpEmail = new \SendGrid\Mail\Mail();
	}
	/**
	* setea los campos necesario de la direcciones de correos
	*/
	private function prepareToEmail()
	{
		$emails = str_replace(' ', '', $this->_toEmail) ;
		$arrayEmail =  array_unique(  array_filter( explode(",", $this->_toEmail)  ) );
        $emailError = "";
        foreach ($arrayEmail as $value) 
        {
            $emailTo = trim( $value );
            //valida si es un email correcto
            if (filter_var($emailTo, FILTER_VALIDATE_EMAIL) ) 
            {
                $elementEmail = ['email'=> $emailTo , 'name'=>'Cliente'];
                array_push($this->_listEmail, $elementEmail );
                $this->_emailValidate .=" ," .$emailTo;

            }else
            {
            	$this->_emailError .= ", " . $emailTo;
            }
        }

	}
	/**
	* setea todas las configuraciones necesarias para enviar
	*/
	private function prepareConfigParamEmail()
	{
		if ( count( $this->_listEmail ) > 0) 
		{
			$this->_sendSmtpEmail->setFrom($this->_sender['email']);
			$this->_sendSmtpEmail->setSubject($this->_subject);
			$this->_sendSmtpEmail->addContent("text/html", $this->_bodyHtml );
			// console.log($this->_listEmail[0]);	
			foreach ($this->_listEmail as $key => $value) 
			{
				$this->_sendSmtpEmail->addTo($value['email'],"");
			}
			if ( count( $this->_listFile ) > 0) 
			{
				foreach ($this->_listFile as $key => $value) {
					$this->_sendSmtpEmail->addAttachment(
                    $value['content'],
                    "application/xls",
                    $value['name'],
                     // $fileName .'.xml' ,
                    "attachment"
                	);
				}
			}
			return true;
		}
		return ['status' => false,'message' => 'Emails invalidos' , 'cod_error' => 404, 'error' => 'Emails invalidos: ' .$this->_emailError];
		
	}
	/**
	* setea la plantilla html del correo
	*/
	private function prepareBody()
	{
		$this->_bodyHtml = $this->_bodyHtml == null ?  "<h2>Puede descargar todos su Lista de comprobantes emitidos</h2>" : $this->_bodyHtml;
	}
	/**
	* setea los archivos a enivar
	*/
	private function prepareFile()
	{
		foreach ( $this->_files as $key => $value ) 
		{
			if (  !isset( $value[ 'base64' ] ) )
			{
				if ( file_exists( $value['path_file'] ) && filesize( $value['path_file'] ) <= 15000000 ) 
				{
					$value['base64'] = base64_encode( file_get_contents(  $value['path_file']  ) );
					$fileElement = [ 'content' => $value['base64'] , 'name' => $value['file_name'] ];
					array_push($this->_listFile, $fileElement);
				}
			}else{
				$fileElement = [ 'content' => $value['base64'] , 'name' => $value['file_name'] ];
				array_push($this->_listFile, $fileElement);
			}
		}
		$this->_listFileSend = $this->_listFile;
	}
	public function sendEmail()
	{
		$message = [];
		$status = $this->prepareConfigParamEmail();
		// var_dump($this->_sendSmtpEmail);
		// exit;
		if ( $status === true )
	    {
	    	$message[ 'service' ] = "SendGrid";
	    	$message[ 'start' ] = "Iniciando envio email: ". date('Y-m-d H:m:s.').round(microtime(true) * 1000);
			$response = $this->_apiInstance->send( $this->_sendSmtpEmail );
	    	$message[ 'end' ] = "Finalizo envio email: ". date('Y-m-d H:m:s.').round(microtime(true) * 1000);
			$statusCodeEmail = $response->statusCode();
			if ( $statusCodeEmail == "202" ) 
		    {
		    	$message['message'] = 'se envio correctamente';
		    	$message['info'] = 'se envio correctamente';
		    	$message['status'] = true;
		    	$message['to_emails'] = $this->_emailValidate;
		    	$message['to_emails_invalid'] = $this->_emailError;
		    	$message['files'] = implode(', ',  array_map(function( $element ){ return $element['name']; }, $this->_listFileSend )  )  ;
		    	$message['code'] = $statusCodeEmail;
		    }else
		    {

		    	$message['status'] = false;
		    	$message['info'] = 'Ocurrio un error';
		    	$message['cod_error'] = $statusCodeEmail;
		    	$message['code'] = $statusCodeEmail;
		    }
		    return $message;
		}
		return $status;
	}
	public function sendEmailByPlantilla( $record )
	{

	}


}

 ?>