<?php
namespace App\Providers;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Client\RequestException;
class FactoringInnovaServiceProvider extends ServiceProvider
{
    private $_access_token ;
    private $_header ;
    private $body;
    /**
     * Consctructo
     * */
    public function __construct(){
        $this->generateToken();
        $this->_header = [
            'Ocp-Apim-Subscription-Key' => env('AUTH_INNOVA_API_INSCRIPTION_ID'),
            "Content-type" => 'application/json'
        ];
    }

    public function generateToken()
    {
        try {
        $url = "https://login.microsoftonline.com/".env('AUTH_INNOVA_FNNG_TENANT')."/oauth2/v2.0/token";
        $response = Http::asForm()
        ->post($url, [
            'grant_type'=>'client_credentials',
            'client_secret'=>env('AUTH_INNOVA_FNNG_CLIENT_SECRET'),
            'client_id'=>env('AUTH_INNOVA_FNNG_CLIENT_ID'),
            'scope'=>env('AUTH_INNOVA_FNNG_SCOPE')
        ]);
        if ( $response->failed() ) 
        {
            throw new \Exception($response->throw(), 1);
        }
        $this->_access_token = $response->json()['access_token'];
        return $response->json();
        } catch (\Exception $e) {
            throw new \Exception( $e->getMessage() );
            \Sentry::captureException($e);
        }

    }

    public function sendInvoce(){

        try {
            // dd($this->getBody());
            
            $response = Http::withToken($this->_access_token)
            ->withHeaders( $this->_header )
            ->retry(3,100)
            ->post( env('URL_API_INNOVA_FNNG') . 'invoice', $this->getBody());
            $statusCode = $response->status();
            if ( $response->clientError() ) 
            {
                \Sentry::captureMessage('ERROR AL ENVIAR INVOINCE: message_error: '. $response->body(). " body: ". json_encode($this->getBody()));
                return [ 'status' => false, 'response' => $response->body() , 'statusCode' => $statusCode];
            }
            if ( $response->serverError() ) 
            {
                throw new \Exception( $response->throw() );
            }
            return [ 'status' => true, 'response' => $response->body() , 'statusCode' => $statusCode , 'data' => $response->json()];
           
        } catch (RequestException $e) {
            \Sentry::captureException($e);
            \Sentry::captureMessage('ERROR AL ENVIAR INVOINCE: message_error: '. $e->getMessage(). " body: ". json_encode($this->getBody()));
            throw new \Exception($e->getMessage(), 1);
        }
        // $response = Http::post($this->url);
        
    }
    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param mixed $body
     *
     * @return self
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }
}
