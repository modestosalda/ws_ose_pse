<?php 
namespace App\Providers\Client;
use Exception;
use Sentry;
/**
 * 
 */
class GetStatusAnulacion 
{
	
	private $_user;
	private $_pass;
	private $_base64zip;
	private $_fileName;
	private $_serie;
	private $_ruc;
	private $_correlativo;
	private $_cpeType;
	private $_modelComprobanteCpe;
	private $_comprobanteId;
	private $_fechaEmision;
	private $_response;
	private $_statusCode;
	private $_cdrBase64;
	private $_reprocess = null ;
	public function __construct( $fileName )
	{
		$this->setFileName( $fileName );
		$porciones = explode('-',  explode('.', $fileName)[0] );
		$this->_ruc = $porciones[ 0 ];
		$this->_cpeType = $porciones[ 1 ];
	}
	
	public function setFileName( $fileName )	{		$this->_fileName = trim($fileName);	}
	public function getFileName(  )	{	return	$this->_fileName ;	}

	public function setStatusCode( $statusCode ){ $this->_statusCode  = $statusCode ;}
	public function getStatusCode(){ return $this->_statusCode ;}

	public function setReprocess( $resprocess ){ $this->_reprocess = $resprocess; }
	public function getReprocess(){ return $this->_reprocess; }

	private function setCDRBase64( $cdrBase64 )
	{
		$this->_cdrBase64 = $cdrBase64;
	}
	public function getCDRBase64(){
		return $this->_cdrBase64;
	}
	/**
	* 
	* @return Array [ data[ responseCode, description, sinProcesar, responseService ], path_file_cdr, path_file_xml ]
	*/
	public function getDataXmlResponse( $xmlResponse,  $base64zip = '', $fechaEmision )
	{
		$result = [];
		$doc = new \DOMDocument();
        $doc->loadXML( $xmlResponse );
        //===================VERIFICAMOS SI EXISTE ETIQUETA DE RESPUESTA=====================
        if ( isset( $doc->getElementsByTagName('content')->item(0)->nodeValue)   ) 
        {
        	$xmlCDR = trim($doc->getElementsByTagName('content')->item(0)->nodeValue);
        	$fileNameCDR = 'R-' . $this->getFileName();
            if(   $xmlCDR == '' )
            {
                $this->setReprocess( verify_cod_service_reprocess( $doc->getElementsByTagName('statusCode')->item(0)->nodeValue   ) );
                return false;
            }
            try {
            	// obtiene la ruta donde se alojara el cdr
        		$pathCDR =  get_path_comprobante(  '' ,  'cdr' ,  $fechaEmision, $this->_ruc, $this->_cpeType ) ;
        		$pathXMLENVIO = get_path_comprobante(  str_replace('.zip', '', $this->getFileName() )  ,  'xml' ,  $fechaEmision, $this->_ruc, $this->_cpeType );
        		//crea el directorio si no existe
            	if ( env('APP_ENV') === 'local' )
				{
					$destineXMLENVIO = get_path_comprobante(  ''  ,  'xml' ,  $fechaEmision, $this->_ruc, $this->_cpeType ) ;
				
					create_new_directory( $destineXMLENVIO );
					$pathXMLENVIOZIP = get_path_comprobante(  str_replace('.zip', '', $this->getFileName() )  ,  'zip' ,  $fechaEmision, $this->_ruc, $this->_cpeType ) ;
					
					file_put_contents( $pathXMLENVIOZIP , base64_decode( $base64zip ) );
					if ( !file_exists( $pathXMLENVIOZIP ) ) 
					{
						throw new \Exception("El archivo no existe :" . $pathXMLENVIOZIP, 1);
						
					}
					extract_to_zip( $pathXMLENVIOZIP, $destineXMLENVIO );	
				}
				create_new_directory( $pathCDR );
        		//concatenamos la ruta con el nombre del cdr
        		$pathFileCDR = $pathCDR . $fileNameCDR;
        		$this->setCDRBase64( $xmlCDR );
        		//guarda el cdr
        		file_put_contents( $pathFileCDR , base64_decode( $xmlCDR ) );
        		
        		//extrae el xml del zip
				extract_to_zip( $pathFileCDR, $pathCDR );
				// echo $pathFileCDR;
    //     		exit;
				// $pahCDR_XML = $pathCDR . '\\' . str_replace('zip', 'xml', $fileNameCDR ) ; //en windows
				$pahCDR_XML = $pathCDR  . str_replace('zip', 'xml', $fileNameCDR ) ;//en linux
				
				$base64XMLCDR = base64_encode( file_get_contents(   $pahCDR_XML ) );
				$dataXml  = $this->getDataXml( $pahCDR_XML );
				return [ 'data' => $dataXml ,'path_file_cdr' => $pathFileCDR, 'path_file_xml' => $pathXMLENVIO ];
            } catch (\Exception $e) {
            	Sentry::captureException( $e );
        		throw new \Exception($e->getMessage(), 1);
            }
        }else{

        	$result[ 'informacion' ] = 'RECHAZO DEL SERVICIO, NO EXISTE ETIQUETA DE RESPUESTA';
        	/**OBTENEMOS LA INFORMACION DEL RECHAZO*/
        	$dataXml =  $this->getDataRechazoXML( $doc );
        	// $result[ 'data_response' ] = $dataXml;
			return ['data' => $dataXml ,'path_file_cdr' => $pathFileCDR, 'path_file_xml' => $pathXMLENVIO  ];
        }
	}
	/**
	* Obtenemos informacion del rechazo
	*/
	private function getDataRechazoXML( $doc )
	{
		$responseCode = '';
		$description = '';
		$sinProcesar = false;
		if ( isset( $doc->getElementsByTagName( 'detail' )->item(0)->nodeValue ) )
		{
			$responseCode = $doc->getElementsByTagName('faultstring')->item(0)->nodeValue;
			foreach ($doc->getElementsByTagName('detail') as $nodo_detail) 
			{	
				$description = $nodo_detail->getElementsByTagName('message')->item(0)->nodeValue;
			}
		}else 
		{
			$responseCode = preg_replace('/[^0-9]+/', '', $doc->getElementsByTagName('faultcode')->item(0)->nodeValue) ;
			$description = $doc->getElementsByTagName('faultstring')->item(0)->nodeValue;
		}
		$this->setReprocess( verify_cod_service_reprocess( $responseCode )  );
		return [ 'responseCode' => $responseCode, 'description'=> $description , 'sinProcesar' => $sinProcesar, 'responseService'=> 'R'];

	}
	/**
	* obtiene la data del xml parseada a array
	*/
	private function getDataXml( $base64XML )
	{
		$doc_cdr = new \DOMDocument();
	    $doc_cdr->load( $base64XML );
	    $responseCode = $doc_cdr->getElementsByTagName( 'ResponseCode' )->item(0)->nodeValue;
	    $description = str_replace("'"," ",$doc_cdr->getElementsByTagName('Description')->item(0)->nodeValue);
	    $digestValue = $doc_cdr->getElementsByTagName('DigestValue')->item(0)->nodeValue;
	    $status = NULL;
	    $sinprocesar = false;
		$obsService = '';
		$desexcService = '';
		$valor_notas = [];
		$nota = $doc_cdr->getElementsByTagName('Note');
		foreach ($nota as $notas) 
		{
			$valor_notas[] = $notas->nodeValue ;
		}
		$obsService = str_replace( "'"," ", implode( ";",$valor_notas ) );
		if ( isset( $doc_cdr->getElementsByTagName('Status')->item(0)->nodeValue ) ) 
		{
			$status = $doc_cdr->getElementsByTagName('Status')->item(0)->nodeValue;
			$obs = $doc_cdr->getElementsByTagName('Status');
			$obsArray = [];
			foreach ($obs as $key => $value) 
			{
				array_push($obsArray, $doc_cdr->getElementsByTagName('Status')->item( $key )->getElementsByTagName('StatusReasonCode')->item(0)->nodeValue);
				array_push($obsArray, $doc_cdr->getElementsByTagName('Status')->item( $key )->getElementsByTagName('StatusReason')->item(0)->nodeValue);
			}
			$obsService = str_replace("'"," ",implode("|",$obsArray));
		}
		if($responseCode == 0 && $status == NULL  )//ACEPTADO
		{ 
			$responseService = "A";
		}elseif ( $responseCode == 0 && $status != NULL ) //OBSERVADO
		{
			$responseService = "O";
		}elseif( $responseCode != 0 )//RECHAZADO
		{
			$responseService = "R";
			if( verify_cod_service_reprocess( $responseCode ) )
			{
				$sinprocesar = true;
			}else{
				$desexcService = $description;
			}
		}
		return [ 'description' => $description, 'responseCode' => $responseCode,  'responseService' => $responseService, 'obsService' => $obsService , 'desexcService' => $desexcService , 'sinProcesar' => $sinprocesar];

	}
}