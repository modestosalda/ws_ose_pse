<?php 
namespace App\Providers\Client;
use Exception;
use Sentry;
/**
 * 
 */
class SendSummary 
{
	
	private $_user;
	private $_pass;
	private $_base64zip;
	private $_fileName;
	private $_serie;
	private $_ruc;
	private $_correlativo;
	private $_cpeType;
	private $_modelComprobanteCpe;
	private $_comprobanteId;
	private $_fechaEmision;
	private $_response;
	private $_statusCode;
	private $_cdrBase64;
	private $_reprocess = null ;
	public function __construct( $fileName )
	{
		$this->setFileName( $fileName );
		$porciones = explode('-',  explode('.', $fileName)[0] );
		$this->_ruc = $porciones[ 0 ];
		$this->_cpeType = $porciones[ 1 ];
	}
	
	public function setFileName( $fileName )	{		$this->_fileName = trim($fileName);	}
	public function getFileName(  )	{	return	$this->_fileName ;	}

	public function setStatusCode( $statusCode ){ $this->_statusCode  = $statusCode ;}
	public function getStatusCode(){ return $this->_statusCode ;}

	public function setReprocess( $resprocess ){ $this->_reprocess = $resprocess; }
	public function getReprocess(){ return $this->_reprocess; }

	private function setCDRBase64( $cdrBase64 )
	{
		$this->_cdrBase64 = $cdrBase64;
	}
	public function getCDRBase64(){
		return $this->_cdrBase64;
	}
	/**
	* 
	* @return Array [ data[ responseCode, description, sinProcesar, responseService ], path_file_cdr, path_file_xml ]
	*/
	public function getDataXmlResponse( $xmlResponse )
	{
		$result = [];
		$doc = new \DOMDocument();
        $doc->loadXML( $xmlResponse );
        //===================VERIFICAMOS SI EXISTE ETIQUETA DE RESPUESTA=====================
        if ( isset( $doc->getElementsByTagName( 'ticket' )->item( 0 )->nodeValue )  ) 
        {
        	$nroTicket  = $doc->getElementsByTagName('ticket')->item(0)->nodeValue;
        	return [ 'data' => [ 'nro_ticket' => $nroTicket ]  , 'sinProcesar' => null  ];
        }else{

        	$responseRechazo = getResponseRechazado( $doc );
        	// var_dump($responseRechazo);
        	// exit;
        	$this->setReprocess( verify_cod_service_reprocess( $responseRechazo[ 'responseCode' ] ) );
        	return [ 'data' => $responseRechazo  ];
        }
	}
}