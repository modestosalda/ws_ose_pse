<?php 
namespace App\Providers\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\RequestException;
// use Exception;
use Sentry;
/**
 * 
 */
class ClientService
{
	public $_body ;
	public $_header ;
	public $_url ;
	public $_initCurl ;
	public $_endCurl ;
	public $_response ;
	public $_statusCode ;
	public $_serviceType ;
	public $_message;
	public $_reprocese = null;
	public $_nroTicket ;

	function __construct(  $serviceType, $url )
	{
		$this->_url = trim( $url );
		$this->_serviceType = $serviceType;
	}

	public function setReprocese( $reprocese )	{ $this->_reprocese = $reprocese;	}
	public function getReprocese(){ return $this->_reprocese ;}

	public function setResponse( $response )	{ $this->_response = $response;	}
	public function getResponse(){ return $this->_response ;}

	public function setStatusCode( $statusCode )	{ $this->_statusCode = $statusCode;	}
	public function getStatusCode(){ return $this->_statusCode ;}

	public function setMessage( $message )	{ $this->_message = $message; }		// write_log( $messageError . 'Object : ' get_class(), 'error' );
	public function getMessage(){ return $this->_message; }

	public function setInitCurl( $date )	{		$this->_initCurl = $date;	}
	public function getInitCurl(  )	{	return	$this->_initCurl ;	}

	public function setEndCurl( $date )	{		$this->_endCurl = $date;	}
	public function getEndCurl(  )	{	return	$this->_endCurl ;	}

	public function setUser( $user )	{		$this->_user = trim($user);	}
	public function getUser(  )	{	return	$this->_user ;	}

	public function setPassword( $pass )	{ $this->_pass = trim($pass);	}
	public function getPassword(  )	{	return	$this->_pass ;	}

	public function setBase64( $base64 )	{		$this->_base64zip = trim($base64);	}
	public function getBase64(  )	{	return	$this->_base64zip ;	}

	public function setFileName( $fileName )	{		$this->_fileName = trim($fileName);	}
	public function getFileName(  )	{	return	$this->_fileName ;	}

	public function setNroTicket( $nroTicket ) { $this->_nroTicket = trim($nroTicket);  }
	public function getNroTicket( ) {  return $this->_nroTicket; }

	public function setBodyAndHeaderSendBill()
	{
		$this->_body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
										  xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.sunat.gob.pe"
										  xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
								  <soapenv:Header>
									  <wsse:Security>
										  <wsse:UsernameToken>
											  <wsse:Username>' . $this->getUser() . '</wsse:Username>
											  <wsse:Password>' .  $this->getPassword() . '</wsse:Password>
										  </wsse:UsernameToken>
									  </wsse:Security>
								  </soapenv:Header>
								  <soapenv:Body>
									  <ser:sendBill>
										  <fileName>' .  $this->getFileName() . '</fileName>
										  <contentFile>' . $this->getBase64() . '</contentFile>
									  </ser:sendBill>
								  </soapenv:Body>
								  </soapenv:Envelope>';
		$this->_header = [
							        "Content-type: text/xml;charset=\"utf-8\"",
							        // "Accept: text/xml",
							        // "Cache-Control: no-cache",
							        // "Pragma: no-cache",
							        "SOAPAction:  urn:sendBill",
						        	"Content-length: " . strlen($this->_body),
						];
	}
	public function setBodyAndHeaderSendSummary()
	{
		$this->_body = 
					'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
							  xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.sunat.gob.pe"
							  xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
   						<soapenv:Header>
							  <wsse:Security>
							  <wsse:UsernameToken>
							  <wsse:Username>' . $this->getUser() . '</wsse:Username>
							  <wsse:Password>' .  $this->getPassword() . '</wsse:Password>
							  </wsse:UsernameToken>
							  </wsse:Security>
						  	</soapenv:Header>
							  <soapenv:Body>
							  <ser:sendSummary>
							  <fileName>' . $this->getFileName() . '</fileName>
							  <contentFile>' . $this->getBase64() . '</contentFile>
							  </ser:sendSummary>
						  </soapenv:Body>
				  </soapenv:Envelope>';
			// var_dump($this->_body);
			// exit;
		$this->_header = [
							        "Content-type: text/xml;charset=\"utf-8\"",
							        // "Accept: text/xml",
							        // "Cache-Control: no-cache",
							        // "Pragma: no-cache",
							        "SOAPAction:  urn:sendSummary",
						        	"Content-length: " . strlen($this->_body),
						];
		// $this->_header = [
		// 					"Content-Type" => "text/xml;charset=UTF-8",
		// 			        "Accept" => "text/xml",
		// 			        "Cache-Control" => "no-cache",
		// 			        "Pragma" => "no-cache",
		// 			        "SOAPAction" => 'urn:sendSummary',
		// 	        		"Content-Length" => strlen($this->_body)
		// 				];
	}
	public function setBodyAndHeaderGetStatus()
	{
		$this->_body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.sunat.gob.pe" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
		                    <soapenv:Header>
		                        <wsse:Security>
		                            <wsse:UsernameToken>
		                                <wsse:Username>' . $this->getUser() . '</wsse:Username>
		                                <wsse:Password>' . $this->getPassword() . '</wsse:Password>
		                            </wsse:UsernameToken>
		                        </wsse:Security>
		                    </soapenv:Header>
		                    <soapenv:Body>
		                      <ser:getStatus>
		                        <ticket>' . $this->getNroTicket() . '</ticket>
		                      </ser:getStatus>
		                    </soapenv:Body>
		                </soapenv:Envelope>';
		$this->_header = [
					        "Content-type: text/xml;charset=\"utf-8\"",
					        "Accept: text/xml",
					        "Cache-Control: no-cache",
					        "Pragma: no-cache",
					        "SOAPAction:  urn:getStatus",
					        "Content-length: " . strlen($this->_body),
						];
	}
	public function prepareService( $service )
	{
		
		switch ( $service ) 
		{
			case 'sendBill':
					$this->setBodyAndHeaderSendBill();
			break;
			case 'sendSummary':
					$this->setBodyAndHeaderSendSummary();
			break;
			case 'getStatus':
					$this->setBodyAndHeaderGetStatus();
			break;
		}
	}

	public function valideUrl()
	{
		if ( strtolower( env('APP_ENV') ) == 'local' ) 
		{
			return true;
			$urlParts = parse_url( $this->_url );
			$scheme = $urlParts[ 'scheme' ] ?? '';
			if ( $scheme !== 'https' )
			{
				$this->setReprocese( null );

				throw new \Exception( 'La url es invalida File:' . $this->getFileName() . ' URL: '.$this->_url , 1);
			}
		}else{
			return true;
		}
		
	}
	public function consume()
	{
		$inicio = "Iniciando envio curl: ". date('Y-m-d H:m:s.').round(microtime(true) * 1000);
		try {
			$message = [];
			$this->setReprocese( null );
			$this->valideUrl();
			// $response = Http::post($this->url);
			// $response = Http::withHeaders( $this->_header )
			// // ->contentType( "text/xml;charset=UTF-8" )
			// // ->retry(3,100)
			// ->withBody($this->_body, '')
			// // ->withHeaders( $this->_header )
			// ->post($this->_url)
			// ;
			
			// $this->setResponse( $response->body() );
			// $this->setStatusCode( $response->status() );
			// $end = "fin envio curl: ". date('Y-m-d H:m:s.').round(microtime(true) * 1000);
			// $message = [
			// 		'START' => $inicio,
			// 		'END' => $end,
		 //    		'URL' => $this->_url,
		 //    		'STATUS_CODE' => $this->getStatusCode(),
		 //    		'BODY' => $this->_body,
		 //    		'HEADER' => $this->_header,
		 //    		'RESPONSE' => $this->getResponse(),
		 //    ];
		 //    \Log::stack(['http_request_sunat'])->info( $message );
			$this->setInitCurl( $inicio );
			$ch = curl_init();
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($ch, CURLOPT_URL, trim( $this->_url ));
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		    curl_setopt($ch, CURLOPT_POST, true);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $this->_body ); // the SOAP request
		    if ( count( $this->_header ) ) 
		    {
		    	curl_setopt($ch, CURLOPT_HTTPHEADER, $this->_header );	
		    }
		    $this->setResponse( curl_exec($ch) ) ;
		    $this->setStatusCode( curl_getinfo($ch, CURLINFO_HTTP_CODE) ) ;
			$end = "fin envio curl: ". date('Y-m-d H:m:s.').round(microtime(true) * 1000);
			$this->setEndCurl( $end );
			$message = [
					'START' => $inicio,
					'END' => $end,
		    		'URL' => $this->_url,
		    		'STATUS_CODE' => $this->getStatusCode(),
		    		'BODY' => $this->_body,
		    		'HEADER' => $this->_header,
		    		'RESPONSE' => $this->getResponse(),
		    ];
		    if ( !$this->getResponse()  ) 
		    {
		    	$message['MESSAGE'] = curl_error($ch);
		    	$message = json_encode( $message );
		    	$this->setMessage( curl_error($ch) );
		    	\Log::stack(['http_request_sunat'])->error( $message );
		    	$this->setReprocese( true );
		    	throw new \Exception( curl_error($ch) , 1);
		    }
		    
		    curl_close($ch);
		    $this->valideStatus();
		   
		    $this->setMessage( json_encode( $message ) );
	    	\Log::stack(['http_request_sunat'])->info( $message );
		    return true;
		} catch (\Exception $e) {

			$message = [
					'START' => $inicio,
		    		'URL' => $this->_url,
		    		'BODY' => $this->_body,
		    		'HEADER' => $this->_header
		    ];
			Sentry::captureMessage( $e->getMessage() . ' ENVIADO : ' . json_encode( $message , JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES ) );
			throw new \Exception( $e->getMessage(), 1);
		}
		
	}
	/***
	* valida el response dado el status Code de la respuesta del servicios
	*/
	public function valideStatus()
	{
		if ( $this->getStatusCode() != '200') 
		{
			$this->setReprocese( true );
			throw new \Exception( 'Error del servicio  RESPONSE: ' . $this->getResponse() . '  statusCode: ' . $this->getStatusCode(), 1);
		}
		
	}
}
 