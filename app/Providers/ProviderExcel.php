<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ProviderExcel extends ServiceProvider
{

    private $_objPHPExcel ;
    private $_title ;
    private $_headerList ;
    private $_rowIni ;
    private $_columnIni ;
    private $_columnEnd ;
    private $_data ;
    private $_columnsSize;
    private $_pathFile;
    public function __construc( $title , $headerList, $rowIni, $columnIni, $data, $pathFile, $columnsSize = [] )
    {
        $this->_title = $title;
        $this->_headerList = $headerList;
        $this->_rowIni = $rowIni;
        $this->_columnIni = $columnIni;
        $this->_columnEnd = '';
        $this->_data = $data;
        $this->_columnsSize = $columnsSize;
        $this->_pathFile = $pathFile;
        $this->init();
    }
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
