<?php 
namespace App\Providers;

use App\Providers\Client\ClientService;
use App\Providers\Client\SendSummary;
use Sentry;
use Exception;
/**
 * 
 */
// class ProviderServiceSunatOse extends ClientService
class ProviderServiceSunatOse extends ClientService
{
	function __construct( $type, $url )
	{

		 parent::__construct( $type, $url );
	}

	public function requestService( )
	{
		try {
			
			$this->prepareService($this->_serviceType  );
			/**peticion curl*/
			$this->consume();
		} catch (\Exception $e) 
		{
			throw new \Exception($e->getMessage(), 1);
		}
	}
	
}