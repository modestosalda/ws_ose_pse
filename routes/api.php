<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\ComprobanteAPI;
use App\Http\Controllers\API\EmisorAPI;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('emisor/usuario',[ EmisorAPI::class, 'crearEmisorUsuario' ])
->name('emisor/usuario')->middleware('client');

Route::post('cola_envio', [ ComprobanteAPI::class, 'sendCpeEmitidos' ])
->name('cola_envio');


Route::post('cpe/sent/tows', [ ComprobanteAPI::class, 'sendCpeToWs' ])
->name('cpe/sent/tows');

// Route::post('cola_envio', 'App\Http\Controllers\API\ComprobanteAPI@sendCpeEmitidos' )
// ->name('cola_envio');

Route::get('getListCpeEmitidos', 'App\Http\Controllers\API\ComprobanteAPI@index')
->name('getListCpeEmitidos')->middleware('client');