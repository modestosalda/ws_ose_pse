<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnUpdatedAtAndCreated extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //public
        if (Schema::hasTable('job_info'))
        {
            Schema::table( 'job_info', function( $table ) { $table->timestamps();});
            Schema::table( 'job_client', function( $table ) { $table->timestamps();});
            // Schema::table( 'job_info', function( $table ) { $table->bigInteger('tb_comprobante_id')->nullable(); });
            // Schema::table( 'job_info', function( $table ) { $table->bigInteger('tb_comprobante_all_id')->nullable(); });

            //sh_cloud_pse
            Schema::table( 'sh_cloud_pse.tb_comprobante', function( $table ) { $table->timestamps();});
            Schema::table( 'sh_cloud_pse.tb_comprobante_all', function( $table ) { $table->timestamps(); });
            Schema::table( 'sh_cloud_pse.tb_emisor', function( $table ) { $table->timestamps();});
            Schema::table( 'sh_cloud_pse.tb_emisor_impresion_pdf', function( $table ) {$table->timestamps();});
            Schema::table( 'sh_cloud_pse.fe_recibidos', function( $table ) {$table->timestamps(); });
            Schema::table( 'sh_cloud_pse.tb_tipo_correo', function( $table ){ $table->timestamps(); });
            Schema::table( 'sh_cloud_pse.tb_correo_plantilla', function( $table ) { $table->timestamps(); });
            //sh_cloud_cpe
            Schema::table( 'sh_cloud_cpe.tb_boleta_cab', function( $table ){ $table->timestamps();});
            Schema::table( 'sh_cloud_cpe.tb_boleta_det', function( $table ){ $table->timestamps(); });
            Schema::table( 'sh_cloud_cpe.tb_boleta_notas', function( $table ){ $table->timestamps(); });
            Schema::table( 'sh_cloud_cpe.tb_boleta_tri', function( $table ){$table->timestamps();});

            Schema::table( 'sh_cloud_cpe.tb_factura_cab', function( $table ){ $table->timestamps();});
            Schema::table( 'sh_cloud_cpe.tb_factura_det', function( $table ){ $table->timestamps(); });
            Schema::table( 'sh_cloud_cpe.tb_factura_notas', function( $table ){ $table->timestamps(); });
            Schema::table( 'sh_cloud_cpe.tb_factura_tri', function( $table ){$table->timestamps();});

            Schema::table( 'sh_cloud_cpe.tb_nc_cab', function( $table ){ $table->timestamps();});
            Schema::table( 'sh_cloud_cpe.tb_nc_det', function( $table ){ $table->timestamps(); });
            Schema::table( 'sh_cloud_cpe.tb_nc_notas', function( $table ){ $table->timestamps(); });
            Schema::table( 'sh_cloud_cpe.tb_nc_tri', function( $table ){$table->timestamps();});

            Schema::table( 'sh_cloud_cpe.tb_nd_cab', function( $table ){ $table->timestamps();});
            Schema::table( 'sh_cloud_cpe.tb_nd_det', function( $table ){ $table->timestamps(); });
            Schema::table( 'sh_cloud_cpe.tb_nd_notas', function( $table ){ $table->timestamps(); });
            Schema::table( 'sh_cloud_cpe.tb_nd_tri', function( $table ){$table->timestamps();});

            Schema::table( 'sh_cloud_cpe.tb_comunicacionbaja', function( $table ){ $table->timestamps();});
            Schema::table( 'sh_cloud_cpe.tb_comunicacionbaja_detalle', function( $table ){ $table->timestamps(); });

            Schema::table( 'sh_cloud_cpe.tb_percepcion_cab', function( $table ){ $table->timestamps(); });
            Schema::table( 'sh_cloud_cpe.tb_percepcion_det', function( $table ){$table->timestamps();});
            Schema::table( 'sh_cloud_cpe.tb_percepcion_notas', function( $table ){$table->timestamps();});

            Schema::table( 'sh_cloud_cpe.tb_retencion_cab', function( $table ){ $table->timestamps(); });
            Schema::table( 'sh_cloud_cpe.tb_retencion_det', function( $table ){$table->timestamps();});
            Schema::table( 'sh_cloud_cpe.tb_retencion_notas', function( $table ){$table->timestamps();});

            Schema::table( 'sh_cloud_cpe.tb_resumendiario', function( $table ){ $table->timestamps(); });
            Schema::table( 'sh_cloud_cpe.tb_resumendiario_detalle', function( $table ){$table->timestamps();});

            Schema::table( 'sh_cloud_cpe.tb_resumenreversiones', function( $table ){ $table->timestamps(); });
            Schema::table( 'sh_cloud_cpe.tb_resumenreversiones_det', function( $table ){$table->timestamps();});

            Schema::table( 'job_info', function( $table ){$table->id();});
            Schema::table( 'job_info', function( $table ){$table->text('message')->nullable();});
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //dropTimestamps
        //public
        Schema::table( 'job_client', function( $table ) { $table->dropTimestamps();});

        Schema::table( 'sh_cloud_pse.tb_comprobante', function( $table ) { $table->dropTimestamps();});
        Schema::table( 'sh_cloud_pse.tb_comprobante_all', function( $table ) { $table->dropTimestamps(); });
        Schema::table( 'sh_cloud_pse.tb_emisor', function( $table ) { $table->dropTimestamps();});
        Schema::table( 'sh_cloud_pse.tb_emisor_impresion_pdf', function( $table ) {$table->dropTimestamps();});
        Schema::table( 'sh_cloud_pse.fe_recibidos', function( $table ) {$table->dropTimestamps(); });
        Schema::table( 'sh_cloud_pse.tb_tipo_correo', function( $table ){ $table->dropTimestamps(); });
        Schema::table( 'sh_cloud_pse.tb_correo_plantilla', function( $table ) { $table->dropTimestamps(); });
        Schema::table( 'sh_cloud_cpe.tb_boleta_cab', function( $table ){ $table->dropTimestamps();});
        Schema::table( 'sh_cloud_cpe.tb_boleta_det', function( $table ){ $table->dropTimestamps(); });
        Schema::table( 'sh_cloud_cpe.tb_boleta_notas', function( $table ){ $table->dropTimestamps(); });
        Schema::table( 'sh_cloud_cpe.tb_boleta_tri', function( $table ){$table->dropTimestamps();});

        Schema::table( 'sh_cloud_cpe.tb_factura_cab', function( $table ){ $table->dropTimestamps();});
        Schema::table( 'sh_cloud_cpe.tb_factura_det', function( $table ){ $table->dropTimestamps(); });
        Schema::table( 'sh_cloud_cpe.tb_factura_notas', function( $table ){ $table->dropTimestamps(); });
        Schema::table( 'sh_cloud_cpe.tb_factura_tri', function( $table ){$table->dropTimestamps();});

        Schema::table( 'sh_cloud_cpe.tb_nc_cab', function( $table ){ $table->dropTimestamps();});
        Schema::table( 'sh_cloud_cpe.tb_nc_det', function( $table ){ $table->dropTimestamps(); });
        Schema::table( 'sh_cloud_cpe.tb_nc_notas', function( $table ){ $table->dropTimestamps(); });
        Schema::table( 'sh_cloud_cpe.tb_nc_tri', function( $table ){$table->dropTimestamps();});

        Schema::table( 'sh_cloud_cpe.tb_nd_cab', function( $table ){ $table->dropTimestamps();});
        Schema::table( 'sh_cloud_cpe.tb_nd_det', function( $table ){ $table->dropTimestamps(); });
        Schema::table( 'sh_cloud_cpe.tb_nd_notas', function( $table ){ $table->dropTimestamps(); });
        Schema::table( 'sh_cloud_cpe.tb_nd_tri', function( $table ){$table->dropTimestamps();});

        Schema::table( 'sh_cloud_cpe.tb_comunicacionbaja', function( $table ){ $table->dropTimestamps();});
        Schema::table( 'sh_cloud_cpe.tb_comunicacionbaja_detalle', function( $table ){ $table->dropTimestamps(); });

        Schema::table( 'sh_cloud_cpe.tb_percepcion_cab', function( $table ){ $table->dropTimestamps(); });
        Schema::table( 'sh_cloud_cpe.tb_percepcion_det', function( $table ){$table->dropTimestamps();});
        Schema::table( 'sh_cloud_cpe.tb_percepcion_notas', function( $table ){$table->dropTimestamps();});

        Schema::table( 'sh_cloud_cpe.tb_retencion_cab', function( $table ){ $table->dropTimestamps(); });
        Schema::table( 'sh_cloud_cpe.tb_retencion_det', function( $table ){$table->dropTimestamps();});
        Schema::table( 'sh_cloud_cpe.tb_retencion_notas', function( $table ){$table->dropTimestamps();});

        Schema::table( 'sh_cloud_cpe.tb_resumendiario', function( $table ){ $table->dropTimestamps(); });
        Schema::table( 'sh_cloud_cpe.tb_resumendiario_detalle', function( $table ){$table->dropTimestamps();});

        Schema::table( 'sh_cloud_cpe.tb_resumenreversiones', function( $table ){ $table->dropTimestamps(); });
        Schema::table( 'sh_cloud_cpe.tb_resumenreversiones_det', function( $table ){$table->dropTimestamps();});
        Schema::table( 'job_info', function( $table ){$table->dropTimestamps();});
        Schema::table( 'job_info', function( $table ){$table->dropColumn('id');});
        Schema::table( 'job_info', function( $table ){$table->dropColumn('message');});
    }
}
